;************************************
; T�cnicas Digitales II 
; Autor: GM
; Fecha: 31-10-2016
; version: 0.1
; for AVR: atmega328p (Arduino UNO)
; clock frequency: 16MHz 
;************************************
;===========================================
; Funci�n del programa
; Configurar el Timer1 de modo que genere
; una se�al de PWM a una frecuencia de 10KHz
; con un ciclo de trabajo del 50%.
; Se podr� modificar el ciclo de trabajo 
; con comandos desde la PC. Estos ser�n:
; $PWM,+,<valor>* 
; $PWM,-,<valor>*
; El parametro <valor> es un Nro entre
; 1 y 50. 
; Respuesta:
; $PWM,<valor>* 
; Donde <valor> es el ciclo de trabajo actual
;-------------------------------------------
.list
.ifndef F_CPU
.set F_CPU = 16000000
.endif
;===========================================
; Declarations for register
.def temp 		= r16
.def overflows  = r17

;===========================================
; Etiquetas
.equ	baud = 9600			;Baud Rate
.equ	CTE_T_PWM = 1600

;===========================================
; Declarations for label
.set FLAGS0	= GPIOR0
.set COMM_PC = 0
.set COMM_Up = 1
.set COMM_Dn = 2
.set COMM_Rsp = 3
.set UpdateOCR1B = 7

;===========================================
; Data Segment
.dseg
VAL_PWM:			.byte	1				; Valor del PWM, en %
VAL_PWMnew:			.byte	1				; Nuevo Valor del PWM
PWM_word:			.byte	2				; Valor del PWM, escalado al registro OCR1B
Temp_OCR1B:			.byte	2				; Valor Temporal del registro OCR1B
BCDR0:				.byte	1				; Valor BCD de las unidades
BCDR1:				.byte	1				; Valor BCD de las decenas
BCDR2:				.byte	1				; Valor BCD de las centenas
BCDdigit:			.byte	1				; Cantidad de digitos en BCD
indexRx:			.byte	1				; �ndice dentro del buffer de recepci�n
BUFFER:				.byte	35				; Buffer de tx/rx a/desde la PC
	
;===========================================
; EEPROM Segment
.eseg
VAR_EEPROM:		.db		$AA

;===========================================
; Code Segment
.cseg
.org RWW_START_ADDR      	; memory (PC) location of reset handler
	rjmp Reset           	; jmp costs 2 cpu cycles and rjmp costs only 1
                         	; so unless you need to jump more than 8k bytes
                         	; you only need rjmp. Some microcontrollers therefore only 
                         	; have rjmp and not jmp

.org INT0addr				; memory location of External Interrupt Request 0
	rjmp isr_INT0_handler	; go here if a External Interrupt 0 occurs 

.org INT1addr				; memory location of External Interrupt Request 1
	rjmp isr_INT1_handler	; go here if a External Interrupt 1 occurs 

.org PCI0addr				; memory location of Pin Change Interrupt Request 0
	rjmp isr_PCI0_handler	; go here if a Pin Change Interrupt 0 occurs 

.org PCI1addr				; memory location of Pin Change Interrupt Request 1
	rjmp isr_PCI1_handler	; go here if a Pin Change Interrupt 1 occurs 

.org PCI2addr				; memory location of Pin Change Interrupt Request 2
	rjmp isr_PCI2_handler	; go here if a Pin Change Interrupt 2 occurs 

.org WDTaddr				; memory location of Watchdog Time-out Interrupt
	rjmp isr_WDT_handler	; go here if a Watchdog Time-out Interrupt occurs 

.org OC2Aaddr				; memory location of Timer/Counter2 Compare Match A Interrupt
	rjmp isr_OC2A_handler	; go here if a Timer/Counter2 Compare Match A Interrupt occurs 

.org OC2Baddr				; memory location of Timer/Counter2 Compare Match B Interrupt
	rjmp isr_OC2B_handler	; go here if a Timer/Counter2 Compare Match B Interrupt occurs 

.org OVF2addr				; memory location of Timer/Counter2 Overflow Interrupt
	rjmp isr_OVF2_handler	; go here if a Timer/Counter2 Overflow Interrupt occurs 

.org ICP1addr				; memory location of Timer/Counter1 Capture Event Interrupt
	rjmp isr_ICP1_handler	; go here if a Timer/Counter1 Capture Event Interrupt occurs 

.org OC1Aaddr				; memory location of Timer/Counter1 Compare Match A Interrupt
	rjmp isr_OC1A_handler	; go here if a Timer/Counter1 Compare Match A Interrupt occurs 

.org OC1Baddr				; memory location of Timer/Counter1 Compare Match B Interrupt
	rjmp isr_OC1B_handler	; go here if a Timer/Counter1 Compare Match B Interrupt occurs 

.org OVF1addr				; memory location of Timer/Counter1 Overflow Interrupt
	rjmp isr_OVF1_handler	; go here if a Timer/Counter1 Overflow Interrupt occurs 

.org OC0Aaddr				; memory location of Timer/Counter0 Compare Match A Interrupt
	rjmp isr_OC0A_handler	; go here if a Timer/Counter0 Compare Match A Interrupt occurs 

.org OC0Baddr				; memory location of Timer/Counter0 Compare Match B Interrupt
	rjmp isr_OC0B_handler	; go here if a Timer/Counter0 Compare Match B Interrupt occurs 

.org OVF0addr              	; memory location of Timer0 overflow handler
	rjmp isr_OVF0_handler	; go here if a timer0 overflow interrupt occurs 

.org SPIaddr              	; memory location of SPI Serial Transfer Complete handler
	rjmp isr_SPI_handler	; go here if a SPI Serial Transfer Complete interrupt occurs 

.org URXCaddr              	; memory location of USART Rx Complete handler
	rjmp isr_URXC_handler	; go here if a USART Rx Complete interrupt occurs 

.org UDREaddr              	; memory location of USART, Data Register Empty handler
	rjmp isr_UDRE_handler	; go here if a USART, Data Register Empty interrupt occurs 

.org UTXCaddr              	; memory location of USART Tx Complete handler
	rjmp isr_UTXC_handler	; go here if a USART Tx Complete interrupt occurs 

.org ADCCaddr              	; memory location of ADC Conversion Complete handler
	rjmp isr_ADCC_handler	; go here if a ADC Conversion Complete interrupt occurs 

.org ERDYaddr              	; memory location of EEPROM Ready handler
	rjmp isr_ERDY_handler	; go here if a EEPROM Ready interrupt occurs 

.org ACIaddr              	; memory location of Analog Comparator handler
	rjmp isr_ACI_handler	; go here if a Analog Comparator interrupt occurs 

.org TWIaddr              	; memory location of Two-wire Serial Interface handler
	rjmp isr_TWI_handler	; go here if a Two-wire Serial Interface interrupt occurs 

.org SPMRaddr              	; memory location of Store Program Memory Read handler
	rjmp isr_SPMR_handler	; go here if a Store Program Memory Read interrupt occurs 

; ==========================
; interrupt service routines  

isr_INT0_handler:
	reti					; External Interrupt 0

isr_INT1_handler:
	reti					; External Interrupt 1 

isr_PCI0_handler:
	reti					; Pin Change Interrupt 0 

isr_PCI1_handler:
	reti					; Pin Change Interrupt 1 

isr_PCI2_handler:
	reti					; Pin Change Interrupt 2 

isr_WDT_handler:
	reti					; Watchdog Time-out Interrupt  

isr_OC2A_handler:
	reti					; Timer/Counter2 Compare Match A Interrupt  

isr_OC2B_handler:
	reti					; Timer/Counter2 Compare Match B Interrupt  

isr_OVF2_handler:
	reti					; Timer/Counter2 Overflow Interrupt  

isr_ICP1_handler:
	reti					; Timer/Counter1 Capture Event Interrupt  

isr_OC1A_handler:
	reti					; Timer/Counter1 Compare Match A Interrupt  

isr_OC1B_handler:
		; Guardamos el Estado del uC
		in r5,SREG
		push r5
		push r16

		sbis FLAGS0,UpdateOCR1B		; Veo si hay que modificar el Duty Cycle
		rjmp _intfin	

		lds r16, Temp_OCR1B+1
		sts OCR1BH, r16
		lds r16, Temp_OCR1B+0
		sts OCR1BL, r16

		cbi FLAGS0,UpdateOCR1B		; UpdateOCR1B = 0
		sbi FLAGS0, COMM_Rsp		; COMM_Rsp = 1 --> Enviar la respuesta

_intfin:
		pop r16
		; Restauramos el Estado del uC
		pop r5
		out SREG,r5
		reti					; Timer/Counter1 Compare Match B Interrupt  

isr_OVF1_handler:
	reti					; Timer/Counter1 Overflow Interrupt  

isr_OC0A_handler:
	reti					; Timer/Counter0 Compare Match A Interrupt  

isr_OC0B_handler:
	reti					; Timer/Counter0 Compare Match B Interrupt  

isr_OVF0_handler:
	reti					; Timer0 overflow interrupt  

isr_SPI_handler:
	reti					; SPI Serial Transfer Complete interrupt  

isr_URXC_handler:
		; Save global interrupt flag
		in r5,SREG
		push r5
		push r16
		push r25
		push XH
		push XL

		ldi XL, low(BUFFER)
		ldi XH, high(BUFFER)

		lds r16, UDR0

		cpi r16, '$'
		brne _noini
_ini:
		clr r25
		sts indexRx, r25
_noini:
		lds r25, indexRx
		add XL, r25
		st X,r16
		inc r25
		sts indexRx, r25
		
		pop XL
		pop XH
		pop r25
		pop r16
		; Restore global interrupt flag
		pop r5
		out SREG,r5
		reti					; USART Rx Complete interrupt  

isr_UDRE_handler:
	reti					; USART, Data Register Empty interrupt  

isr_UTXC_handler:
	reti					; USART Tx Complete interrupt  

isr_ADCC_handler:
	reti					; ADC Conversion Complete interrupt  

isr_ERDY_handler:
	reti					; EEPROM Ready interrupt  

isr_ACI_handler:
	reti					; Analog Comparator interrupt  

isr_TWI_handler:
	reti					; Two-wire Serial Interface interrupt  

isr_SPMR_handler:
	reti					; Store Program Memory Read interrupt  


;======================
; Main body of program:
Reset:
		ldi R16, LOW(RAMEND)    	; Lower address byte RAM byte lo.
		out SPL, R16         		; Stack pointer initialise lo.
		ldi R16, HIGH(RAMEND)   	; Higher address of the RAM byte hi.
		out SPH, R16			 	; Stack pointer initialise hi. 
	
		rcall	Init_USART0			; Inicializo la USART0
		rcall	Init_PWM1			; Inicializo el Timer1
		sei							; Habilito las interrupciones
Loop:
		rcall Get_COMM_PC			; Veo si llego un comando por la USART0
		sbic FLAGS0,COMM_PC			; Si no llego, salto
		call Procces_PC				; Si llego, proceso el comando enviado
		rjmp Loop					; loop back to the start

;===========================================
; Get_COMM_PC
; Detecta si llego un comando por la USART0
; Par�metro:	none
; Retorno:		none
Get_COMM_PC:
{
		lds r16, indexRx							; Leo el Indice del buffer
		cpi r16, 11									; Veo si llegaron 11 datos
		breq _data
		cpi r16, 10									; Veo si llegaron 10 datos
		breq _data
		cpi r16, 9									; Veo si llegaron 9 datos
		breq _data
		rjmp _nodata
_data:
		ldi YL, low(BUFFER)							; Y <-- [BUFFER]
		ldi YH, high(BUFFER)

		ldd r16, Y+0
		cpi r16, '$'								; Veo si llego '$'
		brne _errordata
		ldd r16, Y+1		
		cpi r16, 'P'								; Veo si llego 'P'
		brne _errordata
		ldd r16, Y+2
		cpi r16, 'W'								; Veo si llego 'W'
		brne _errordata
		ldd r16, Y+3
		cpi r16, 'M'								; Veo si llego 'M'
		brne _errordata
		ldd r16, Y+4
		cpi r16, ','								; Veo si llego ','
		brne _errordata
		ldd r16, Y+6
		cpi r16, ','								; Veo si llego ','
		brne _errordata

		cbi FLAGS0, COMM_Dn							; COMM_Dn = 0
		sbi FLAGS0, COMM_Up							; COMM_Up = 1
		ldd r16, Y+5
		cpi r16, '+'								; Veo si llego '+'
		breq _mass
_minus:
		cpi r16, '-'								; Veo si llego '-'
		brne _errordata
		sbi FLAGS0, COMM_Dn							; COMM_Dn = 1
		cbi FLAGS0, COMM_Up							; COMM_Up = 0
_mass:
		sbi FLAGS0, COMM_PC							; COMM_PC = 1
		ldi r16, 0
		sts BCDR0, r16
		sts BCDR1, r16

		clr r18
		ldd r17, Y+7
		andi r17, 0x0F								; Dejo el Nro BCD
		sts BCDR0, r17								; BCDR0 <-- Y[7]
		inc r18
		ldd r16, Y+8
		cpi r16, '*'								; Veo si llego '*' en Y[8]
		breq _asteric
		andi r16, 0x0F								; Dejo el Nro BCD
		sts BCDR1, r17								; BCDR1 <-- Y[7]
		sts BCDR0, r16								; BCDR0 <-- Y[8]
		inc r18
_asteric:
		sts BCDdigit,r18							; Almaceno cantidad de d�gitos
		rjmp _fin 
_errordata:
		ldi r16, 0 
		sts indexRx, r16
		sts BCDR0, r16
		sts BCDR1, r16
		sts BCDR2, r16
		
		sbi FLAGS0, COMM_Rsp
		sbi FLAGS0, COMM_PC							; COMM_PC = 1
_fin:
		ldi r16, 0 
		sts indexRx, r16
_nodata:
		ret
}
	
;===========================================
; Procces_PC
; Procesa los comandos enviados desde la PC
; Par�metro:	none
; Retorno:		none
Procces_PC:
{
		sbic FLAGS0, COMM_Up
		call PWM_Up
		sbic FLAGS0, COMM_Dn
		call PWM_Dn

		sbic FLAGS0, COMM_Rsp
		call Send_Rsp
		ret
}

;===========================================
; PWM_Up
; Procesa el comando "+" que fue enviado
; desde la PC
; Par�metro:	none
; Retorno:		none
PWM_Up:
{
		ldi ZL, low(VAL_PWMnew)
		ldi ZH, high(VAL_PWMnew)
		ldi YL, low(BCDR0)
		ldi YH, high(BCDR0)
		call BCD_TO_BIN							; Convierto PWM a Binario
		
		lds r20, VAL_PWMnew						; Cargo valor Nuevo de PWM
		lds r21, VAL_PWM						; Cargo valor Actual de PWM
		add r21, r20							; sumo los pwm
		cpi r21,101								; comparo si es menor a 101
		brlt _pwmUp								; salto si es "less than"
		ldi r21, 100							; sino lo establezco en 100
_pwmUp:		 
		sts VAL_PWM, r21						; Guardo el nuevo valor de pwm
		call Escalar_PWM						; Convierto el PWM a valor del registro ICR
		
		lds r16, PWM_word						; Copio el nuevo valor
		lds r17, PWM_word+1						; del registro OCR1B
		sts Temp_OCR1B+0,r16					; al registro temporal
		sts Temp_OCR1B+1,r17					; y activo el flag UpdateOCR1B
				
		sbi FLAGS0,UpdateOCR1B					; UpdateOCR1B = 1
		cbi FLAGS0,COMM_Up						; COMM_Up = 0
		ret
}

;===========================================
; PWM_Dn
; Procesa el comando "-" que fue enviado
; desde la PC
; Par�metro:	none
; Retorno:		none
PWM_Dn:
{
		ldi ZL, low(VAL_PWMnew)
		ldi ZH, high(VAL_PWMnew)
		ldi YL, low(BCDR0)
		ldi YH, high(BCDR0)
		call BCD_TO_BIN							; Convierto PWM a Binario
		
		lds r20, VAL_PWMnew						; Cargo valor Nuevo de PWM
		lds r21, VAL_PWM						; Cargo valor Actual de PWM
		sub r21, r20							; resto los pwm
		brsh _pwmDn								; salto si es "same or Higher"
		ldi r21, 0								; sino lo establezco en 0

_pwmDn:		 
		sts VAL_PWM, r21						; Guardo el nuevo valor de pwm
		call Escalar_PWM						; Convierto el PWM a valor del registro ICR
		
		lds r16, PWM_word						; Copio el nuevo valor
		lds r17, PWM_word+1						; del registro OCR1B
		sts Temp_OCR1B+0,r16					; al registro temporal
		sts Temp_OCR1B+1,r17					; y activo el flag UpdateOCR1B
				
		sbi FLAGS0,UpdateOCR1B					; UpdateOCR1B = 1
		cbi FLAGS0,COMM_Dn
		ret
}	

;===========================================
; Escalar_PWM
; Convierte el valor de VAL_PWM, expresado
; como porcentaje, en un valor del registro
; PWM_word = pwm_table[VAL_PWM] 
; Par�metro:	none
; Retorno:		none
Escalar_PWM:
{
		push ZL
		push ZH
		push YL
		push YH

		ldi YL, low(VAL_PWM)				; Y <-- [VAL_PWM]
		ldi YH, high(VAL_PWM)
		ldi ZL, low(pwm_table*2)			; Z <-- [pwm_table]
		ldi ZH, high(pwm_table*2)			
		ldd r2, Y+0							; R2 <-- Val_PWM
		lsl r2								; Val_PWM * 2
		clr r3								; r3 = 0
		add ZL, r2							; ZL <-- ZL + (Val_PWMbin * 2)
		adc ZH, r3							; ZH <-- ZH + r3 + C
		lpm	r16,Z+							; r16 <-- [Z+]
		lpm	r17,Z							; r17 <-- [Z]
				
		ldi YL, low(PWM_word)				; Y <-- [PWM_word]
		ldi YH, high(PWM_word)
		std Y+0,r16							; PWM_word+0 <-- r16 
		std Y+1,r17							; PWM_word+1 <-- r17
		
		pop YH
		pop YL
		pop ZH
		pop ZL
		ret
} 

;===========================================
; Send_Rsp
; Env�a la CadenaRp al Buffer, completa con
; los datos del PWM en BCD y env�a por USART0
; Par�metro:	none
; Retorno:		none
Send_Rsp:
{
		ldi	r16,10						; Tama�o CadenaRp
		ldi	YL,low(BUFFER)				
		ldi YH,high(BUFFER)				; Y <- [Buffer]
		ldi	ZL,low(CadenaRp << 1 )	
		ldi ZH,high(CadenaRp << 1 )		; Z <- [CadenaRp * 2]

		rcall Copy_To_Buffer
		
		rcall Fill_PWM

		ldi	YL,low(BUFFER)				
		ldi YH,high(BUFFER)				; Y <- [Buffer]
		call Send_Buffer

		cbi FLAGS0, COMM_PC				; COMM_PC = 0
		cbi FLAGS0, COMM_Rsp			; COMM_Rsp = 0
		
		ret
}

;===========================================
; Send_Buffer
; Env�a el contenido del Buffer, hasta 
; encontrar un '\n', que tambien se env�a
; Par�metro:	none
; Retorno:		none
Send_Buffer:
{
		ld r16,Y+
		cpi r16,'\n'
		breq _finsend
		call Tx_Byte_USART0
		rjmp Send_Buffer
_finsend:
		ldi r16,'\n'
		call Tx_Byte_USART0

		ret
}

;===========================================
; Fill_PWM
; Completa en el Buffer los valores, segun
; se trate de 1,2 o 3 BCD, y los convierte 
; a ASCII
; Par�metro:	none
; Retorno:		none
Fill_PWM:
{
		ldi	YL,low( BCDR0 )				
		ldi YH,high( BCDR0 )		; Y <- [BCDR0]
		ldi	ZL,low( VAL_PWM )	
		ldi ZH,high( VAL_PWM )		; Z <- [VAL_PWM]

		rcall BIN_TO_BCD			; BCDR1:BCDR0 = BIN_TO_BCD ( VAL_PWM )

		clr r21
		ldd r20,Z+0
		cpi r20, 10
		brlo _undigPWM
		cpi r20,100
		brlo _dosdigPWM
_tresdigPWM:
		inc r21
_dosdigPWM:
		inc r21
_undigPWM:
		inc r21
		sts BCDdigit, r21

		ldi	YL,low( BUFFER )				
		ldi YH,high( BUFFER )		; Y <- [BUFFER]
		
		ldi r17,$30
		lds r16, BCDdigit
		cpi r16, 3
		breq _tresPWM
		cpi r16, 2
		breq _dosPWM
		cpi r16, 1
		breq _unoPWM
		jmp _nonePWM

_tresPWM:
		lds r20, BCDR1				; Implemento el SPLIT 
		sts BCDR2, r20				; Si hay tres digitos
		lds r21, BCDR0				; 
		sts BCDR1, r21				;
		andi r21, 0x0F				;
		sts BCDR0, r21				;
		lds r21, BCDR1				;
		andi r21, 0xF0				;
		swap r21					;
		sts BCDR1, r21				;

		lds r16, BCDR2
		add r16,r17
		std Y+5, r16
		lds r16, BCDR1
		add r16,r17
		std Y+6, r16
		lds r16, BCDR0
		add r16,r17
		std Y+7, r16
		rjmp _finPWM
_dosPWM:
		lds r20, BCDR0				; Implemento el SPLIT 
		sts BCDR1, r20				; Si hay dos digitos
		andi r20, 0x0F				;
		sts BCDR0, r20				;
		lds r21, BCDR1				;
		andi r21, 0xF0				;
		swap r21					;
		sts BCDR1, r21				;

		lds r16, BCDR1
		add r16,r17
		std Y+5, r16
		lds r16, BCDR0
		add r16,r17
		std Y+6, r16

		ldi r16, '*'
		std Y+7, r16
		ldi r16, '\n'
		std Y+8, r16

		rjmp _finPWM
_unoPWM:
		lds r20, BCDR0				; Implemento el SPLIT 
		andi r20, 0x0F				; Si hay un digito
		sts BCDR0, r20				;

		lds r16, BCDR0
		add r16,r17
		std Y+5, r16

		ldi r16, '*'
		std Y+6, r16
		ldi r16, '\n'
		std Y+7, r16

		rjmp _finPWM

_nonePWM:
_finPWM:
		ret
}



;===========================================
; Copy_To_Buffer
; Copia del puntero Z al Y la cantidad
; indicada en R16
; Par�metro:	R16=Cantidad, Z: Fuente, Y: Dest
; Retorno:		none
Copy_To_Buffer:
{
		lpm r17,Z+
		st Y+,r17
		dec r16
		brne Copy_To_Buffer
		ret
}

;===========================================
; BCD_TO_BIN
; Convierte un Nro BCD en BIN
; Par�metro:	ZH:ZL	-> Nro Binario
; Retorno:		YH:YL	-> Direcci�n Nro BCD
BCD_TO_BIN:
{
		;tbin	;binary result
		;fBCDL	;lower digit of BCD input
		;fBCDH	;higher digit of BCD input

		ldd r20, Y+0
		mov fBCDL, r20					; fBCDL <-- Y[0]
		ldd r20, Y+1	
		mov fBCDH, r20					; fBCDH <-- Y[1]

		call BCD2bin8
		
		st Z, tbin						; [Z] <-- tbin

		ret
}

;===========================================
; BIN_TO_BCD
; Convierte un Nro BIN a BCD
; Par�metro:	ZH:ZL	-> Nro Binario
; Retorno:		YH:YL	-> Direcci�n Nro BCD
BIN_TO_BCD:
{
		;tBCD0	;BCD value digits 1 and 0
		;tBCD1	;BCD value digits 3 and 2
		;tBCD2	;BCD value digit 4
		;fbinL	;binary value Low byte
		;fbinH	;binary value High byte
		ldd fbinL, Z+0
		;ldd fbinH, Z+1
		ldi fbinH, 0

		push ZL
		push ZH

		call bin2BCD16

		pop ZH
		pop ZL

		mov r20, tBCD0	; Movimiento temporal
		mov r21, tBCD1
		;mov r22, tBCD2	; DESCARTADO

		std Y+0, r20
		std Y+1, r21
		;std Y+2, r22	; DESCARTADO
		ret
}

;===========================================
; Tx_Byte_USART0
; Env�a dato por USART0
; Par�metro: R16 -> dato que se env�a
; Retorno: No
Tx_Byte_USART0:
{
		; Wait for empty transmit buffer
		lds r17,UCSR0A			;Load into R17 from SRAM UCSR0A         
		sbrs r17,UDRE0			;Skip next instruction If Bit Register is set
		rjmp Tx_Byte_USART0
		; Put data (r16) into buffer, sends the data
		sts UDR0,r16
		ret
}

;===========================================
; Rx_Byte_USART0
; Recibe dato por USART0
; Par�metro: No 
; Retorno: R16 -> dato que se recibe
Rx_Byte_USART0:
{
		; Wait for data to be received
		lds r17,UCSR0A
		sbrs r17,RXC0
		rjmp Rx_Byte_USART0
		; Get and return received data from buffer
		lds r16, UDR0
		ret
}

;===========================================
; Init_USART0
; Inicializa la transmisi�n serie: 8N1 9600
; Par�metro: No
; Retorno: No
Init_USART0:
{
		; Establecemos la Velocidad (Baudrate)
		ldi	r17,high(F_CPU/(16*baud)-1)	
		ldi	r16,low(F_CPU/(16*baud)-1)	
		sts UBRR0H, r17
		sts UBRR0L, r16
			
		; Habilitamos Rx, Tx y las Interrupcion por Rx
		ldi r16, (1<<RXEN0)|(1<<TXEN0)|(1<<RXCIE0)		
		sts UCSR0B,r16

		; Establecemos el formato del frame: 8 data, 1 stop bit
		ldi r16, (0<<USBS0)|(1<<UCSZ01)|(1<<UCSZ00)		
		sts UCSR0C,r16
		ret
}

;===========================================
; Init_PWM
; Inicializa el PWM del Timer1
; Se�al de 10 KHz en OCR1B, con per�odo
; establecido en ICR1 = CTE_T_PWM - 1
; con se�al sin invertir
; Par�metro: No
; Retorno: No
Init_PWM1:
{
		// PB1 y PB2 como SALIDA
		sbi DDRB, (1 << DDB2)
		sbi DDRB, (1 << DDB1)
		
		// Establecemos el Per�odo
		ldi r16, high( CTE_T_PWM - 1 )
		sts ICR1H, r16
		ldi r16, low( CTE_T_PWM - 1 )
		sts ICR1L, r16
			
		// Establecemos el 50% de Duty Cycle @ 16bit en OCR1B
		ldi r16, high( (CTE_T_PWM - 1) / 2 )
		sts OCR1BH, r16
		ldi r16, low( (CTE_T_PWM - 1) / 2 )
		sts OCR1BL, r16
		
		// Establecemos el modo SIN-INVERTIR
		lds r16, TCCR1A
		ori r16, (1 << COM1B1) | (0 << COM1B0)
		sts TCCR1A, r16

		// Establezco el "PWM R�pido" usando el ICR1 como TOP
		lds r16, TCCR1A
		ori r16, (1 << WGM11)
		sts TCCR1A, r16
		lds r16, TCCR1B
		ori r16, (1 << WGM12)|(1 << WGM13)
		sts TCCR1B, r16
		
		// Habilito la interrupci�n Output Compare en el canal B
		lds r16, TIMSK1
		ori r16, ( 1 << OCIE1B )
		sts TIMSK1, r16
			
		// Inicio el Timer sin Prescaler
		lds r16, TCCR1B
		ori r16, (1 << CS10)
		sts TCCR1B, r16	
		
		// Inicio el PWM en 50%
		ldi r16, 50
		sts VAL_PWM, r16					
			
		ret
}

LED_SWH:
		ldi r16,0x20
		in r17,PORTB
		eor r16,r17
		out PORTB,r16
		cbi FLAGS0,COMM_PC
		ret

;===========================================
; FLASH Segment (Const)

CadenaUp:	.db	"$PWM,+,  *",'\n',0			;Dejo 2 lugares para el nro (12Byte)
CadenaDn:	.db	"$PWM,-,  *",'\n',0			;Dejo 2 lugares para el nro (12Byte)
CadenaRp:	.db	"$PWM,   *",'\n'			;Dejo 3 lugares para el nro (10Byte)


;===========================================
; Include other File
;.include "avr200.inc"
.include "avr204.inc"


pwm_table:
.dw 0,15,31,47,63,79,95,111,127,143,159,175,191,207,223,239,255,271,287,303,319
.dw 335,351,367,383,399,415,431,447,463,479,495,511,527,543,559,575,591,607,623
.dw 639,655,671,687,703,719,735,751,767,783,799,815,831,847,863,879,895,911,927
.dw 943,959,975,991,1007,1023,1039,1055,1071,1087,1103,1119,1135,1151,1167,1183
.dw 1199,1215,1231,1247,1263,1279,1295,1311,1327,1343,1359,1375,1391,1407,1423
.dw 1439,1455,1471,1487,1503,1519,1535,1551,1567,1583,1599

