;
; TPNro1.2.asm
;
; Created: 7/13/2020 8:35:30 PM
; Author : Gustavo Maggiolo
;
; Blink (13) with enable (10)

;===========================================
; Code Segment
.cseg
	ldi r16,LOW(RAMEND)	; Inicio del stack pointer
	out SPL,r16
	ldi r16,HIGH(RAMEND)
	out SPH,r16

    ldi r16, 0		
	out SREG, r16	; Reset status del sistema
	
		
	ldi r16,$20
	out DDRB,r16	;PortB.5 como salida (Pin 13 Arduino)

clear:	
	clr r17			; Borro el registro r17
	out PORTB,r17	; (Pin 13 Arduino)
mainloop:
	
	sbis PINB,2		; PINB.2 == 1? (Pin 10 Arduino)
	rjmp clear

	eor r17,r16		; invierto el bit
	out PORTB,r17	; escribe el puerto
	
	rcall wait		; espera un tiempo
	
	rjmp mainloop	; loop infinito


wait:
	push r16
	push r17
	push r18

	ldi r16,0x0F 
	ldi r17,0x00 
	ldi r18,0x00 
_w0:
	dec r18
	brne _w0
	dec r17
	brne _w0
	dec r16
	brne _w0

	pop r18
	pop r17
	pop r16
	ret
