;************************************
; T�cnicas Digitales II 
; Autor: GM
; Fecha: 15-09-2020
; version: 0.1
; for AVR: atmega328p (Arduino UNO)
; clock frequency: 16MHz 
;************************************

;===========================================
; Funci�n del programa
; Se deber� realizar un programa que reciba 
; datos, de una PC, por medio del puerto
; serie y los re-transmita a la PC, de a 
; un byte.
;-------------------------------------------
.ifndef F_CPU
.set F_CPU = 16000000
.endif
;===========================================
; Declarations for register
.def temp 		= r16
.def overflows  = r17

;===========================================
; Etiquetas
.equ	baud = 9600			;Baud Rate

;===========================================
; Data Segment
.dseg
VAR1:			.byte	1
VAR2:			.byte	2

;===========================================
; EEPROM Segment
.eseg
VAR_EEPROM:		.db		$AA

;===========================================
; Code Segment
.cseg
.org RWW_START_ADDR      	; memory (PC) location of reset handler
	rjmp Reset           	; jmp costs 2 cpu cycles and rjmp costs only 1
                         	; so unless you need to jump more than 8k bytes
                         	; you only need rjmp. Some microcontrollers therefore only 
                         	; have rjmp and not jmp

.org INT0addr				; memory location of External Interrupt Request 0
	rjmp isr_INT0_handler	; go here if a External Interrupt 0 occurs 

.org INT1addr				; memory location of External Interrupt Request 1
	rjmp isr_INT1_handler	; go here if a External Interrupt 1 occurs 

.org PCI0addr				; memory location of Pin Change Interrupt Request 0
	rjmp isr_PCI0_handler	; go here if a Pin Change Interrupt 0 occurs 

.org PCI1addr				; memory location of Pin Change Interrupt Request 1
	rjmp isr_PCI1_handler	; go here if a Pin Change Interrupt 1 occurs 

.org PCI2addr				; memory location of Pin Change Interrupt Request 2
	rjmp isr_PCI2_handler	; go here if a Pin Change Interrupt 2 occurs 

.org WDTaddr				; memory location of Watchdog Time-out Interrupt
	rjmp isr_WDT_handler	; go here if a Watchdog Time-out Interrupt occurs 

.org OC2Aaddr				; memory location of Timer/Counter2 Compare Match A Interrupt
	rjmp isr_OC2A_handler	; go here if a Timer/Counter2 Compare Match A Interrupt occurs 

.org OC2Baddr				; memory location of Timer/Counter2 Compare Match B Interrupt
	rjmp isr_OC2B_handler	; go here if a Timer/Counter2 Compare Match B Interrupt occurs 

.org OVF2addr				; memory location of Timer/Counter2 Overflow Interrupt
	rjmp isr_OVF2_handler	; go here if a Timer/Counter2 Overflow Interrupt occurs 

.org ICP1addr				; memory location of Timer/Counter1 Capture Event Interrupt
	rjmp isr_ICP1_handler	; go here if a Timer/Counter1 Capture Event Interrupt occurs 

.org OC1Aaddr				; memory location of Timer/Counter1 Compare Match A Interrupt
	rjmp isr_OC1A_handler	; go here if a Timer/Counter1 Compare Match A Interrupt occurs 

.org OC1Baddr				; memory location of Timer/Counter1 Compare Match B Interrupt
	rjmp isr_OC1B_handler	; go here if a Timer/Counter1 Compare Match B Interrupt occurs 

.org OVF1addr				; memory location of Timer/Counter1 Overflow Interrupt
	rjmp isr_OVF1_handler	; go here if a Timer/Counter1 Overflow Interrupt occurs 

.org OC0Aaddr				; memory location of Timer/Counter0 Compare Match A Interrupt
	rjmp isr_OC0A_handler	; go here if a Timer/Counter0 Compare Match A Interrupt occurs 

.org OC0Baddr				; memory location of Timer/Counter0 Compare Match B Interrupt
	rjmp isr_OC0B_handler	; go here if a Timer/Counter0 Compare Match B Interrupt occurs 

.org OVF0addr              	; memory location of Timer0 overflow handler
	rjmp isr_OVF0_handler	; go here if a timer0 overflow interrupt occurs 

.org SPIaddr              	; memory location of SPI Serial Transfer Complete handler
	rjmp isr_SPI_handler	; go here if a SPI Serial Transfer Complete interrupt occurs 

.org URXCaddr              	; memory location of USART Rx Complete handler
	rjmp isr_URXC_handler	; go here if a USART Rx Complete interrupt occurs 

.org UDREaddr              	; memory location of USART, Data Register Empty handler
	rjmp isr_UDRE_handler	; go here if a USART, Data Register Empty interrupt occurs 

.org UTXCaddr              	; memory location of USART Tx Complete handler
	rjmp isr_UTXC_handler	; go here if a USART Tx Complete interrupt occurs 

.org ADCCaddr              	; memory location of ADC Conversion Complete handler
	rjmp isr_ADCC_handler	; go here if a ADC Conversion Complete interrupt occurs 

.org ERDYaddr              	; memory location of EEPROM Ready handler
	rjmp isr_ERDY_handler	; go here if a EEPROM Ready interrupt occurs 

.org ACIaddr              	; memory location of Analog Comparator handler
	rjmp isr_ACI_handler	; go here if a Analog Comparator interrupt occurs 

.org TWIaddr              	; memory location of Two-wire Serial Interface handler
	rjmp isr_TWI_handler	; go here if a Two-wire Serial Interface interrupt occurs 

.org SPMRaddr              	; memory location of Store Program Memory Read handler
	rjmp isr_SPMR_handler	; go here if a Store Program Memory Read interrupt occurs 

; ==========================
; interrupt service routines  

isr_INT0_handler:
	reti					; External Interrupt 0

isr_INT1_handler:
	reti					; External Interrupt 1 

isr_PCI0_handler:
	reti					; Pin Change Interrupt 0 

isr_PCI1_handler:
	reti					; Pin Change Interrupt 1 

isr_PCI2_handler:
	reti					; Pin Change Interrupt 2 

isr_WDT_handler:
	reti					; Watchdog Time-out Interrupt  

isr_OC2A_handler:
	reti					; Timer/Counter2 Compare Match A Interrupt  

isr_OC2B_handler:
	reti					; Timer/Counter2 Compare Match B Interrupt  

isr_OVF2_handler:
	reti					; Timer/Counter2 Overflow Interrupt  

isr_ICP1_handler:
	reti					; Timer/Counter1 Capture Event Interrupt  

isr_OC1A_handler:
	reti					; Timer/Counter1 Compare Match A Interrupt  

isr_OC1B_handler:
	reti					; Timer/Counter1 Compare Match B Interrupt  

isr_OVF1_handler:
	reti					; Timer/Counter1 Overflow Interrupt  

isr_OC0A_handler:
	reti					; Timer/Counter0 Compare Match A Interrupt  

isr_OC0B_handler:
	reti					; Timer/Counter0 Compare Match B Interrupt  

isr_OVF0_handler:
	reti					; Timer0 overflow interrupt  

isr_SPI_handler:
	reti					; SPI Serial Transfer Complete interrupt  

isr_URXC_handler:
	reti					; USART Rx Complete interrupt  

isr_UDRE_handler:
	reti					; USART, Data Register Empty interrupt  

isr_UTXC_handler:
	reti					; USART Tx Complete interrupt  

isr_ADCC_handler:
	reti					; ADC Conversion Complete interrupt  

isr_ERDY_handler:
	reti					; EEPROM Ready interrupt  

isr_ACI_handler:
	reti					; Analog Comparator interrupt  

isr_TWI_handler:
	reti					; Two-wire Serial Interface interrupt  

isr_SPMR_handler:
	reti					; Store Program Memory Read interrupt  

;======================
; Main body of program:
Reset:
	ldi R16, LOW(RAMEND)    	; Lower address byte RAM byte lo.
	out SPL, R16         		; Stack pointer initialise lo.
	ldi R16, HIGH(RAMEND)   	; Higher address of the RAM byte hi.
	out SPH, R16			 	; Stack pointer initialise hi. 
; write your code here
	rcall Init_USART0
			
	ldi r19,(1 << DDB5)
	out DDRB, r19

	clr r18
Loop:
	eor r18,r19					; invert output bit
	out PORTB,r18				; write to port
		   
	rcall Rx_Byte_USART0		; Get Byte
	rcall Tx_Byte_USART0		; Send Byte

	rjmp Loop            ; loop back to the start
 
  
;===========================================
; Init_USART0
; Inicializa la transmisi�n serie: 8N1 9600
; Par�metro: No
; Retorno: No
Init_USART0:
{
			;Set Baud Rate
			ldi	r17,high(F_CPU/(16*baud)-1)	
			ldi	r16,low(F_CPU/(16*baud)-1)	
			sts UBRR0H, r17
			sts UBRR0L, r16
			
			; Enable receiver and transmitter
			ldi r16, (1<<RXEN0)|(1<<TXEN0)		
			sts UCSR0B,r16

			; Set frame format: 8data, 1stop bit
			ldi r16, (0<<USBS0)|(3<<UCSZ00)		
			sts UCSR0C,r16
			ret
}

;===========================================
; Tx_Byte_USART0
; Env�a dato por USART0
; Par�metro: R16 -> dato que se env�a
; Retorno: No
Tx_Byte_USART0:
{
		   ; Wait for empty transmit buffer
		   lds r17,UCSR0A			;Load into R17 from SRAM UCSR0A         
		   sbrs r17,UDRE0			;Skip next instruction If Bit Register is set
		   rjmp Tx_Byte_USART0
		   ; Put data (r16) into buffer, sends the data
		   sts UDR0,r16
		   ret
}

;===========================================
; Rx_Byte_USART0
; Recibe dato por USART0
; Par�metro: No 
; Retorno: R16 -> dato que se recibe
Rx_Byte_USART0:
{
		   ; Wait for data to be received
		   lds r17,UCSR0A
		   sbrs r17,RXC0
		   rjmp Rx_Byte_USART0
		   ; Get and return received data from buffer
		   lds r16, UDR0
		   ret
}
 
  

