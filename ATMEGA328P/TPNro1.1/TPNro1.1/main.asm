;
; TPNro1.1.asm
;
; Created: 7/13/2020 8:20:23 PM
; Author : Gustavo Maggiolo
;
; Blink (13)

    ldi r16,0		
	out SREG,r16		; Reset status del sistema
	ldi r16,LOW(RAMEND)	; Inicio del stack pointer
	out SPL,r16
	ldi r16,HIGH(RAMEND)
	out SPH,r16
	
	;ldi r16,0x00
	;out PORTB,r16

	ldi r16, (1 << PB5)
	out DDRB,r16
	nop
	clr r17			; Borro el registro r17
	
mainloop:
	
	eor r17,r16		; invierto el bit
	out PORTB,r17	; escribe el puerto

	rcall wait		; espera un tiempo

	jmp mainloop	; loop infinito


wait:
	push r16
	push r17
	push r18

	ldi r16,0x40 ; 40 loop 0x400000 times
	ldi r17,0x00 ; 00 ~12 million cycles
	ldi r18,0x00 ; 00 ~0.7s at 16Mhz
_w0:
	dec r18
	brne _w0
	dec r17
	brne _w0
	dec r16
	brne _w0

	pop r18
	pop r17
	pop r16
	ret
