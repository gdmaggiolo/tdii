;******************************************************************************
;T�cnicas Digitales II
;Ejercitaci�n Nro 2.6
;Autor: Ing. Maggiolo Gustavo
;******************************************************************************

; Ejercitaci�n Nro 2.6
; Realice un programa que ordene, de menor a mayor, un grupo de 8 datos,
; almacenados en la memoria RAM desde la posici�n 3000h.

;******************************************************************************
;	Definici�n de Etiquetas
;******************************************************************************

.define
	BootAddr		0000h
	AddrIntRST1	0008h
	AddrIntRST2	0010h
	AddrIntRST3	0018h
	AddrIntRST4	0020h
	AddrIntTRAP	0024h
	AddrIntRST5	0028h
	AddrIntRST55	002Ch
	AddrIntRST6	0030h
	AddrIntRST65	0034h
	AddrIntRST7	0038h
	AddrIntRST75	003Ch

	STACK_ADDR	FFFFh

	IniDataROM	0540h		;Definir donde termina el Programa
	IniDataRAM	2000h		;Definir de acuerdo al Hardware

	ptrDatos		1040h
		
;******************************************************************************
;	Definici�n de Datos en RAM (Variables)
;******************************************************************************
.data	1500h
CANT:			dB	10

.data	IniDataRAM

DATA:			dB	60,50,10,10,110,10,11,43,44,3,0
	
;******************************************************************************
;	Definici�n de Datos en ROM (Constantes)
;******************************************************************************
.data	IniDataROM
	
CteMAX:	dW	03FFh
CteMIN:	dB	80h

;******************************************************************************
;	Sector de Arranque del 8085
;******************************************************************************
	.org	BootAddr
		JMP	Boot
;******************************************************************************
;	Sector del Vector de Interrupciones
;******************************************************************************
	.org	AddrIntRST1
		JMP	IntRST1
	.org	AddrIntRST2
		JMP	IntRST2
	.org	AddrIntRST3
		JMP	IntRST3
	.org	AddrIntRST4
		JMP	IntRST4
	.org	AddrIntTRAP
		JMP	IntTRAP
	.org	AddrIntRST5
		JMP	IntRST5
	.org	AddrIntRST55
		JMP	IntRST55
	.org	AddrIntRST6
		JMP	IntRST6
	.org	AddrIntRST65
		JMP	IntRST65
	.org	AddrIntRST7
		JMP	IntRST7
	.org	AddrIntRST75
		JMP	IntRST75

;******************************************************************************
;	Sector de las Interrupciones
;******************************************************************************

IntRST1:	;Ac� va el c�digo de la Interrupci�n RST1
		EI
		RET
IntRST2:	;Ac� va el c�digo de la Interrupci�n RST2
		EI
		RET
IntRST3:	;Ac� va el c�digo de la Interrupci�n RST3
		EI
		RET
IntRST4:	;Ac� va el c�digo de la Interrupci�n RST4
		EI
		RET
IntTRAP:	;Ac� va el c�digo de la Interrupci�n TRAP
		EI
		RET
IntRST5:	;Ac� va el c�digo de la Interrupci�n RST5
		EI
		RET
IntRST55:	;Ac� va el c�digo de la Interrupci�n RST5.5
		EI
		RET
IntRST6:	;Ac� va el c�digo de la Interrupci�n RST6
		EI
		RET
IntRST65:	;Ac� va el c�digo de la Interrupci�n RST6.5
		EI
		RET
IntRST7:	;Ac� va el c�digo de la Interrupci�n RST7
		EI
		RET
IntRST75:	;Ac� va el c�digo de la Interrupci�n RST7.5
		EI
		RET


;******************************************************************************
;	Sector del Programa Principal
;******************************************************************************
Boot:
	LXI	SP,STACK_ADDR	;Inicializo el Puntero de Pila
	
Main:
	
	CALL	MENOR_A_MAYOR
	
	HLT

;******************************************************************************
; Funci�n: MENOR_A_MAYOR
; Descripci�n: Ordena de menor a mayor 8 datos desde la posici�n 3000h
; Entrada: ninguno
; Salida: ninguno
;******************************************************************************
MENOR_A_MAYOR:
		PUSH PSW
		PUSH H
		PUSH B

Reinicio:		
		LXI H, 3000h 		;Inicio puntero
		MVI C, 7				;Inicio contador de comparaciones, CANTIDAD DE DATOS - 1
Loop:
		MOV A, M 			;A <- [M] Leo el dato
		INX H 				;HL += 1  Incremento puntero
		CMP M 				;A - M	  Comparo el acumulador A con el dato en memoria
		JC NoConmutar		;Si hay carri los datos estan ordenados, no conmutar
		JZ NoConmutar		;Si hay Zero, son iguales, no conmuto
Conmutar:
		MOV B, M				;Si hay que conmutar, guardo temporal en B el valor de memoria
		MOV M, A				;
		DCX H				;Decremento el puntero
		MOV M, B				;
		;INX H				;Vuelvo a incrementar el puntero
		JMP Reinicio
NoConmutar:
		DCR C				;Decremento el contador de comparaciones
		JNZ Loop				;Si llega a cero, termine
		
		POP	B
		POP	H
		POP	PSW
		RET
