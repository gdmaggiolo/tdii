;******************************************************************************
;T�cnicas Digitales II
;Ejercitaci�n Nro 2.4
;Autor: Ing. Maggiolo Gustavo
;******************************************************************************

; Ejercitaci�n Nro 2.4
; Se desea conocer la cantidad de valores iguales a 150 que se encuentren en un
; banco de memoria RAM hallado entre la posici�n 1010h y 1100h. La cuenta
; resultante se debe escribir en la direcci�n de entrada/salida 50h.


;******************************************************************************
;	Definici�n de Etiquetas
;******************************************************************************

.define
	BootAddr		0000h
	AddrIntRST1	0008h
	AddrIntRST2	0010h
	AddrIntRST3	0018h
	AddrIntRST4	0020h
	AddrIntTRAP	0024h
	AddrIntRST5	0028h
	AddrIntRST55	002Ch
	AddrIntRST6	0030h
	AddrIntRST65	0034h
	AddrIntRST7	0038h
	AddrIntRST75	003Ch

	STACK_ADDR	FFFFh

	IniDataROM	0540h		;Definir donde termina el Programa
	IniDataRAM	2000h		;Definir de acuerdo al Hardware

	ptrDatos		1040h
		
;******************************************************************************
;	Definici�n de Datos en RAM (Variables)
;******************************************************************************
.data	IniDataRAM
Temp:			dB	0,0,0,0,0,0,0,0
	
;******************************************************************************
;	Definici�n de Datos en ROM (Constantes)
;******************************************************************************
.data	IniDataROM
	
CteMAX:	dW	03FFh
CteMIN:	dB	80h

;******************************************************************************
;	Sector de Arranque del 8085
;******************************************************************************
	.org	BootAddr
		JMP	Boot
;******************************************************************************
;	Sector del Vector de Interrupciones
;******************************************************************************
	.org	AddrIntRST1
		JMP	IntRST1
	.org	AddrIntRST2
		JMP	IntRST2
	.org	AddrIntRST3
		JMP	IntRST3
	.org	AddrIntRST4
		JMP	IntRST4
	.org	AddrIntTRAP
		JMP	IntTRAP
	.org	AddrIntRST5
		JMP	IntRST5
	.org	AddrIntRST55
		JMP	IntRST55
	.org	AddrIntRST6
		JMP	IntRST6
	.org	AddrIntRST65
		JMP	IntRST65
	.org	AddrIntRST7
		JMP	IntRST7
	.org	AddrIntRST75
		JMP	IntRST75

;******************************************************************************
;	Sector de las Interrupciones
;******************************************************************************

IntRST1:	;Ac� va el c�digo de la Interrupci�n RST1
		EI
		RET
IntRST2:	;Ac� va el c�digo de la Interrupci�n RST2
		EI
		RET
IntRST3:	;Ac� va el c�digo de la Interrupci�n RST3
		EI
		RET
IntRST4:	;Ac� va el c�digo de la Interrupci�n RST4
		EI
		RET
IntTRAP:	;Ac� va el c�digo de la Interrupci�n TRAP
		EI
		RET
IntRST5:	;Ac� va el c�digo de la Interrupci�n RST5
		EI
		RET
IntRST55:	;Ac� va el c�digo de la Interrupci�n RST5.5
		EI
		RET
IntRST6:	;Ac� va el c�digo de la Interrupci�n RST6
		EI
		RET
IntRST65:	;Ac� va el c�digo de la Interrupci�n RST6.5
		EI
		RET
IntRST7:	;Ac� va el c�digo de la Interrupci�n RST7
		EI
		RET
IntRST75:	;Ac� va el c�digo de la Interrupci�n RST7.5
		EI
		RET

;******************************************************************************
;	Sector del Programa Principal
;******************************************************************************
Boot:
	LXI	SP,STACK_ADDR			;Inicializo el Puntero de Pila
	
Main:
	
	CALL	IGUALA150
	
	HLT


;******************************************************************************
; Funci�n: IGUALA150
; Descripci�n: Cuenta, desde 1010h hasta 1100h, los valores iguales a 150.
; Entrada: ninguno
; Salida: ninguno
;******************************************************************************
IGUALA150:
	PUSH	PSW
	PUSH	H
	PUSH	B

	LXI H, 1010h   		;Cargo puntero
	MVI B,00       		;Inicio contador
LOOP:
	MOV A, M       		;leo valor de la memoria
	CPI 150d	       		;comparo con 96h (150d). Hace A-150
	JNZ DISTINTO   		;si no es Zero --> es distinto
	INR B          		;si es Zero, es igual --> cuento
DISTINTO:
	INX H          		;incremento puntero
	MOV A, H       		;muevo para comparar 
	CPI 11h        		;con "1100h"
	JNZ LOOP       		;termine: no, salto a loop
	MOV A, M       		;termine: si, pero
	CPI 96h        		;falta el �ltimo
	JNZ FIN        		;n� comparar
	INR B          		;si es igual cuento
FIN:
	MOV A, B        		;muevo para sacar
	OUT 50h        		;por la direci�n de IO 50h
	
	POP	B
	POP	H
	POP	PSW
	RET