;******************************************************************************
;T�cnicas Digitales II
;Ejercitaci�n Nro 2.2
;Autor: Ing. Maggiolo Gustavo
;******************************************************************************

; Ejercitaci�n Nro 2.2
; Escriba un programa que almacene a partir de la posici�n 2000h los valores
; ASCII perteneciente al alfabeto, comenzando con las may�sculas y a
; continuaci�n las min�sculas


;******************************************************************************
;	Definici�n de Etiquetas
;******************************************************************************

.define
	BootAddr			0000h
	AddrIntRST1		0008h
	AddrIntRST2		0010h
	AddrIntRST3		0018h
	AddrIntRST4		0020h
	AddrIntTRAP		0024h
	AddrIntRST5		0028h
	AddrIntRST55		002Ch
	AddrIntRST6		0030h
	AddrIntRST65		0034h
	AddrIntRST7		0038h
	AddrIntRST75		003Ch

	STACK_ADDR		FFFFh

	IniDataROM		0540h		;Definir donde termina el Programa
	IniDataRAM		2000h		;Definir de acuerdo al Hardware

	ptrDatos		1040h
		
;******************************************************************************
;	Definici�n de Datos en RAM (Variables)
;******************************************************************************
.data	IniDataRAM
Temp:			dB	0,0,0,0,0,0,0,0
	
;******************************************************************************
;	Definici�n de Datos en ROM (Constantes)
;******************************************************************************
.data	IniDataROM
	
CteMAX:	dW	03FFh
CteMIN:	dB	80h

;******************************************************************************
;	Sector de Arranque del 8085
;******************************************************************************
	.org	BootAddr
		JMP	Boot
;******************************************************************************
;	Sector del Vector de Interrupciones
;******************************************************************************
	.org	AddrIntRST1
		JMP	IntRST1
	.org	AddrIntRST2
		JMP	IntRST2
	.org	AddrIntRST3
		JMP	IntRST3
	.org	AddrIntRST4
		JMP	IntRST4
	.org	AddrIntTRAP
		JMP	IntTRAP
	.org	AddrIntRST5
		JMP	IntRST5
	.org	AddrIntRST55
		JMP	IntRST55
	.org	AddrIntRST6
		JMP	IntRST6
	.org	AddrIntRST65
		JMP	IntRST65
	.org	AddrIntRST7
		JMP	IntRST7
	.org	AddrIntRST75
		JMP	IntRST75

;******************************************************************************
;	Sector de las Interrupciones
;******************************************************************************

IntRST1:	;Ac� va el c�digo de la Interrupci�n RST1
		EI
		RET
IntRST2:	;Ac� va el c�digo de la Interrupci�n RST2
		EI
		RET
IntRST3:	;Ac� va el c�digo de la Interrupci�n RST3
		EI
		RET
IntRST4:	;Ac� va el c�digo de la Interrupci�n RST4
		EI
		RET
IntTRAP:	;Ac� va el c�digo de la Interrupci�n TRAP
		EI
		RET
IntRST5:	;Ac� va el c�digo de la Interrupci�n RST5
		EI
		RET
IntRST55:	;Ac� va el c�digo de la Interrupci�n RST5.5
		EI
		RET
IntRST6:	;Ac� va el c�digo de la Interrupci�n RST6
		EI
		RET
IntRST65:	;Ac� va el c�digo de la Interrupci�n RST6.5
		EI
		RET
IntRST7:	;Ac� va el c�digo de la Interrupci�n RST7
		EI
		RET
IntRST75:	;Ac� va el c�digo de la Interrupci�n RST7.5
		EI
		RET


;******************************************************************************
;	Sector del Programa Principal
;******************************************************************************
Boot:
	LXI	SP,STACK_ADDR	;Inicializo el Puntero de Pila
	
Main:
	
	CALL	ALMACENA
		
	HLT

;******************************************************************************
; Funci�n: ALMACENA
; Descripci�n: Llena con los ASCII desde la 2000h.
; Entrada: ninguno
; Salida: ninguno
;******************************************************************************
ALMACENA:
		PUSH	PSW
		PUSH	H
		PUSH	B

		LXI H, 2000h	;Inicio puntero
		LXI B, 4161h   	;B= 41h (65d)  y C= 61h (97d)
		MVI A, 1Ah     	;A = 1Ah (26d)
LOOP1:                  	;COMIENZO MAYUSC.
		MOV M, B			;Guardo el valor de B en memoria
		INR B			;Incremento B, ya que los codigos son n�meros consecutivos
		INX H			;Incremento el puntero (par H-L)
		CMP L			;Comparo A con L. Hace A-L
		JNZ LOOP1		;Si son = A y L, sigue
		ADI 26d			;A += 26
LOOP2:                  ;COMIENZO MINUSC.
		MOV M, C
		INR C
		INX H
		CMP L			;A-L
		JNZ LOOP2
		
		POP	B	
		POP	H
		POP	PSW
		RET 

