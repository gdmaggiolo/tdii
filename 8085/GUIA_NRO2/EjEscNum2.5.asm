;******************************************************************************
;T�cnicas Digitales II
;Ejercitaci�n Nro 2.5
;Autor: Ing. Maggiolo Gustavo
;******************************************************************************

; Ejercitaci�n Nro 2.5
; Realice un programa que encuentre el menor elemento en un bloque de datos
; cuya longitud se encuentra en la posici�n de memoria 2001h y que empieza
; en la posici�n 200Ah. Los n�meros en el bloque son binarios de 8 bit sin
; signo. Almacene el elemento en la posici�n 2000h.

;******************************************************************************
;	Definici�n de Etiquetas
;******************************************************************************

.define
	BootAddr		0000h
	AddrIntRST1	0008h
	AddrIntRST2	0010h
	AddrIntRST3	0018h
	AddrIntRST4	0020h
	AddrIntTRAP	0024h
	AddrIntRST5	0028h
	AddrIntRST55	002Ch
	AddrIntRST6	0030h
	AddrIntRST65	0034h
	AddrIntRST7	0038h
	AddrIntRST75	003Ch

	STACK_ADDR	FFFFh

	IniDataROM	0540h		;Definir donde termina el Programa
	IniDataRAM	2000h		;Definir de acuerdo al Hardware

	ptrDatos		1040h
		
;******************************************************************************
;	Definici�n de Datos en RAM (Variables)
;******************************************************************************
.data	1500h
CANT:			dB	10

.data	IniDataRAM

DATA:			dB	60,50,10,10,110,10,11,43,44,3,0
	
;******************************************************************************
;	Definici�n de Datos en ROM (Constantes)
;******************************************************************************
.data	IniDataROM
	
CteMAX:	dW	03FFh
CteMIN:	dB	80h

;******************************************************************************
;	Sector de Arranque del 8085
;******************************************************************************
	.org	BootAddr
		JMP	Boot
;******************************************************************************
;	Sector del Vector de Interrupciones
;******************************************************************************
	.org	AddrIntRST1
		JMP	IntRST1
	.org	AddrIntRST2
		JMP	IntRST2
	.org	AddrIntRST3
		JMP	IntRST3
	.org	AddrIntRST4
		JMP	IntRST4
	.org	AddrIntTRAP
		JMP	IntTRAP
	.org	AddrIntRST5
		JMP	IntRST5
	.org	AddrIntRST55
		JMP	IntRST55
	.org	AddrIntRST6
		JMP	IntRST6
	.org	AddrIntRST65
		JMP	IntRST65
	.org	AddrIntRST7
		JMP	IntRST7
	.org	AddrIntRST75
		JMP	IntRST75

;******************************************************************************
;	Sector de las Interrupciones
;******************************************************************************

IntRST1:	;Ac� va el c�digo de la Interrupci�n RST1
		EI
		RET
IntRST2:	;Ac� va el c�digo de la Interrupci�n RST2
		EI
		RET
IntRST3:	;Ac� va el c�digo de la Interrupci�n RST3
		EI
		RET
IntRST4:	;Ac� va el c�digo de la Interrupci�n RST4
		EI
		RET
IntTRAP:	;Ac� va el c�digo de la Interrupci�n TRAP
		EI
		RET
IntRST5:	;Ac� va el c�digo de la Interrupci�n RST5
		EI
		RET
IntRST55:	;Ac� va el c�digo de la Interrupci�n RST5.5
		EI
		RET
IntRST6:	;Ac� va el c�digo de la Interrupci�n RST6
		EI
		RET
IntRST65:	;Ac� va el c�digo de la Interrupci�n RST6.5
		EI
		RET
IntRST7:	;Ac� va el c�digo de la Interrupci�n RST7
		EI
		RET
IntRST75:	;Ac� va el c�digo de la Interrupci�n RST7.5
		EI
		RET


;******************************************************************************
;	Sector del Programa Principal
;******************************************************************************
Boot:
	LXI	SP,STACK_ADDR	;Inicializo el Puntero de Pila
	
Main:
	
	CALL	ELMENOR
	
	HLT

;******************************************************************************
; Funci�n: ELMENOR
; Descripci�n: Encuentre el menor elemento en un bloque de datos que empieza
; 200Ah. El tama�o esta en 2001h. Guardar el menor en 2000h.
; Entrada: ninguno
; Salida: ninguno
;******************************************************************************
ELMENOR:
		PUSH	PSW
		PUSH	H
		PUSH	D

		LXI H, 200Ah  		;cargo puntero
		LDA 2001h     		;cargo longitud de los datos
		MOV D, A 
		MVI E, FFh     		;Inicio menor
LOOP:
		MOV A, M      		;leo valor
		CMP E         		;comparo con menor. Hace A-E 
		JNC NOESMENOR 		;es menor?
		MOV E, A      		;intercambio el menor con el dato leido
NOESMENOR:
		INX H         		;incremento puntero
		MOV A, D      		;muevo longitud
		ADI 10d				;sumo 10 que es el desplazamiento que tienen los datos
		CMP L         		;comparo c/ parte baja
		JNZ LOOP      		;termine: no
		MOV A, E      		;termine: si, muevo el
		STA 2000h     		;valor minimo para 
							;almacenar y retorno
		POP	D
		POP	H
		POP	PSW
		RET
