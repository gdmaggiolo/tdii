;******************************************************************************
;T�cnicas Digitales II
;Ejercitaci�n Nro 9.1
;Autor: Ing. Maggiolo Gustavo
;******************************************************************************

; Ejercitaci�n Nro 9.1
; Realizar un programa que controle un teclado matricial de 4x4 a trav�s de
; los puertos 10h de salida y 20h de entrada. Al presionar una tecla, deber�
; almacenarse en la variable "scancode".

;********************************************************************
;	Definici�n de Etiquetas
;********************************************************************

.define
	BootAddr		0000h
	AddrIntRST1		0008h
	AddrIntRST2		0010h
	AddrIntRST3		0018h
	AddrIntRST4		0020h
	AddrIntTRAP		0024h
	AddrIntRST5		0028h
	AddrIntRST55	002Ch
	AddrIntRST6		0030h
	AddrIntRST65	0034h
	AddrIntRST7		0038h
	AddrIntRST75	003Ch

	STACK_ADDR		FFFFh

	IniDataROM		0540h		;Comienzo de Constantes en ROM
	IniDataRAM		2000h		;Comienzo de Variables en RAM

	PORTIN       	20h
	PORTOUT			10h			
	
	KEY_0			81h
	KEY_1			41h
	KEY_2			21h
	KEY_3			11h
	KEY_4			82h
	KEY_5			42h
	KEY_6			22h
	KEY_7			12h
	KEY_8			84h
	KEY_9			44h
	KEY_10		24h
	KEY_11		14h
	KEY_12		88h
	KEY_13		48h
	KEY_14		28h
	KEY_15		18h
	
;********************************************************************
;	Definici�n de Datos en RAM (Variables)
;********************************************************************

.data	IniDataRAM

scancode:	dB		0
Col:		dB		0
Row:		dB		0


	
;********************************************************************
;	Definici�n de Datos en ROM (Constantes)
;********************************************************************

.data	IniDataROM
Scancode:		db	81h, 41h, 21h, 11h, 82h, 42h, 22h, 12h, 84h, 44h, 24h, 14h, 88h, 48h, 28h, 18h	
				;0  1   2   3  4   5   6   7   8   9   A   B  C   D   E   F
CteWord:		dW	03E8h
CteByte:		dB	64h
	
Texto:		dB	'C','a','d','e','n','a',0

;********************************************************************
;	Sector de Arranque del 8085
;********************************************************************
	.org	BootAddr
		JMP	Boot
;********************************************************************
;	Sector del Vector de Interrupciones
;********************************************************************
	.org	AddrIntRST1
		JMP	IntRST1
	.org	AddrIntRST2
		JMP	IntRST2
	.org	AddrIntRST3
		JMP	IntRST3
	.org	AddrIntRST4
		JMP	IntRST4
	.org	AddrIntTRAP
		JMP	IntTRAP
	.org	AddrIntRST5
		JMP	IntRST5
	.org	AddrIntRST55
		JMP	IntRST55
	.org	AddrIntRST6
		JMP	IntRST6
	.org	AddrIntRST65
		JMP	IntRST65
	.org	AddrIntRST7
		JMP	IntRST7
	.org	AddrIntRST75
		JMP	IntRST75
;********************************************************************
;	Sector de las Interrupciones
;********************************************************************
IntRST1:
		;Ac� va el c�digo de la Interrupci�n RST1
		EI
		RET
IntRST2:
		;Ac� va el c�digo de la Interrupci�n RST2
		EI
		RET
IntRST3:
		;Ac� va el c�digo de la Interrupci�n RST3
		EI
		RET
IntRST4:
		;Ac� va el c�digo de la Interrupci�n RST4
		EI
		RET
IntTRAP:
		;Ac� va el c�digo de la Interrupci�n TRAP
		EI
		RET
IntRST5:
		;Ac� va el c�digo de la Interrupci�n RST5
		EI
		RET
IntRST55:
		;Ac� va el c�digo de la Interrupci�n RST5.5
		CALL	SCANKBD
		STA		scancode
		EI
		RET
IntRST6:
		;Ac� va el c�digo de la Interrupci�n RST6
		EI
		RET
IntRST65:
		;Ac� va el c�digo de la Interrupci�n RST6.5
		EI
		RET
IntRST7:
		;Ac� va el c�digo de la Interrupci�n RST7
		EI
		RET
IntRST75:
		;Ac� va el c�digo de la Interrupci�n RST7.5
		EI
		RET
;********************************************************************
;	Sector del Programa Principal
;********************************************************************
Boot:
	LXI	SP,STACK_ADDR	;Inicializo el Puntero de Pila
	
Main:
	
Loop:

	CALL	SCANKBD
	STA		scancode

	JMP Loop

;******************************************************************************
; Funci�n: SCANKBD
; Descripci�n: Escanea un teclado Matricial
; Entrada: ninguna
; Salida: A - Tecla presionada (SCAN_CODE)	 
;******************************************************************************
SCANKBD:
	PUSH	B
	
	XRA	A
	MVI	B,80h		; 1er Columna	B = 80h
	MOV	A,B
	OUT	PORTOUT		; Activo 1er columna
	CALL	Filas		; Veo las filas si estan activas.
	JNZ	VerCol2		; Si no detecto tecla, paso a la proxima Columna.
	ORA	B			; Conformo el SCANCODE, entre el 80h del reg. B y el A que puede ser 1,2,4 u 8.
	JMP	FinCol
VerCol2:
	MVI	B,40h		; 2da Columna	B = 40h
	MOV	A,B
	OUT	PORTOUT		; Activo 2da columna
	CALL	Filas		; Veo las filas si estan activas.
	JNZ	VerCol3		; Si no detecto tecla, paso a la proxima Columna.
	ORA	B
	JMP	FinCol
VerCol3:
	MVI	B,20h		; 3er Columna	B = 20h
	MOV	A,B
	OUT	PORTOUT		; Activo 3er columna
	CALL	Filas		; Veo las filas si estan activas.
	JNZ	VerCol4		; Si no detecto tecla, paso a la proxima Columna.
	ORA	B
	JMP	FinCol
VerCol4:
	MVI	B,10h			; 4ta Columna	B = 10h
	MOV	A,B
	OUT	PORTOUT
	CALL	Filas
	JNZ	FinCol		; Si no detecto tecla, termina la rutina.
	ORA	B
FinCol:
	POP	B
	RET

Filas:
	IN	PORTIN
	ANI	0Fh
	CPI	1			; 1er Fila
	JNZ	VerFila2
	;MVI	A,1		; No muevo el valor, porque ya esta ese valor en el A.
	JMP	FinFil
VerFila2:
	CPI	2			; 2da Fila
	JNZ	VerFila3
	;MVI	A,2		; No muevo el valor, porque ya esta ese valor en el A.
	JMP	FinFil
VerFila3:
	CPI	4			; 3er Fila
	JNZ	VerFila4
	;MVI	A,4		; No muevo el valor, porque ya esta ese valor en el A.
	JMP	FinFil
VerFila4:
	CPI	8			; 4ta Fila
	JNZ	FinFil
	;MVI	A,8		; No muevo el valor, porque ya esta ese valor en el A.
FinFil:
	RET
	

	HLT
	