;******************************************************************************
;T�cnicas Digitales II
;Ejercitaci�n Nro 9.3
;Autor: Ing. Maggiolo Gustavo
;******************************************************************************

; Ejercitaci�n Nro 9.3
; Realizar un programa que controle un teclado matricial de 4x4 por medio de
; una PPI, ubicada en la direcci�n 20h. Cada tecla presionada se deber�
; mostrar, en exadecimal, en un display de siete segmentos, en un puerto
; acorde.

;********************************************************************
;	Definici�n de Etiquetas
;********************************************************************

.define
	BootAddr		0000h
	AddrIntRST1		0008h
	AddrIntRST2		0010h
	AddrIntRST3		0018h
	AddrIntRST4		0020h
	AddrIntTRAP		0024h
	AddrIntRST5		0028h
	AddrIntRST55	002Ch
	AddrIntRST6		0030h
	AddrIntRST65	0034h
	AddrIntRST7		0038h
	AddrIntRST75	003Ch

	STACK_ADDR		FFFFh

	IniDataROM		0540h		;Comienzo de Constantes en ROM
	IniDataRAM		2000h		;Comienzo de Variables en RAM
	
	ADDRA       	20h
	ADDRB       	ADDRA+1
	ADDRC       	ADDRA+2
	ADDRRC      	ADDRA+3

	PORTOUT			ADDRC
	PORTIN			ADDRC
	
	KEY_0			81h
	KEY_1			41h
	KEY_2			21h
	KEY_3			11h
	KEY_4			82h
	KEY_5			42h
	KEY_6			22h
	KEY_7			12h
	KEY_8			84h
	KEY_9			44h
	KEY_10			24h
	KEY_11			14h
	KEY_12			88h
	KEY_13			48h
	KEY_14			28h
	KEY_15			18h
	
	sega			02h
	segb			04h
	segc			40h
	segd			20h
	sege			10h
	segf			01h
	segg			08h

;		a			a: 02h	
;	-------------	b: 04h
;	|	     	|	c: 40h	 
;	| 	   		|	d: 20h
;  f|  	   		|b	e: 10h
;	|  			|	f: 01h
;	| 			|	g: 08h
;	------g------	
;	|    		|	
;	|  	 		|	
;  e|  	  		|c	
;	|			|	
;	|        	| 	
;	------------- 	
;		d			

; PORTA
; |-------------------------------------------------------
; |  b7 |  b6  |  b5  |  b4  |  b3  |  b2  |  b1  |  b0  |
; | pto | segc | segd | sege | segg | segb | sega | segf |
; |-------------------------------------------------------

;********************************************************************
;	Definici�n de Datos en RAM (Variables)
;********************************************************************

.data	IniDataRAM

scancode:	dB		0
teclahex:	dB		0
Col:		dB		0
Row:		dB		0

;********************************************************************
;	Definici�n de Datos en ROM (Constantes)
;********************************************************************

.data	IniDataROM
Numero0:		db sega|segb|segc|segd|sege|segf			; 0
Numero1:		db segb|segc							; 1
Numero2:		db sega|segb|segg|sege|segd				; 2
Numero3:		db sega|segb|segc|segd|segg				; 3
Numero4:		db segf|segg|segb|segc					; 4
Numero5:		db sega|segf|segg|segc|segd				; 5
Numero6:		db segf|sege|segd|segc|segg				; 6
Numero7:		db sega|segb|segc						; 7
Numero8:		db sega|segb|segc|segd|sege|segf|segg		; 8
Numero9:		db sega|segb|segc|segg|segf				; 9
Numero10:		db sega|segb|segc|sege|segf|segg			; A
Numero11:		db segc|segd|sege|segf|segg				; b
Numero12:		db sega|segd|sege|segf					; C
Numero13:		db segb|segc|segd|sege|segg				; d
Numero14:		db sega|segd|sege|segf|segg				; E
Numero15:		db sega|sege|segf|segg					; F

;********************************************************************
;	Sector de Arranque del 8085
;********************************************************************
	.org	BootAddr
		JMP	Boot
;********************************************************************
;	Sector del Vector de Interrupciones
;********************************************************************
	.org	AddrIntRST1
		JMP	IntRST1
	.org	AddrIntRST2
		JMP	IntRST2
	.org	AddrIntRST3
		JMP	IntRST3
	.org	AddrIntRST4
		JMP	IntRST4
	.org	AddrIntTRAP
		JMP	IntTRAP
	.org	AddrIntRST5
		JMP	IntRST5
	.org	AddrIntRST55
		JMP	IntRST55
	.org	AddrIntRST6
		JMP	IntRST6
	.org	AddrIntRST65
		JMP	IntRST65
	.org	AddrIntRST7
		JMP	IntRST7
	.org	AddrIntRST75
		JMP	IntRST75
;********************************************************************
;	Sector de las Interrupciones
;********************************************************************
IntRST1:
		;Ac� va el c�digo de la Interrupci�n RST1
		EI
		RET
IntRST2:
		;Ac� va el c�digo de la Interrupci�n RST2
		EI
		RET
IntRST3:
		;Ac� va el c�digo de la Interrupci�n RST3
		EI
		RET
IntRST4:
		;Ac� va el c�digo de la Interrupci�n RST4
		EI
		RET
IntTRAP:
		;Ac� va el c�digo de la Interrupci�n TRAP
		EI
		RET
IntRST5:
		;Ac� va el c�digo de la Interrupci�n RST5
		EI
		RET
IntRST55:
		;Ac� va el c�digo de la Interrupci�n RST5.5
		EI
		RET
IntRST6:
		;Ac� va el c�digo de la Interrupci�n RST6
		EI
		RET
IntRST65:
		;Ac� va el c�digo de la Interrupci�n RST6.5
		EI
		RET
IntRST7:
		;Ac� va el c�digo de la Interrupci�n RST7
		EI
		RET
IntRST75:
		;Ac� va el c�digo de la Interrupci�n RST7.5
		EI
		RET
;********************************************************************
;	Sector del Programa Principal
;********************************************************************
Boot:
	LXI	SP,STACK_ADDR	;Inicializo el Puntero de Pila
	
Main:
	CALL	Init_PPI
Loop:

	CALL	SCANKBD
	STA		scancode
	CALL	CODSCANTOHEX
	STA		teclahex
	CALL	HEX7SEG
	OUT		ADDRA

	JMP Loop
;******************************************************************************
; Funci�n: Init_PPI
; Descripci�n: Inicializa la PPI
; Entrada: ninguna
; Salida: ninguna	 
;******************************************************************************
Init_PPI:
	MVI A,10000011B ; Conf. PPI MODO 0
	OUT ADDRRC      ; A:OUT, B:IN, Ch:OUT, Cl:IN
	RET
	
;******************************************************************************
; Funci�n: CODSCANTOHEX
; Descripci�n: Convierte el Codigo scan de una tecla en su representaci�n HEX
; Entrada: A (Code scan)
; Salida: A (N�mero Hexadecimal)	 
;******************************************************************************
CODSCANTOHEX:
	CPI	KEY_0
	JNZ	NOKEY_0
	MVI	A,0
	RET
NOKEY_0:
	CPI	KEY_1
	JNZ	NOKEY_1
	MVI	A,1
	RET
NOKEY_1:
	CPI	KEY_2
	JNZ	NOKEY_2
	MVI	A,2
	RET
NOKEY_2:
	CPI	KEY_3
	JNZ	NOKEY_3
	MVI	A,3
	RET
NOKEY_3:	
	CPI	KEY_4
	JNZ	NOKEY_4
	MVI	A,4
	RET
NOKEY_4:
	CPI	KEY_5
	JNZ	NOKEY_5
	MVI	A,5
	RET
NOKEY_5:
	CPI	KEY_6
	JNZ	NOKEY_6
	MVI	A,6
	RET
NOKEY_6:
	CPI	KEY_7
	JNZ	NOKEY_7
	MVI	A,7
	RET
NOKEY_7:	
	CPI	KEY_8
	JNZ	NOKEY_8
	MVI	A,8
	RET
NOKEY_8:	
	CPI	KEY_9
	JNZ	NOKEY_9
	MVI	A,9
	RET
NOKEY_9:
	CPI	KEY_10
	JNZ	NOKEY_10
	MVI	A,10
	RET
NOKEY_10:
	CPI	KEY_11
	JNZ	NOKEY_11
	MVI	A,11
	RET
NOKEY_11:
	CPI	KEY_12
	JNZ	NOKEY_12
	MVI	A,12
	RET
NOKEY_12:
	CPI	KEY_13
	JNZ	NOKEY_13
	MVI	A,13
	RET
NOKEY_13:
	CPI	KEY_14
	JNZ	NOKEY_14
	MVI	A,14
	RET
NOKEY_14:
	CPI	KEY_15
	JNZ	NOKEY_15
	MVI	A,15
	RET
NOKEY_15:
	XRA	A
	CMA
	RET

;******************************************************************************
; Funci�n: HEX7SEG
; Descripci�n: Convierte un n�mero HEX a 7 segmentos.
; Entrada: A (n�mero HEX a convertir)
; Salida: A (representaci�n en 7 segmentos)	 
;******************************************************************************
HEX7SEG:
	CPI	0
	JNZ	NO0
	LDA	Numero0
	RET
NO0:	CPI	1
	JNZ	NO1
	LDA	Numero1
	RET
NO1:	CPI	2
	JNZ	NO2
	LDA	Numero2
	RET
NO2:	CPI	3
	JNZ	NO3
	LDA	Numero3
	RET
NO3:	CPI	4
	JNZ	NO4
	LDA	Numero4
	RET
NO4:	CPI	5
	JNZ	NO5
	LDA	Numero5
	RET
NO5:	CPI	6
	JNZ	NO6
	LDA	Numero6
	RET
NO6:	CPI	7
	JNZ	NO7
	LDA	Numero7
	RET
NO7:	CPI	8
	JNZ	NO8
	LDA	Numero8
	RET
NO8:	CPI	9
	JNZ	NO9
	LDA	Numero9
	RET
NO9:	CPI	10
	JNZ	NO10
	LDA	Numero10
	RET
NO10:	
	CPI	11
	JNZ	NO11
	LDA	Numero11
	RET
NO11:	
	CPI	12
	JNZ	NO12
	LDA	Numero12
	RET
NO12:	
	CPI	13
	JNZ	NO13
	LDA	Numero13
	RET
NO13:	
	CPI	14
	JNZ	NO14
	LDA	Numero14
	RET
NO14:	
	CPI	15
	JNZ	NO15
	LDA	Numero15
	RET
NO15:
	XRA	A
	RET

;******************************************************************************
; Funci�n: SCANKBD
; Descripci�n: Escanea un teclado Matricial
; Entrada: ninguna
; Salida: A - Tecla presionada (SCAN_CODE)	 
;******************************************************************************
SCANKBD:
	PUSH	B
	
	XRA	A
	MVI	B,80h		; 1er Columna	B = 80h
	MOV	A,B
	OUT	PORTOUT		; Activo 1er columna
	CALL	Filas		; Veo las filas si estan activas.
	JNZ	VerCol2		; Si no detecto tecla, paso a la proxima Columna.
	ORA	B			; Conformo el SCANCODE, entre el 80h del reg. B y el A que puede ser 1,2,4 u 8.
	JMP	FinCol
VerCol2:
	MVI	B,40h		; 2da Columna	B = 40h
	MOV	A,B
	OUT	PORTOUT		; Activo 2da columna
	CALL	Filas		; Veo las filas si estan activas.
	JNZ	VerCol3		; Si no detecto tecla, paso a la proxima Columna.
	ORA	B
	JMP	FinCol
VerCol3:
	MVI	B,20h		; 3er Columna	B = 20h
	MOV	A,B
	OUT	PORTOUT		; Activo 3er columna
	CALL	Filas		; Veo las filas si estan activas.
	JNZ	VerCol4		; Si no detecto tecla, paso a la proxima Columna.
	ORA	B
	JMP	FinCol
VerCol4:
	MVI	B,10h			; 4ta Columna	B = 10h
	MOV	A,B
	OUT	PORTOUT
	CALL	Filas
	JNZ	FinCol		; Si no detecto tecla, termina la rutina.
	ORA	B
FinCol:
	POP	B
	RET

Filas:
	IN	PORTIN
	ANI	0Fh
	CPI	1			; 1er Fila
	JNZ	VerFila2
	;MVI	A,1		; No muevo el valor, porque ya esta ese valor en el A.
	JMP	FinFil
VerFila2:
	CPI	2			; 2da Fila
	JNZ	VerFila3
	;MVI	A,2		; No muevo el valor, porque ya esta ese valor en el A.
	JMP	FinFil
VerFila3:
	CPI	4			; 3er Fila
	JNZ	VerFila4
	;MVI	A,4		; No muevo el valor, porque ya esta ese valor en el A.
	JMP	FinFil
VerFila4:
	CPI	8			; 4ta Fila
	JNZ	FinFil
	;MVI	A,8		; No muevo el valor, porque ya esta ese valor en el A.
FinFil:
	RET
	
	HLT
	