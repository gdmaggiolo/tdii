;******************************************************************************
;T�cnicas Digitales II
;Ejemplo para Dividir 8 bit por 8 bit (DIV8x8)
;Ing. Maggiolo Gustavo
;******************************************************************************

; Funci�n: DIV8x8
; Realizar un programa que divida los n�meros, en binario, ubicados 
; en las posiciones de memoria 1000h, 1001h de la
; siguiente forma:
;	Dividendo		-->	1000h
;	Divisor		-->	1001h
;	Resultado 		-->	1002h
;	Resto			-->	1003h
; 	DivErr 		--> 	1004h

;******************************************************************************
;	Definici�n de Etiquetas
;******************************************************************************

.define
	BootAddr		0000h
	AddrIntRST1		0008h
	AddrIntRST2		0010h
	AddrIntRST3		0018h
	AddrIntRST4		0020h
	AddrIntTRAP		0024h
	AddrIntRST5		0028h
	AddrIntRST55	002Ch
	AddrIntRST6		0030h
	AddrIntRST65	0034h
	AddrIntRST7		0038h
	AddrIntRST75	003Ch

	STACK_ADDR		FFFFh

	IniDataROM		0540h
	IniDataRAM		2000h

	ptrDatos		1040h

	ErrNUMcero		80h
	ErrDENcero		01h
	
	
;******************************************************************************
;	Definici�n de Datos en RAM (Variables)
;******************************************************************************
.data	IniDataRAM
	
Dividendo:	dB		0		;Inicializo la variable en 0
Divisor:	dB		0
Resultado:	dB		0
Resto:	dB		0
DivErr:	dB		0

;******************************************************************************
;	Definici�n de Datos en ROM (Constantes)
;******************************************************************************
.data	IniDataROM
	
CteWord:		dW	03E8h
CteByte:		dB	64h
		
		
Texto:		dB	'C','a','d','e','n','a',0



;******************************************************************************
;	Sector de Arranque del 8085
;******************************************************************************
	.org	BootAddr
		JMP	Boot

;******************************************************************************
;	Sector del Vector de Interrupciones
;******************************************************************************
	.org	AddrIntRST1
		JMP	IntRST1
	.org	AddrIntRST2
		JMP	IntRST2
	.org	AddrIntRST3
		JMP	IntRST3
	.org	AddrIntRST4
		JMP	IntRST4
	.org	AddrIntTRAP
		JMP	IntTRAP
	.org	AddrIntRST5
		JMP	IntRST5
	.org	AddrIntRST55
		JMP	IntRST55
	.org	AddrIntRST6
		JMP	IntRST6
	.org	AddrIntRST65
		JMP	IntRST65
	.org	AddrIntRST7
		JMP	IntRST7
	.org	AddrIntRST75
		JMP	IntRST75

;******************************************************************************
;	Sector de las Interrupciones
;******************************************************************************

IntRST1:
		;Ac� va el c�digo de la Interrupci�n RST1
		EI
		RET
IntRST2:
		;Ac� va el c�digo de la Interrupci�n RST2
		EI
		RET
IntRST3:
		;Ac� va el c�digo de la Interrupci�n RST3
		EI
		RET
IntRST4:
		;Ac� va el c�digo de la Interrupci�n RST4
		EI
		RET
IntTRAP:
		;Ac� va el c�digo de la Interrupci�n TRAP
		EI
		RET
IntRST5:
		;Ac� va el c�digo de la Interrupci�n RST5
		EI
		RET
IntRST55:
		;Ac� va el c�digo de la Interrupci�n RST5.5
		EI
		RET
IntRST6:
		;Ac� va el c�digo de la Interrupci�n RST6
		EI
		RET
IntRST65:
		;Ac� va el c�digo de la Interrupci�n RST6.5
		EI
		RET
IntRST7:
		;Ac� va el c�digo de la Interrupci�n RST7
		EI
		RET
IntRST75:
		;Ac� va el c�digo de la Interrupci�n RST7.5
		EI
		RET



;******************************************************************************
;	Sector del Programa Principal
;******************************************************************************
Boot:
	LXI	SP,STACK_ADDR	;Inicializo el Puntero de Pila
	
Main:
	MVI	A,80
	STA	Dividendo
	MVI	A,20
	STA	Divisor
	CALL	DIV8x8
	
	;JMP	Main
	
	HLT
;******************************************************************************
; Funci�n: DIV8x8
; Descripci�n: Divide un par de datos de 8 bit almacenados en Dividendo
;		   y Divisor y guarda el resultado en la variable Resultado y el 
;		   resto en la variable Resto.
; Retorno: 00h - No hay Error
;	     01h - Error de Divisi�n por Cero
;	     80h - Dividendo cero
;******************************************************************************
DIV8x8:
		PUSH	H
		PUSH	B
		PUSH	PSW
	
		XRA	A
		MOV	B,A
		LXI	H,Resultado
		
		LDA	Dividendo
		DCR	A
		JM	NumNulo
		INR	A
		STA	Dividendo

		LDA	Divisor
		DCR	A
		JZ	DivUno
		JM	DivCero
		INR	A
		
		
		MOV	B,A
		LDA	Dividendo
Loop:
		CMP	B		;Comparo A - B (Dividendo - divisor)
		JC	NoDiv
		SUB	B
		INR	M
						
		JMP	Loop
NoDiv:
		JMP	Salir
DivUno:
		JMP	Salir
DivCero:	
		MVI	A,ErrDENcero
		STA	DivErr
		JMP	SalirErr
NumNulo:
		MVI	A,ErrNUMcero
		STA	DivErr
		JMP	SalirErr
		
Salir:	MVI	A,0
		STA	DivErr
SalirErr:
		POP PSW
		POP B
		POP H

		RET
