;******************************************************************************
;T�cnicas Digitales II
;Ejemplo para Multiplicar 8 bit por 8 bit (MULT8x8) con instrucci�n DAD
;Ing. Maggiolo Gustavo
;******************************************************************************

; Funci�n: MULT8x8
; Realizar un programa que multiplique los n�meros, en binario, ubicados 
; en las posiciones de memoria 1010h, 1011h de la
; siguiente forma:
;	Multiplicando (MTCDO)	-->	2010h
;	Multiplicador (MTDOR)	-->	2011h
;	Resultado LSB		-->	2012h
;	Resultado MSB		-->	2013h


;******************************************************************************
;	Definici�n de Etiquetas
;******************************************************************************

.define
	BootAddr		0000h
	AddrIntRST1		0008h
	AddrIntRST2		0010h
	AddrIntRST3		0018h
	AddrIntRST4		0020h
	AddrIntTRAP		0024h
	AddrIntRST5		0028h
	AddrIntRST55	002Ch
	AddrIntRST6		0030h
	AddrIntRST65	0034h
	AddrIntRST7		0038h
	AddrIntRST75	003Ch

	STACK_ADDR		FFFFh

	IniDataROM		0540h
	IniDataRAM		2010h

	ptrDatos		1040h

	
	
;******************************************************************************
;	Definici�n de Datos en RAM (Variables)
;******************************************************************************
.data	IniDataRAM
	
MTCDO:		dB		F3h	;Ini. la variable Multiplicando en 0 (BYTE)
MTDOR:		dB		D3h	;Ini. la variable Multiplicador en 0 (BYTE)
RES_LSB:		dW		0	;Ini. la variable Resultado en 0 (WORD)


;******************************************************************************
;	Definici�n de Datos en ROM (Constantes)
;******************************************************************************
.data	IniDataROM
	
CteWord:		dW	03E8h
CteByte:		dB	64h
		
		
Texto:		dB	'C','a','d','e','n','a',0



;******************************************************************************
;	Sector de Arranque del 8085
;******************************************************************************
	.org	BootAddr
		JMP	Boot

;******************************************************************************
;	Sector del Vector de Interrupciones
;******************************************************************************
	.org	AddrIntRST1
		JMP	IntRST1
	.org	AddrIntRST2
		JMP	IntRST2
	.org	AddrIntRST3
		JMP	IntRST3
	.org	AddrIntRST4
		JMP	IntRST4
	.org	AddrIntTRAP
		JMP	IntTRAP
	.org	AddrIntRST5
		JMP	IntRST5
	.org	AddrIntRST55
		JMP	IntRST55
	.org	AddrIntRST6
		JMP	IntRST6
	.org	AddrIntRST65
		JMP	IntRST65
	.org	AddrIntRST7
		JMP	IntRST7
	.org	AddrIntRST75
		JMP	IntRST75

;******************************************************************************
;	Sector de las Interrupciones
;******************************************************************************

IntRST1:
		;Ac� va el c�digo de la Interrupci�n RST1
		EI
		RET
IntRST2:
		;Ac� va el c�digo de la Interrupci�n RST2
		EI
		RET
IntRST3:
		;Ac� va el c�digo de la Interrupci�n RST3
		EI
		RET
IntRST4:
		;Ac� va el c�digo de la Interrupci�n RST4
		EI
		RET
IntTRAP:
		;Ac� va el c�digo de la Interrupci�n TRAP
		EI
		RET
IntRST5:
		;Ac� va el c�digo de la Interrupci�n RST5
		EI
		RET
IntRST55:
		;Ac� va el c�digo de la Interrupci�n RST5.5
		EI
		RET
IntRST6:
		;Ac� va el c�digo de la Interrupci�n RST6
		EI
		RET
IntRST65:
		;Ac� va el c�digo de la Interrupci�n RST6.5
		EI
		RET
IntRST7:
		;Ac� va el c�digo de la Interrupci�n RST7
		EI
		RET
IntRST75:
		;Ac� va el c�digo de la Interrupci�n RST7.5
		EI
		RET
;******************************************************************************
;	Sector del Programa Principal
;******************************************************************************
Boot:
	LXI	SP,STACK_ADDR	;Inicializo el Puntero de Pila
	
Main:
	;Multiplica F3h x D3h = C849h
	CALL	MULT8x8
		
	HLT
;***********************************
; Funci�n: MULT8x8 
; Descripci�n: Multiplica un par de 
; datos de 8 bit 
; Entrada: MTCDO, MTDOR
; Salida: RES_LSB
;***********************************
MULT8x8:
	PUSH	H
	PUSH	D
	PUSH	PSW
	
	XRA	A
	MOV	L,A
	MOV	H,A
	MOV	D,A
	MOV	E,A
	STA	RES_LSB
	STA	RES_LSB+1

	LDA 	MTCDO			; Cargo MTCDO
	MOV	E,A			; E <-- MTCDO
	LDA	MTDOR			; Cargo MTDOR
Loop:
	DCR 	A 			;Decremento MTDOR
	JM 	Termine 		;Salto si es negativo
	DAD 	D			;H-L <- H-L + D-E
	JMP	Loop
Termine:
	MOV 	A,L     			;Almaceno RES_LSB
	STA 	RES_LSB
	MOV 	A,H     			;Almaceno RES_MSB
	STA 	RES_LSB+1
		
	POP PSW
	POP D
	POP H
	RET