;******************************************************************************
;T�cnicas Digitales II
;Ejercitaci�n Nro 8.3
;Autor: Ing. Maggiolo Gustavo
;******************************************************************************

; Ejercitaci�n Nro 8.3
; Se dispone de un sistema con microprocesador 8085 y una USART 8251. Adem�s
; el sistema cuenta con un puerto de entrada en la direcci�n 74h. Cada vez que
; el microprocesador tiene un dato nuevo en el puerto de entrada, se produce
; una interrupci�n RST5.5. El microprocesador deber� leer el dato y 
; retransmitirlo en forma serial por el puerto 20h. El formato de la 
; comunicaci�n es 8N1, 9600 baudios.

;********************************************************************
;	Definici�n de Etiquetas
;********************************************************************

.define
	BootAddr			0000h
	AddrIntRST1		0008h
	AddrIntRST2		0010h
	AddrIntRST3		0018h
	AddrIntRST4		0020h
	AddrIntTRAP		0024h
	AddrIntRST5		0028h
	AddrIntRST55		002Ch
	AddrIntRST6		0030h
	AddrIntRST65		0034h
	AddrIntRST7		0038h
	AddrIntRST75		003Ch

	STACK_ADDR		FFFFh

	IniDataROM		0540h		;Comienzo de Constantes en ROM
	IniDataRAM		2000h		;Comienzo de Variables en RAM
	
	MaskSetEn			8
	M75				4
	M65				2
	M55				1
	
	BAUDRATEx1		1		; x1
	BAUDRATEx16		2		; x16
	BAUDRATEx64		3		; x64
	CHLENG5			0		; 5
	CHLENG6			4		; 6
	CHLENG7			8		; 7
	CHLENG8			0Ch		; 8
	PARENB			10h		; Parity Enabled Bit
	PEVEN			20h		; Paridad PAR
	PODD			0		; Paridad IMPAR
	STOPBIT1			40h		; 1 BIT STOP
	STOPBIT15			80h		; 1,5 BIT STOP
	STOPBIT2			C0h		; 2 BIT STOP
	
	TxEN				1		; Tx Enabled
	DTREN			2		; Data Terminal Ready Enabled
	RxEN				4		; Rx Enabled
	SendBRK			8		; Send Break character
	ErrRST			10h		; Error RESET
	RTSEN			20h		; Request to Send Enabled	
	INTRST			40h		; Internal RESET
	EnHM			80h		; Enabled hunt mode (sync)
	ADDRDATA		20h
	ADDRCMD			21h
	ADDRPORTIN		74h

;********************************************************************
;	Definici�n de Datos en RAM (Variables)
;********************************************************************
.data	IniDataRAM
Flags:		dB		0	
Buffer:		dB		0
ErrUSART:		dB		0
CNTPERR:		dB		0
CNTOERR:		dB		0
CNTFERR:		dB		0
	
;********************************************************************
;	Definici�n de Datos en ROM (Constantes)
;********************************************************************
.data	IniDataROM

;********************************************************************
;	Sector de Arranque del 8085
;********************************************************************
	.org	BootAddr
		JMP	Boot
;********************************************************************
;	Sector del Vector de Interrupciones
;********************************************************************
	.org	AddrIntRST1
		JMP	IntRST1
	.org	AddrIntRST2
		JMP	IntRST2
	.org	AddrIntRST3
		JMP	IntRST3
	.org	AddrIntRST4
		JMP	IntRST4
	.org	AddrIntTRAP
		JMP	IntTRAP
	.org	AddrIntRST5
		JMP	IntRST5
	.org	AddrIntRST55
		JMP	IntRST55
	.org	AddrIntRST6
		JMP	IntRST6
	.org	AddrIntRST65
		JMP	IntRST65
	.org	AddrIntRST7
		JMP	IntRST7
	.org	AddrIntRST75
		JMP	IntRST75
;********************************************************************
;	Sector de las Interrupciones
;********************************************************************
IntRST1:
		;Ac� va el c�digo de la Interrupci�n RST1
		EI
		RET
IntRST2:
		;Ac� va el c�digo de la Interrupci�n RST2
		EI
		RET
IntRST3:
		;Ac� va el c�digo de la Interrupci�n RST3
		EI
		RET
IntRST4:
		;Ac� va el c�digo de la Interrupci�n RST4
		EI
		RET
IntTRAP:
		;Ac� va el c�digo de la Interrupci�n TRAP
		EI
		RET
IntRST5:
		;Ac� va el c�digo de la Interrupci�n RST5
		EI
		RET
IntRST55:
		;Ac� va el c�digo de la Interrupci�n RST5.5
		PUSH	PSW

		IN		ADDRPORTIN
		STA		Buffer
		LDA	 	Flags
		ORI		1
		STA		Flags
		
		POP		PSW
		EI
		RET
IntRST6:
		;Ac� va el c�digo de la Interrupci�n RST6
		EI
		RET
IntRST65:
		;Ac� va el c�digo de la Interrupci�n RST6.5
		EI
		RET
IntRST7:
		;Ac� va el c�digo de la Interrupci�n RST7
		EI
		RET
IntRST75:
		;Ac� va el c�digo de la Interrupci�n RST7.5
		EI
		RET
;********************************************************************
;	Sector del Programa Principal
;********************************************************************
Boot:
	LXI		SP,STACK_ADDR	;Inicializo el Puntero de Pila
	
	CALL	Init_USART

	MVI		A , MaskSetEn | M75 | M65		;0Eh para las interrupciones	
	SIM										;Inicializo las interrupciones
	EI
	
	
Loop:	
	LDA		Flags
	ANI		1
	CNZ		SEND_BUFFER
	JMP 	Loop

SEND_BUFFER:
	IN		ADDRCMD
	ANI		1
	JZ		TxNotReady
	LDA		Buffer
	CALL	SEND_USART
	LDA	 	Flags
	ANI		FEh
	STA		Flags
		
TxNotReady:
	RET

;******************************************************************************
; Funci�n: Init_USART
; Descripci�n: Inicializa la USART
; Entrada: ninguna
; Salida: ninguna	 
;******************************************************************************
Init_USART:
	
	; MODO: 8b,Sin P, 1s, velocidad x1
	MVI		A , STOPBIT1|CHLENG8|BAUDRATEx1
	OUT		ADDRCMD
		
	; COMMAND: Enabled Tx, Error RESET
	MVI		A , ErrRST|TxEN
	OUT		ADDRCMD

	RET

;******************************************************************************
; Funci�n: SEND_USART
; Descripci�n: Env�a el dato por el puerto serie.
; Entrada: A (Dato a transmitir)
; Salida: ninguna	 
;******************************************************************************
SEND_USART:
	OUT	ADDRDATA
	RET
	
;******************************************************************************
; Funci�n: ERRORs
; Descripci�n: Cuenta los errores de Paridad, Sobreescritura y Trama
; Entrada: ninguna
; Salida: ninguna	 
;******************************************************************************
ERRORs:
	PUSH	PSW
	
	CALL	GET_ERROR
	STA		ErrUSART
	ANI		08h
	CNZ		Perror
	LDA		ErrUSART
	ANI		10h
	CNZ		Oerror
	LDA		ErrUSART
	ANI		20h
	CNZ		Ferror
	
	PUSH	PSW
	RET

;******************************************************************************
; Funci�n: Perror
; Descripci�n: Incrementa contador errores de Paridad
; Entrada: ninguna
; Salida: ninguna	 
;******************************************************************************
Perror:
	LDA		CNTPERR
	INR		A
	STA		CNTPERR
	RET
;******************************************************************************
; Funci�n: Oerror
; Descripci�n: Incrementa contador errores de Sobreescritura 
; Entrada: ninguna
; Salida: ninguna	 
;******************************************************************************
Oerror:
	LDA		CNTOERR
	INR		A
	STA		CNTOERR
	RET
;******************************************************************************
; Funci�n: Ferror
; Descripci�n: Incrementa contador errores de Trama 
; Entrada: ninguna
; Salida: ninguna	 
;******************************************************************************
Ferror:
	LDA		CNTFERR
	INR		A
	STA		CNTFERR
	RET

;******************************************************************************
; Funci�n: GET_ERROR
; Descripci�n: Obtiene los bits de error.
; Entrada: ninguna
; Salida: A (Errores)	 
;******************************************************************************
GET_ERROR:
	IN		ADDRCMD
	ANI		38h
	RET

	HLT
	