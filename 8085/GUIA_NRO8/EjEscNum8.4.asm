;******************************************************************************
;T�cnicas Digitales II
;Ejercitaci�n Nro 8.4
;Autor: Ing. Maggiolo Gustavo
;******************************************************************************

; Ejercitaci�n Nro 8.4
; En un sistema, implementado a partir de un microprocesador 8085, se dispone
; de una USART 8251 en la direcci�n 8F00h. Se requiere que el sistema reciba
; los datos que llegan por medio de la USART y los almacene a partir de la 
; direcci�n 8000h. El fin de datos puede ser: por la llegada del n�mero 0Dh,
; o porque no hay m�s espacio en RAM. El sistema deber� almacenar, a 
; partir de la direcci�n 7500h y en direcciones consecutivas la cantidad
; de errores por paridad, por sobreescritura y por trama. El formato
; de la comunicaci�n es 8O2, 9600 baudios.

;********************************************************************
;	Definici�n de Etiquetas
;********************************************************************

.define
	BootAddr			0000h
	AddrIntRST1		0008h
	AddrIntRST2		0010h
	AddrIntRST3		0018h
	AddrIntRST4		0020h
	AddrIntTRAP		0024h
	AddrIntRST5		0028h
	AddrIntRST55		002Ch
	AddrIntRST6		0030h
	AddrIntRST65		0034h
	AddrIntRST7		0038h
	AddrIntRST75		003Ch

	STACK_ADDR		FFFFh

	IniDataROM		0540h		;Comienzo de Constantes en ROM
	IniDataRAM		6000h		;Comienzo de Variables en RAM
	
	INITERRORUSART	7500h
	INITBUFFER		8000h
	USARTADDR		8F00h		;Direcci�n de la USART
	
	MaskSetEn			8
	M75				4
	M65				2
	M55				1
	
	BAUDRATEx1		1		; x1
	BAUDRATEx16		2		; x16
	BAUDRATEx64		3		; x64
	CHLENG5			0		; 5
	CHLENG6			4		; 6
	CHLENG7			8		; 7
	CHLENG8			0Ch		; 8
	PARENB			10h		; Parity Enabled Bit
	PEVEN			20h		; Paridad PAR
	PODD			0		; Paridad IMPAR
	STOPBIT1		40h		; 1 BIT STOP
	STOPBIT15		80h		; 1,5 BIT STOP
	STOPBIT2		C0h		; 2 BIT STOP
	
	TxEN			1		; Tx Enabled
	DTREN		2		; Data Terminal Ready Enabled
	RxEN			4		; Rx Enabled
	SendBRK			8		; Send Break character
	ErrRST			10h		; Error RESET
	RTSEN			20h		; Request to Send Enabled	
	INTRST			40h		; Internal RESET
	EnH				80h		; Enabled hunt mode (sync)
	
	RxENABLED		1		; Recepci�n de datos habilitada

;********************************************************************
;	Definici�n de Datos en RAM (Variables)
;********************************************************************
.data	IniDataRAM		;6000h
Dato:			dB		0
Flags:		dB		0	
ErrUSART:		dB		0
ptrBuffer:		dW		0

.data INITERRORUSART	;7500h
CNTPERR:		dB		0
CNTOERR:		dB		0
CNTFERR:		dB		0

.data	INITBUFFER		;8000h
Buffer:		dB		0

.data	USARTADDR		;8F00h
DATOUSART:	dB		0
STATUS:		dB		0
	
;********************************************************************
;	Definici�n de Datos en ROM (Constantes)
;********************************************************************

.data	IniDataROM

;********************************************************************
;	Sector de Arranque del 8085
;********************************************************************
	.org	BootAddr
		JMP	Boot
;********************************************************************
;	Sector del Vector de Interrupciones
;********************************************************************
	.org	AddrIntRST1
		JMP	IntRST1
	.org	AddrIntRST2
		JMP	IntRST2
	.org	AddrIntRST3
		JMP	IntRST3
	.org	AddrIntRST4
		JMP	IntRST4
	.org	AddrIntTRAP
		JMP	IntTRAP
	.org	AddrIntRST5
		JMP	IntRST5
	.org	AddrIntRST55
		JMP	IntRST55
	.org	AddrIntRST6
		JMP	IntRST6
	.org	AddrIntRST65
		JMP	IntRST65
	.org	AddrIntRST7
		JMP	IntRST7
	.org	AddrIntRST75
		JMP	IntRST75
;********************************************************************
;	Sector de las Interrupciones
;********************************************************************
IntRST1:
		;Ac� va el c�digo de la Interrupci�n RST1
		EI
		RET
IntRST2:
		;Ac� va el c�digo de la Interrupci�n RST2
		EI
		RET
IntRST3:
		;Ac� va el c�digo de la Interrupci�n RST3
		EI
		RET
IntRST4:
		;Ac� va el c�digo de la Interrupci�n RST4
		EI
		RET
IntTRAP:
		;Ac� va el c�digo de la Interrupci�n TRAP
		EI
		RET
IntRST5:
		;Ac� va el c�digo de la Interrupci�n RST5
		EI
		RET
IntRST55:
		;Ac� va el c�digo de la Interrupci�n RST5.5
		EI
		RET
IntRST6:
		;Ac� va el c�digo de la Interrupci�n RST6
		EI
		RET
IntRST65:
		;Ac� va el c�digo de la Interrupci�n RST6.5
		EI
		RET
IntRST7:
		;Ac� va el c�digo de la Interrupci�n RST7
		EI
		RET
IntRST75:
		;Ac� va el c�digo de la Interrupci�n RST7.5
		EI
		RET
;********************************************************************
;	Sector del Programa Principal
;********************************************************************
Boot:
	LXI		SP,STACK_ADDR	;Inicializo el Puntero de Pila
	LXI		D , 0			;Inicializo D-E en 0
	
	LXI		H , Buffer		;Leo la direcci�n de "Buffer"
	SHLD	ptrBuffer			;La guardo en la variable ptrBuffer
	
	MVI		A , RxENABLED	;Habilito la recepci�n de datos (Flag)
	STA		Flags			;Flags.0 = 1

	CALL	Init_USART
		
Loop:	
	LDA		Flags
	ANI		RxENABLED		;Flags.0 = 1?
	CNZ		RECIBIR		
	JMP		Loop

RECIBIR:
	LDA		STATUS			;Leo el STATUS de la USART
	ANI		2				;Veo si RxRDY esta activo (RxRDY = 1)
	CNZ		RCV_DATO		
	RET

;******************************************************************************
; Funci�n: RCV_DATO
; Descripci�n: Recibe el dato de la USART y lo almacena en RAM. Verifica fin de datos
; Entrada: ninguna
; Salida: ninguna	 
;******************************************************************************
RCV_DATO:
	;CALL	RCV_USART		;Leo el dato de la USART, puede ser una funcion
	LDA		DATOUSART		;o directamente leo desde la memoria.
	STA		Dato			;Almaceno temporalmente
	CALL	ERRORs
	CPI		0Dh				;Veo si llego el fin de datos (0x0D). Que tambi�n lo almaceno.
	CZ		FinDato			
	
	LHLD	ptrBuffer			;Apunto al buffer con el puntero
	MOV		M , A			;Guardo el dato recibido desde la USART
	INX		H				;Incremento el puntero, antes de verificar la memoria.
	CALL	CHECK_MEM		;Verifico la memoria disponible
	CPI		1
	JZ		FINADDRs
	SHLD	ptrBuffer
FINADDRs:
	RET

;******************************************************************************
; Funci�n: CHECK_MEM
; Descripci�n: Verifica que quede lugar en la RAM
; Entrada: ninguna
; Salida: ninguna	 
;******************************************************************************
CHECK_MEM:	
	XCHG					;Intercambio H-L <-> D-E. D-E contiene ahora el ptrBuffer
	LXI		H,DATOUSART		;Cargo el fin de RAM (o direccion de USART)
	CALL	CMP16
	CPI		1
	CZ 		FinDato
	XCHG					;Intercambio H-L <-> D-E
	RET

;******************************************************************************
; Funci�n: FinDato
; Descripci�n: Deshabilita la recepci�n de datos
; Entrada: ninguna
; Salida: ninguna	 
;******************************************************************************
FinDato:
	PUSH	PSW
	LDA		Flags
	ANI		FEh				;Deshabilito la recepci�n de datos (Flag)
	STA		Flags
	POP		PSW
	RET

;******************************************************************************
; Funci�n: CMP16
; Descripci�n: Compara dos direcciones de 16bits, almacenadas en H-L y D-E
; Entrada: H-L y D-E
; Salida: A = 0 -> Distintas
;		  A = 1 -> Iguales	 
;******************************************************************************
CMP16:
	MVI		B , 0			; Valor temporal. No son iguales.
	MOV		A , L			
	CMP		E
	JNZ		Distinto
	MOV		A , H
	CMP		D
	JNZ		Distinto
	MVI		B , 1			; Valor temporal. Son iguales.
Distinto:
	MOV		A , B			
	RET

;******************************************************************************
; Funci�n: Init_USART
; Descripci�n: Inicializa la USART
; Entrada: ninguna
; Salida: ninguna	 
;******************************************************************************
Init_USART:
	
	; MODO: 8b,Odd Par, 2s, velocidad x1
	MVI		A , STOPBIT2 | CHLENG8 | PARENB | PODD | BAUDRATEx1
	STA		USARTADDR+1
		
	; COMMAND: Enabled Rx, Error RESET
	MVI		A , ErrRST | RxEN
	STA		USARTADDR+1

	RET

;******************************************************************************
; Funci�n: RCV_USART
; Descripci�n: Lee el dato de la USART.
; Entrada: ninguna
; Salida: A (Dato Recibido)	 
;******************************************************************************
RCV_USART:
	LDA		DATOUSART
	RET

;******************************************************************************
; Funci�n: ERRORs
; Descripci�n: Cuenta los errores de Paridad, Sobreescritura y Trama
; Entrada: ninguna
; Salida: ninguna	 
;******************************************************************************
ERRORs:
	PUSH	PSW
	
	CALL	GET_ERROR
	STA		ErrUSART
	ANI		08h
	CNZ		Perror
	LDA		ErrUSART
	ANI		10h
	CNZ		Oerror
	LDA		ErrUSART
	ANI		20h
	CNZ		Ferror
	
	PUSH	PSW
	RET

;******************************************************************************
; Funci�n: Perror
; Descripci�n: Incrementa contador errores de Paridad
; Entrada: ninguna
; Salida: ninguna	 
;******************************************************************************
Perror:
	LDA		CNTPERR
	INR		A
	STA		CNTPERR
	RET
;******************************************************************************
; Funci�n: Oerror
; Descripci�n: Incrementa contador errores de Sobreescritura 
; Entrada: ninguna
; Salida: ninguna	 
;******************************************************************************
Oerror:
	LDA		CNTOERR
	INR		A
	STA		CNTOERR
	RET
;******************************************************************************
; Funci�n: Ferror
; Descripci�n: Incrementa contador errores de Trama 
; Entrada: ninguna
; Salida: ninguna	 
;******************************************************************************
Ferror:
	LDA		CNTFERR
	INR		A
	STA		CNTFERR
	RET

;******************************************************************************
; Funci�n: GET_ERROR
; Descripci�n: Obtiene los bits de error.
; Entrada: ninguna
; Salida: A (Errores)	 
;******************************************************************************
GET_ERROR:
	LDA		STATUS
	ANI		38h
	RET
	
	HLT
	