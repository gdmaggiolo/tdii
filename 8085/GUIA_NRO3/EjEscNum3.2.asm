;******************************************************************************
;T�cnicas Digitales II
;Ejercitaci�n N�mero 3.2: Conversi�n de BCD a Binario
;Autor: Ing. Maggiolo Gustavo
;******************************************************************************

; Funci�n: BCD2BIN
; Realizar un programa que convierta a binario el n�mero almacenado 
; en las posiciones de memoria 2002h, 2003h, 2004h y 2005h de la
; siguiente forma:
;	Unidades				-->	2002h
;	Decenas  				-->	2003h
;	Centenas				--> 2004h
;	U. de Mil				--> 2005h
;	Numero Binario LSB		-->	2006h
;	Numero Binario MSB		-->	2007h

;******************************************************************************
;	Definici�n de Etiquetas
;******************************************************************************

.define
	BootAddr			0000h
	AddrIntRST1		0008h
	AddrIntRST2		0010h
	AddrIntRST3		0018h
	AddrIntRST4		0020h
	AddrIntTRAP		0024h
	AddrIntRST5		0028h
	AddrIntRST55		002Ch
	AddrIntRST6		0030h
	AddrIntRST65		0034h
	AddrIntRST7		0038h
	AddrIntRST75		003Ch

	STACK_ADDR		FFFFh

	IniDataROM		0540h		;Definir donde termina el Programa
	IniDataRAM		2000h		;Definir de acuerdo al Hardware

	ptrDatos			1040h
		
;******************************************************************************
;	Definici�n de Datos en RAM (Variables)
;******************************************************************************
.data	IniDataRAM
BCDL: 			dB	0			
BCDH:			dB	0			
Unid:				db	4			;Inicializaci�n del dato
Dece:				db	5			;Inicializaci�n del dato
Cent:				db	7			;Inicializaci�n del dato
UMil:				db	9			;Inicializaci�n del dato
NumBinLSB:		dB	0
NumBinMSB:		dB	0
BCDValL:			dB	0
BCDValM:			dB	0
BCDValH:			dB	0
Temp:			dB	0,0,0,0,0,0,0,0
	
;******************************************************************************
;	Definici�n de Datos en ROM (Constantes)
;******************************************************************************
.data	IniDataROM
	
CteMAX:	dW	03FFh
CteMIN:	dB	80h

;******************************************************************************
;	Sector de Arranque del 8085
;******************************************************************************
	.org	BootAddr
		JMP	Boot
;******************************************************************************
;	Sector del Vector de Interrupciones
;******************************************************************************
	.org	AddrIntRST1
		JMP	IntRST1
	.org	AddrIntRST2
		JMP	IntRST2
	.org	AddrIntRST3
		JMP	IntRST3
	.org	AddrIntRST4
		JMP	IntRST4
	.org	AddrIntTRAP
		JMP	IntTRAP
	.org	AddrIntRST5
		JMP	IntRST5
	.org	AddrIntRST55
		JMP	IntRST55
	.org	AddrIntRST6
		JMP	IntRST6
	.org	AddrIntRST65
		JMP	IntRST65
	.org	AddrIntRST7
		JMP	IntRST7
	.org	AddrIntRST75
		JMP	IntRST75

;******************************************************************************
;	Sector de las Interrupciones
;******************************************************************************
IntRST1:	;Ac� va el c�digo de la Interrupci�n RST1
		EI
		RET
IntRST2:	;Ac� va el c�digo de la Interrupci�n RST2
		EI
		RET
IntRST3:	;Ac� va el c�digo de la Interrupci�n RST3
		EI
		RET
IntRST4:	;Ac� va el c�digo de la Interrupci�n RST4
		EI
		RET
IntTRAP:	;Ac� va el c�digo de la Interrupci�n TRAP
		EI
		RET
IntRST5:	;Ac� va el c�digo de la Interrupci�n RST5
		EI
		RET
IntRST55:	;Ac� va el c�digo de la Interrupci�n RST5.5
		EI
		RET
IntRST6:	;Ac� va el c�digo de la Interrupci�n RST6
		EI
		RET
IntRST65:	;Ac� va el c�digo de la Interrupci�n RST6.5
		EI
		RET
IntRST7:	;Ac� va el c�digo de la Interrupci�n RST7
		EI
		RET
IntRST75:	;Ac� va el c�digo de la Interrupci�n RST7.5
		EI
		RET
;******************************************************************************
;	Sector del Programa Principal
;******************************************************************************
Boot:
	LXI	SP,STACK_ADDR	;Inicializo el Puntero de Pila
	
Main:
	;Dato cargado de ejemplo: 9754d  = 1579h
	;El dato esta inicializado m�s arriba

	CALL BCD2BIN
		
	HLT

;******************************************************************************
; Funci�n: BCD2BIN
; Descripci�n: 	Convierte de BCD a binario.
; Entrada: 		Unidades, Decenas, Centenas y U. de Mil
; Salida: 		NumBinMSB:NumBinLSB
;******************************************************************************
BCD2BIN:
		PUSH	PSW
		PUSH	B

		LDA Unid
		MOV B,A
		LDA Dece
		RLC
		RLC
		RLC
		RLC
		ORA B
		STA BCDL
		
		LDA Cent
		MOV C,A
		LDA UMil
		RLC
		RLC
		RLC
		RLC
		ORA C
		STA BCDH
		
		MVI	A,0
		STA	BCDValH
		LDA	BCDH
		STA	BCDValM
		LDA	BCDL
		STA	BCDValL
	
		CALL BCDTOBIN
				
		POP	B
		POP	PSW
		RET			; Retorno de la funci�n
		
;******************************************************************************
; Funci�n: BCDTOBIN
; Descripci�n: 	Convierte de BCD a binario.
; Entrada: 		BCDValH, BCDValM y BCDValL.
; Salida: 		NumBinMSB:NumBinLSB
;******************************************************************************
BCDTOBIN:
		PUSH	PSW
		PUSH	B
		PUSH	D
		PUSH	H
		
		MVI	C,16
		MVI	A,0
		STA	NumBinLSB		; Borro resultado LSB
		STA	NumBinMSB		; Borro resultado MSB
		MOV	B,A				; Borro dato temporal

LoopBCDbin:
		STC				; Borro
		CMC				; Carry
		
		LDA	BCDValH		; Leo BCDValH
		RAR				; Roto a DERECHA 
		STA	BCDValH		; Guardo BCDValH
		
		LDA	BCDValM		; Leo BCDValM
		RAR				; Roto a DERECHA 
		STA	BCDValM		; Guardo BCDValM
		
		LDA	BCDValL		; Leo BCDValL
		RAR				; Roto a DERECHA
		STA	BCDValL		; Guardo BCDValL
		
		LDA	NumBinMSB
		RAR				; Roto a DERECHA NumBinMSB
		STA	NumBinMSB
		LDA	NumBinLSB
		RAR				; Roto a DERECHA NumBinMSB
		STA	NumBinLSB

		LXI	H,BCDValL		; Apunto a la parte baja
		CALL AdjBCD		; Llamo a la funci�n de ajuste
		LXI	H,BCDValM	; Apunto a la parte media
		CALL AdjBCD		; Llamo a la funci�n de ajuste
		LXI	H,BCDValH		; Apunto a la parte alta
		CALL AdjBCD		; Llamo a la funci�n de ajuste
		
		DCR	C
		JNZ	LoopBCDbin

		POP	H
		POP	D
		POP	B
		POP	PSW
		RET			; Retorno de la funci�n

;******************************************************************************
; Funcion: AdjBCD
; Descripcion: 	Hace el ajuste BCD en cada nibble, si corresponde.
; Entrada: 		H-L apunta al valor a ajustar.
; Salida: 		ninguna
;******************************************************************************
AdjBCD:
		MVI	B,0			; Inicializo B
		MOV	A,M		
		ANI	08h			; Es >= 8 ?
		JZ	norestalsb		; NO
		MVI	B,03h		; SI
norestalsb:
		MOV	A,M
		ANI	80h			; Es >= 80 ?
		JZ	norestamsb	; NO
		MVI	A,30h		; SI
		ORA	B
		MOV	B,A
norestamsb:
		MOV	A,M
		SUB	B			; A <-- A - B
		MOV	M,A
		RET