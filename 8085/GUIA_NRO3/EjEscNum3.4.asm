;******************************************************************************
;T�cnicas Digitales II
;Ejercitaci�n N�mero 3.4: Conversi�n de Binario a BCD
;Autor: Ing. Maggiolo Gustavo
;******************************************************************************

; Funci�n: BIN2BCD
; Realizar un programa que convierta a BCD el n�mero binario almacenado 
; en las posiciones de memoria 2002h, 2003h, 2004h y 2005h de la
; siguiente forma:
;	Unidades			-->	2002h
;	Decenas  			-->	2003h
;	Centenas			--> 2004h
;	U. de Mil			--> 2005h
;	Numero Binario LSB	-->	2006h
;	Numero Binario MSB	-->	2007h

;******************************************************************************
;	Definici�n de Etiquetas
;******************************************************************************

.define
	BootAddr		0000h
	AddrIntRST1		0008h
	AddrIntRST2		0010h
	AddrIntRST3		0018h
	AddrIntRST4		0020h
	AddrIntTRAP		0024h
	AddrIntRST5		0028h
	AddrIntRST55	002Ch
	AddrIntRST6		0030h
	AddrIntRST65	0034h
	AddrIntRST7		0038h
	AddrIntRST75	003Ch

	STACK_ADDR	FFFFh

	IniDataROM	0540h		;Definir donde termina el Programa
	IniDataRAM	2000h		;Definir de acuerdo al Hardware

	ptrDatos		1040h
		
;******************************************************************************
;	Definici�n de Datos en RAM (Variables)
;******************************************************************************
.data	IniDataRAM
BCDL:			dB	0
BCDH:			dB	0
Unid:			dB	0
Dece:			dB	0
Cent:			dB	0
UMil:			dB	0
NumBinLSB:		dB	94h
NumBinMSB:		dB	25h
BCDValL:		dB	0
BCDValM:		dB	0
BCDValH:		dB	0
Temp:			dB	0,0,0,0,0,0,0,0
	
;******************************************************************************
;	Definici�n de Datos en ROM (Constantes)
;******************************************************************************
.data	IniDataROM
	
CteMAX:	dW	03FFh
CteMIN:	dB	80h

;******************************************************************************
;	Sector de Arranque del 8085
;******************************************************************************
	.org	BootAddr
		JMP	Boot
;******************************************************************************
;	Sector del Vector de Interrupciones
;******************************************************************************
	.org	AddrIntRST1
		JMP	IntRST1
	.org	AddrIntRST2
		JMP	IntRST2
	.org	AddrIntRST3
		JMP	IntRST3
	.org	AddrIntRST4
		JMP	IntRST4
	.org	AddrIntTRAP
		JMP	IntTRAP
	.org	AddrIntRST5
		JMP	IntRST5
	.org	AddrIntRST55
		JMP	IntRST55
	.org	AddrIntRST6
		JMP	IntRST6
	.org	AddrIntRST65
		JMP	IntRST65
	.org	AddrIntRST7
		JMP	IntRST7
	.org	AddrIntRST75
		JMP	IntRST75

;******************************************************************************
;	Sector de las Interrupciones
;******************************************************************************

IntRST1:	;Ac� va el c�digo de la Interrupci�n RST1
		EI
		RET
IntRST2:	;Ac� va el c�digo de la Interrupci�n RST2
		EI
		RET
IntRST3:	;Ac� va el c�digo de la Interrupci�n RST3
		EI
		RET
IntRST4:	;Ac� va el c�digo de la Interrupci�n RST4
		EI
		RET
IntTRAP:	;Ac� va el c�digo de la Interrupci�n TRAP
		EI
		RET
IntRST5:	;Ac� va el c�digo de la Interrupci�n RST5
		EI
		RET
IntRST55:	;Ac� va el c�digo de la Interrupci�n RST5.5
		EI
		RET
IntRST6:	;Ac� va el c�digo de la Interrupci�n RST6
		EI
		RET
IntRST65:	;Ac� va el c�digo de la Interrupci�n RST6.5
		EI
		RET
IntRST7:	;Ac� va el c�digo de la Interrupci�n RST7
		EI
		RET
IntRST75:	;Ac� va el c�digo de la Interrupci�n RST7.5
		EI
		RET

;******************************************************************************
;	Sector del Programa Principal
;******************************************************************************
Boot:
	LXI	SP,STACK_ADDR	;Inicializo el Puntero de Pila
	
Main:
	;Dato cargado de ejemplo: 2594h  = 9620
		
	CALL	BIN2BCD

	
	

	HLT

;******************************************************************************
; Funci�n: BIN2BCD
; Descripci�n: 	Convierte un n�mero que esta en Binario a su representaci�n
;			en BCD.
; Entrada: 		NumBinMSB:NumBinLSB
; Salida:  		BCDValH:BCDValM:BCDValL
;******************************************************************************
BIN2BCD:
		PUSH	PSW
		PUSH	B
		PUSH	D
		PUSH	H

		CALL	BINTOBCD
		
		LDA	BCDValM		; Muevo 
		STA	BCDH		; resultad
		LDA	BCDValL		; de la funci�n
		STA	BCDL		; BINTOBCD
		
		LDA BCDH
		MOV B,A
		ANI 0Fh
		STA Cent
		MOV A,B
		ANI F0h
		RRC
		RRC
		RRC
		RRC
		STA UMil
		
		LDA BCDL
		MOV B,A
		ANI 0Fh
		STA Unid
		MOV A,B
		ANI F0h
		RRC
		RRC
		RRC
		RRC
		STA Dece
		
		POP	H
		POP	D
		POP	B
		POP	PSW
		RET			; Retorno de la funci�n
		
;******************************************************************************
; Funci�n: BINTOBCD
; Descripci�n: 	Convierte un n�mero que esta en Binario a su representaci�n
;			en BCD.
; Entrada: 		NumBinMSB:NumBinLSB
; Salida:  		BCDValH:BCDValM:BCDValL
;******************************************************************************
BINTOBCD:
		PUSH	PSW
		PUSH	B
		PUSH	D
		PUSH	H
		
		MVI	C,16
		MVI	A,0
		STA	BCDValH		; Borro resultado BCDValH
		STA	BCDValM		; Borro resultado BCDValM
		STA	BCDValL		; Borro resultado BCDValL
		MOV	B,A			; Borro dato temporal
		
		STC				; Borro
		CMC				; Carry
LoopBINbcd:		
		LDA	NumBinLSB		; Leo NumBinLSB
		RAL				; Roto a IZQ 
		STA	NumBinLSB		; Guardo NumBinLSB

		LDA	NumBinMSB		; Leo NumBinMSB
		RAL				; Roto a DERECHA
		STA	NumBinMSB		; Guardo NumBinMSB

		LDA	BCDValL
		RAL				; Roto a DERECHA BCDValL
		STA	BCDValL
		LDA	BCDValM
		RAL				; Roto a DERECHA BCDValM
		STA	BCDValM
		LDA	BCDValH
		RAL				; Roto a DERECHA BCDValH
		STA	BCDValH

		DCR	C
		JZ	FinLoopBINbcd
		
		LXI	H,BCDValL
		CALL AdjBCD
		LXI	H,BCDValM
		CALL AdjBCD
		LXI	H,BCDValH
		CALL AdjBCD
		JMP	LoopBINbcd
FinLoopBINbcd:
						
		POP	H
		POP	D
		POP	B
		POP	PSW
		RET			; Retorno de la funci�n
	
;******************************************************************************
; Funcion: AdjBCD
; Descripcion: 	Hace el ajuste BCD en cada nibble, si corresponde.
; Entrada: 		H-L apunta al valor a ajustar.
; Salida: 		ninguna
;******************************************************************************
AdjBCD:
		MVI	B, 0
		MOV	A,M			; Cargo BCDValx (Dependiendo de la llamada)
		ADI	03h			; Sumo 3 para ver si da 8 o mas
		ANI	08h			; Veo el bit 3
		JZ	nosumlsb
		MVI	B, 03h
nosumlsb:
		MOV	A,M			; Cargo BCDValx (Dependiendo de la llamada)
		ADI	30h			; Sumo 3 para ver si da 8 o mas
		ANI	80h			; Veo el bit 7
		JZ	nosummsb
		MVI	A, 30h		
		ORA	B			; En B ya puede haber un valor
		MOV	B,A
nosummsb:
		MOV	A,M			; Cargo BCDValx (Dependiendo de la llamada)
		ADD	B			; en B queda el ajuste final
		MOV	M,A
		RET