;******************************************************************************
;T�cnicas Digitales II
;Ejercitaci�n Nro 7.3
;Autor: Ing. Maggiolo Gustavo
;******************************************************************************

; Ejercitaci�n Nro 7.3
; Realizar un programa que escriba 00h o FFh en el puerto A de una PPI
; dependiendo del estado en que se encuentra la entrada serial del 8085.
; La direcci�n de la PPI es 20h.


;********************************************************************
;	Definici�n de Etiquetas
;********************************************************************

.define
	BootAddr			0000h
	AddrIntRST1		0008h
	AddrIntRST2		0010h
	AddrIntRST3		0018h
	AddrIntRST4		0020h
	AddrIntTRAP		0024h
	AddrIntRST5		0028h
	AddrIntRST55		002Ch
	AddrIntRST6		0030h
	AddrIntRST65		0034h
	AddrIntRST7		0038h
	AddrIntRST75		003Ch

	STACK_ADDR		FFFFh

	IniDataROM		0540h		;Comienzo de Constantes en ROM
	IniDataRAM		2000h		;Comienzo de Variables en RAM

	ADDRA       	20h
	ADDRB       	21h
	ADDRC       	22h
	ADDRRC      	23h
	
	SetMODE			80h	
	WBitC			00h
	SetM0GA			00h
	SetM1GA			20h
	SetM2GA			40h
	SetM0GB			00h
	SetM1GB			04h
	DirINport			1
	DirOutport		0
			

;********************************************************************
;	Definici�n de Datos en RAM (Variables)
;********************************************************************

.data	IniDataRAM
Var1:		dB		16		;Inicializo la variable en 16
Var2:	dB		0
Var3:	dB		0
Var4:	dB		0

	
;********************************************************************
;	Definici�n de Datos en ROM (Constantes)
;********************************************************************

.data	IniDataROM
	
CteWord:		dW	03E8h
CteByte:		dB	64h
	
Texto:		dB	'C','a','d','e','n','a',0

;********************************************************************
;	Sector de Arranque del 8085
;********************************************************************
	.org	BootAddr
		JMP	Boot
;********************************************************************
;	Sector del Vector de Interrupciones
;********************************************************************
	.org	AddrIntRST1
		JMP	IntRST1
	.org	AddrIntRST2
		JMP	IntRST2
	.org	AddrIntRST3
		JMP	IntRST3
	.org	AddrIntRST4
		JMP	IntRST4
	.org	AddrIntTRAP
		JMP	IntTRAP
	.org	AddrIntRST5
		JMP	IntRST5
	.org	AddrIntRST55
		JMP	IntRST55
	.org	AddrIntRST6
		JMP	IntRST6
	.org	AddrIntRST65
		JMP	IntRST65
	.org	AddrIntRST7
		JMP	IntRST7
	.org	AddrIntRST75
		JMP	IntRST75
;********************************************************************
;	Sector de las Interrupciones
;********************************************************************
IntRST1:
		;Ac� va el c�digo de la Interrupci�n RST1
		EI
		RET
IntRST2:
		;Ac� va el c�digo de la Interrupci�n RST2
		EI
		RET
IntRST3:
		;Ac� va el c�digo de la Interrupci�n RST3
		EI
		RET
IntRST4:
		;Ac� va el c�digo de la Interrupci�n RST4
		EI
		RET
IntTRAP:
		;Ac� va el c�digo de la Interrupci�n TRAP
		EI
		RET
IntRST5:
		;Ac� va el c�digo de la Interrupci�n RST5
		EI
		RET
IntRST55:
		;Ac� va el c�digo de la Interrupci�n RST5.5
		EI
		RET
IntRST6:
		;Ac� va el c�digo de la Interrupci�n RST6
		EI
		RET
IntRST65:
		;Ac� va el c�digo de la Interrupci�n RST6.5
		EI
		RET
IntRST7:
		;Ac� va el c�digo de la Interrupci�n RST7
		EI
		RET
IntRST75:
		;Ac� va el c�digo de la Interrupci�n RST7.5
		EI
		RET
;********************************************************************
;	Sector del Programa Principal
;********************************************************************
Boot:
	LXI	SP,STACK_ADDR	;Inicializo el Puntero de Pila
	DI
Main:
	MVI A,10001011B 	;Configuro PPI en
    OUT ADDRRC      ;modo 0, PA salida. PB y PC entrada
Loop:
	RIM	
	RLC
	JC ESUNO
ESCERO:
	MVI A,FFh
	OUT ADDRA
	JMP Loop
ESUNO:
	MVI A,00h
	OUT ADDRA
	JMP Loop
	HLT
	