;******************************************************************************
;T�cnicas Digitales II
;Ejercitaci�n Nro 7.5
;Autor: Ing. Maggiolo Gustavo
;******************************************************************************

; Ejercitaci�n Nro 7.5
; Realizar un programa con 8085 y dos PPI, una en la direcci�n 24h y la
; otra en la direcci�n 80h. La primera configurada en modo 1, con el
; puerto A como entrada y el B como salida, con pedido de interrupci�n
; RST 6.5 el PA y RST 5.5 el PB. La segunda en modo 0 con todos sus
; puertos como salida. El sistema deber� recibir los datos del puerto
; A y transmitirlo por el puerto B. En la segunda PPI deber� mostrar
; la cantidad de datos que se transmitieron, por medio de un
; contador de 3 digitos.

;********************************************************************
;	Definici�n de Etiquetas
;********************************************************************
.define
	BootAddr			0000h
	AddrIntRST1		0008h
	AddrIntRST2		0010h
	AddrIntRST3		0018h
	AddrIntRST4		0020h
	AddrIntTRAP		0024h
	AddrIntRST5		0028h
	AddrIntRST55		002Ch
	AddrIntRST6		0030h
	AddrIntRST65		0034h
	AddrIntRST7		0038h
	AddrIntRST75		003Ch

	STACK_ADDR		FFFFh

	IniDataROM		0540h		;Comienzo de Constantes en ROM
	IniDataRAM		2000h		;Comienzo de Variables en RAM

	MaskSetEn		8
	M75				4
	M65				2
	M55				1
	
	ADDRA1       	24h
	ADDRB1       	25h
	ADDRC1       	26h
	ADDRRC1      	27h
	ADDRA2       	80h
	ADDRB2       	81h
	ADDRC2       	82h
	ADDRRC2      	83h
	
	sega				02h
	segb				04h
	segc				40h
	segd				20h
	sege				10h
	segf				01h
	segg				08h

;		a			a: 02h	
;	-------------	b: 04h
;	|	     	|	c: 40h	 
;	| 	   		|	d: 20h
;  f|  	   		|b	e: 10h
;	|  			|	f: 01h
;	| 			|	g: 08h
;	------g------	
;	|    		|	
;	|  	 		|	
;  e|  	  		|c	
;	|			|	
;	|        	| 	
;	------------- 	
;		d			

; PORTA
; |-------------------------------------------------------
; |  b7 |  b6  |  b5  |  b4  |  b3  |  b2  |  b1  |  b0  |
; | pto | segc | segd | sege | segg | segb | sega | segf |
; |-------------------------------------------------------

;********************************************************************
;	Definici�n de Datos en RAM (Variables)
;********************************************************************
.data	IniDataRAM
Flags:		db		0		;Banderas=> B0: Datos Tx
pDataRx:		dw		0		;Puntero a Datos Recibidos
pDataTx:		dw		0		;Puntero a Datos Transmitidos
CNT:			dw		0
CNT7SEG: 	db		0 , 0 , 0
Buffer:		dB		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 ;32 byte

	
;********************************************************************
;	Definici�n de Datos en ROM (Constantes)
;********************************************************************
.data	IniDataROM
Numero0:		db sega|segb|segc|segd|sege|segf
Numero1:		db segb|segc
Numero2:		db sega|segb|segg|sege|segd
Numero3:		db sega|segb|segc|segd|segg
Numero4:		db segf|segg|segb|segc
Numero5:		db sega|segf|segg|segc|segd
Numero6:		db segf|sege|segd|segc|segg
Numero7:		db sega|segb|segc
Numero8:		db sega|segb|segc|segd|sege|segf|segg
Numero9:		db sega|segb|segc|segg|segf
LetraE:
NumeroA:	db sega|segd|sege|segf|segg			;Letra E
NumeroB:		db sega|segd|sege|segf|segg			;Letra E
NumeroC:	db sega|segd|sege|segf|segg			;Letra E
NumeroD:	db sega|segd|sege|segf|segg			;Letra E
NumeroE:		db sega|segd|sege|segf|segg			;Letra E
NumeroF:		db sega|segd|sege|segf|segg			;Letra E

;********************************************************************
;	Sector de Arranque del 8085
;********************************************************************
	.org	BootAddr
		JMP	Boot
;********************************************************************
;	Sector del Vector de Interrupciones
;********************************************************************
	.org	AddrIntRST1
		JMP	IntRST1
	.org	AddrIntRST2
		JMP	IntRST2
	.org	AddrIntRST3
		JMP	IntRST3
	.org	AddrIntRST4
		JMP	IntRST4
	.org	AddrIntTRAP
		JMP	IntTRAP
	.org	AddrIntRST5
		JMP	IntRST5
	.org	AddrIntRST55
		JMP	IntRST55
	.org	AddrIntRST6
		JMP	IntRST6
	.org	AddrIntRST65
		JMP	IntRST65
	.org	AddrIntRST7
		JMP	IntRST7
	.org	AddrIntRST75
		JMP	IntRST75
;********************************************************************
;	Sector de las Interrupciones
;********************************************************************
IntRST1:
		;Ac� va el c�digo de la Interrupci�n RST1
		EI
		RET
IntRST2:
		;Ac� va el c�digo de la Interrupci�n RST2
		EI
		RET
IntRST3:
		;Ac� va el c�digo de la Interrupci�n RST3
		EI
		RET
IntRST4:
		;Ac� va el c�digo de la Interrupci�n RST4
		EI
		RET
IntTRAP:
		;Ac� va el c�digo de la Interrupci�n TRAP
		EI
		RET
IntRST5:
		;Ac� va el c�digo de la Interrupci�n RST5
		EI
		RET
IntRST55:
		;Interrupci�n RST5.5
		;Interrupci�n del Puerto B de Salida PPI1
		PUSH	PSW
		PUSH	H
					
		LHLD	pDataTx					;Transmito el siguiente dato
		MOV		A , M					;del buffer
		OUT		ADDRB1					
		INX		H 						;Incremento puntero Tx
		
		MOV		A , L					;Verifico si llego al final
		CPI		32						;del buffer
		JNZ		SeguirTx
		LXI 	H , Buffer				;Reinicio el puntero
SeguirTx:
		SHLD 	pDataTx					;Guardo puntero
		
		CALL	IncCont					;Incremento contador de datos Tx
		CALL	Mostrar					;Muestro el contador
		
		POP	H
		POP	PSW
		EI
		RET
IntRST6:
		;Ac� va el c�digo de la Interrupci�n RST6
		EI
		RET
IntRST65:
		;Interrupci�n RST6.5
		;Interrupci�n del Puerto A de entrada PPI1
		PUSH	PSW
		PUSH	H
		
		IN 		ADDRA1			;Leo dato entrada
		LHLD 	pDataRx			;Leo el puntero de Rx
		MOV		M , A			;Almaceno valor
		INX		H				;Incremento puntero
		
		MOV		A , L			;Verifico si llego al final 
		CPI		32				;del buffer
		JNZ		SeguirRx			
		LXI 	H , Buffer		;Reinicio el puntero
SeguirRx:
		SHLD 	pDataRx			;Guardo puntero
		
		POP	H
		POP	PSW
		EI
		RET
IntRST7:
		;Ac� va el c�digo de la Interrupci�n RST7
		EI
		RET
IntRST75:
		;Ac� va el c�digo de la Interrupci�n RST7.5
		EI
		RET

;********************************************************************
;	Sector del Programa Principal
;********************************************************************
Boot:
	LXI	SP,STACK_ADDR				;Inicializo el Puntero de Pila
	
	MVI 	A , 10111100B 				; Conf. PPI MODO 1
    OUT	 	ADDRRC1					; A:IN, B:OUT C6,7:IN
	
	MVI 	A , 10000000B 			; Conf. PPI MODO 0
    OUT 	ADDRRC2					; A:OUT, B:OUT C:OUT
	
	MVI		A , 00001001B				; Hab. Interrupci�n Rx. 
	OUT		ADDRC1					; INTE A (PC4)
	
;	MVI		A , 00000101B			; Hab. Interrupci�n Tx.
;	OUT		ADDRC1					; INTE B (PC2)
	
	LXI 	H , Buffer
	SHLD 	pDataRx					;Inicializo los punteros
	SHLD	pDataTx					;con igual valor => no hay datos para Tx
	
	MVI		A , MaskSetEn | M75 		;Solo hab. RST6.5 y RST5.5	
	SIM								;Inicializo las interrupciones
	EI
	
	CALL	Mostrar
	
Main:	
	CALL	CheckBuff
	CPI		0
	JZ		Main
	
	LHLD	pDataTx					;Transmito el primer dato
	MOV		A , M					;
	OUT		ADDRB1					;
	INX		H
	SHLD	pDataTx					;
	
	
	MVI		A , 00000101B				; Hab. Interrupci�n Tx.
	OUT		ADDRC1					; INTE B (PC2)

	CALL	IncCont					;Incremento contador de datos Tx
	CALL	Mostrar					;Muestro el contador
Loop:
	
	JMP 	Loop

;******************************************************************************
;	Funci�n: CheckBuff
;	Funci�n: Checkea si el puntero pDataRx y pDataTx son iguales.
;	Retorno:
;			0 : No hay datos
;			1 : Hay datos 
;******************************************************************************
CheckBuff:
		PUSH	H
		
		LHLD	pDataRx
		MOV 	A , L
		LHLD	pDataTx
		CMP		L			;A-L
		MVI		A , 0
		JZ		NoData
		MVI		A , 1
NoData:
		POP		H
		RET
;******************************************************************************
;	Funci�n: IncCont
;	Funci�n: Incrementa un contador de 8 digitos, con direcci�n base en CNT. 
;******************************************************************************
IncCont:

		CALL	IncCont0
		CC		IncCont1
		RET

IncCont0:
		LDA		CNT
		ADI		1
		DAA
		STA 	CNT
		RET
IncCont1:
		LDA		CNT+1
		ADI		1	
		DAA
		CPI		10h
		JNZ		NoBorrar
		XRA		A
		STA		CNT
NoBorrar:
		STA 	CNT+1
		RET

;******************************************************************************
; Funci�n: Mostrar
; Descripci�n: Divide los valores BCD empaquetados los convierte a
; 7 seg y los muestra
; Entrada: ninguna
; Salida: ninguna	 
;******************************************************************************
Mostrar:
		CALL	SPLIT
		CALL	CONV7SEG
		CALL	PrintDig7S
		RET

;******************************************************************************
; Funci�n: SPLIT
; Descripci�n: Divide los valores BCD Empaquetados
; Entrada: ninguna
; Salida: ninguna	 
;******************************************************************************
SPLIT:
		LDA	CNT
		ANI	0Fh
		STA	CNT7SEG
		LDA	CNT
		ANI	F0h
		RRC
		RRC
		RRC
		RRC
		STA	CNT7SEG+1
		
		LDA	CNT+1
		ANI	0Fh
		STA	CNT7SEG+2
		RET

;******************************************************************************
; Funci�n: CONV7SEG
; Descripci�n: Convierte los valores de Reloj a 7 segmentos
; Entrada: ninguna
; Salida: ninguna	 
;******************************************************************************
CONV7SEG:
		LDA		CNT7SEG	; Convierto CNT.LSB
		CALL 	BCDTO7SEG
		STA		CNT7SEG 
		
		LDA		CNT7SEG+1	; Convierto CNT.Med
		CALL 	BCDTO7SEG
		STA		CNT7SEG+1 

		LDA		CNT7SEG+2	; Convierto CNT.MSB
		CALL 	BCDTO7SEG
		STA		CNT7SEG+2 
		RET
		
;******************************************************************************
; Funci�n: BCDTO7SEG
; Descripci�n: Convierte un n�mero BCD a 7 segmentos.
; Entrada: A (n�mero BCD a convertir)
; Salida: A (representaci�n en 7 segmentos)	 
;******************************************************************************
BCDTO7SEG:
	PUSH 	H
	LXI 	H , Numero0
	ANI		0Fh
	ADD 	L
	MOV 	L , A
	MOV 	A , M
	POP 	H
	RET

;******************************************************************************
;	Funci�n: PrintDig7S
;	Descripci�n: Imprime los N�meros en los display de 7 Segmentos
;		   Los datos son tomados de CNT7SEG
;******************************************************************************
PrintDig7S:
		LDA		CNT7SEG	; Convierto CNT.LSB
		OUT		ADDRC2
		
		LDA		CNT7SEG+1	; Convierto CNT.Med
		OUT		ADDRB2
		
		LDA		CNT7SEG+2	; Convierto CNT.MSB
		OUT		ADDRA2
		RET	


	HLT
	