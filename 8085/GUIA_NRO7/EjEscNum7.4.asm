;******************************************************************************
;T�cnicas Digitales II
;Ejercitaci�n Nro 7.4
;Autor: Ing. Maggiolo Gustavo
;******************************************************************************

; Ejercitaci�n Nro 7.4
; Realizar un programa con 8085 y una PPI (direcci�n 44h). El sistema debe
; leer los datos que ingresan por el puerto A (8 bits); si el dato es un
; n�mero BCD, de 0 a 9, escribirlo en el puerto B (�ste posee conectado un
; display de 7 segmentos), de lo contrario mostrar una "E".

;********************************************************************
;	Definici�n de Etiquetas
;********************************************************************

.define
	BootAddr			0000h
	AddrIntRST1		0008h
	AddrIntRST2		0010h
	AddrIntRST3		0018h
	AddrIntRST4		0020h
	AddrIntTRAP		0024h
	AddrIntRST5		0028h
	AddrIntRST55		002Ch
	AddrIntRST6		0030h
	AddrIntRST65		0034h
	AddrIntRST7		0038h
	AddrIntRST75		003Ch

	STACK_ADDR		FFFFh

	IniDataROM		0540h		;Comienzo de Constantes en ROM
	IniDataRAM		2000h		;Comienzo de Variables en RAM

	ADDRA       	44h
	ADDRB       	45h
	ADDRC       	46h
	ADDRRC      	47h
	
	sega			02h
	segb			04h
	segc			40h
	segd			20h
	sege			10h
	segf			01h
	segg			08h

;		a			a: 02h	
;	-------------	b: 04h
;	|	     	|	c: 40h	 
;	| 	   		|	d: 20h
;  f|  	   		|b	e: 10h
;	|  			|	f: 01h
;	| 			|	g: 08h
;	------g------	
;	|    		|	
;	|  	 		|	
;  e|  	  		|c	
;	|			|	
;	|        	| 	
;	------------- 	
;		d			

; PORTA
; |-------------------------------------------------------
; |  b7 |  b6  |  b5  |  b4  |  b3  |  b2  |  b1  |  b0  |
; | pto | segc | segd | sege | segg | segb | sega | segf |
; |-------------------------------------------------------

;********************************************************************
;	Definici�n de Datos en RAM (Variables)
;********************************************************************

.data	IniDataRAM
Var1:		dB		16		;Inicializo la variable en 16
Var2:	dB		0
Var3:	dB		0
Var4:	dB		0
	
;********************************************************************
;	Definici�n de Datos en ROM (Constantes)
;********************************************************************
.data	IniDataROM
Numero0:		db sega|segb|segc|segd|sege|segf
Numero1:		db segb|segc
Numero2:		db sega|segb|segg|sege|segd
Numero3:		db sega|segb|segc|segd|segg
Numero4:		db segf|segg|segb|segc
Numero5:		db sega|segf|segg|segc|segd
Numero6:		db segf|sege|segd|segc|segg
Numero7:		db sega|segb|segc
Numero8:		db sega|segb|segc|segd|sege|segf|segg
Numero9:		db sega|segb|segc|segg|segf
LetraE:
NumeroA:	db sega|segd|sege|segf|segg			;Letra E
NumeroB:		db sega|segd|sege|segf|segg			;Letra E
NumeroC:	db sega|segd|sege|segf|segg			;Letra E
NumeroD:	db sega|segd|sege|segf|segg			;Letra E
NumeroE:		db sega|segd|sege|segf|segg			;Letra E
NumeroF:		db sega|segd|sege|segf|segg			;Letra E

;********************************************************************
;	Sector de Arranque del 8085
;********************************************************************
	.org	BootAddr
		JMP	Boot
;********************************************************************
;	Sector del Vector de Interrupciones
;********************************************************************
	.org	AddrIntRST1
		JMP	IntRST1
	.org	AddrIntRST2
		JMP	IntRST2
	.org	AddrIntRST3
		JMP	IntRST3
	.org	AddrIntRST4
		JMP	IntRST4
	.org	AddrIntTRAP
		JMP	IntTRAP
	.org	AddrIntRST5
		JMP	IntRST5
	.org	AddrIntRST55
		JMP	IntRST55
	.org	AddrIntRST6
		JMP	IntRST6
	.org	AddrIntRST65
		JMP	IntRST65
	.org	AddrIntRST7
		JMP	IntRST7
	.org	AddrIntRST75
		JMP	IntRST75
;********************************************************************
;	Sector de las Interrupciones
;********************************************************************
IntRST1:
		;Ac� va el c�digo de la Interrupci�n RST1
		EI
		RET
IntRST2:
		;Ac� va el c�digo de la Interrupci�n RST2
		EI
		RET
IntRST3:
		;Ac� va el c�digo de la Interrupci�n RST3
		EI
		RET
IntRST4:
		;Ac� va el c�digo de la Interrupci�n RST4
		EI
		RET
IntTRAP:
		;Ac� va el c�digo de la Interrupci�n TRAP
		EI
		RET
IntRST5:
		;Ac� va el c�digo de la Interrupci�n RST5
		EI
		RET
IntRST55:
		;Ac� va el c�digo de la Interrupci�n RST5.5
		EI
		RET
IntRST6:
		;Ac� va el c�digo de la Interrupci�n RST6
		EI
		RET
IntRST65:
		;Ac� va el c�digo de la Interrupci�n RST6.5
		EI
		RET
IntRST7:
		;Ac� va el c�digo de la Interrupci�n RST7
		EI
		RET
IntRST75:
		;Ac� va el c�digo de la Interrupci�n RST7.5
		EI
		RET
;********************************************************************
;	Sector del Programa Principal
;********************************************************************
Boot:
	LXI		SP,STACK_ADDR	;Inicializo el Puntero de Pila
	
	CALL	Init_PPI
	
Loop:	
	IN		ADDRA
	CALL	BCD7SEG
	;CALL	BCDTO7SEG
	OUT		ADDRB	
	JMP Loop

;******************************************************************************
; Funci�n: Init_PPI
; Descripci�n: Inicializa la PPI
; Entrada: ninguna
; Salida: ninguna	 
;******************************************************************************
Init_PPI:
	MVI A,10011001B 	; Conf. PPI MODO 0
    OUT ADDRRC		; A:IN, B:OUT C:IN
	RET

;******************************************************************************
; Funci�n: BCD7SEG
; Descripci�n: Convierte un n�mero BCD a 7 segmentos.
; Entrada: A (n�mero BCD a convertir)
; Salida: A (representaci�n en 7 segmentos)	 
;******************************************************************************
BCD7SEG:
	CPI	0
	JNZ	NO0
	LDA	Numero0
	RET
NO0:
	CPI	1
	JNZ	NO1
	LDA	Numero1
	RET
NO1:	
	CPI	2
	JNZ	NO2
	LDA	Numero2
	RET
NO2:
	CPI	3
	JNZ	NO3
	LDA	Numero3
	RET
NO3:
	CPI	4
	JNZ	NO4
	LDA	Numero4
	RET
NO4:
	CPI	5
	JNZ	NO5
	LDA	Numero5
	RET
NO5:
	CPI	6
	JNZ	NO6
	LDA	Numero6
	RET
NO6:
	CPI	7
	JNZ	NO7
	LDA	Numero7
	RET
NO7:
	CPI	8
	JNZ	NO8
	LDA	Numero8
	RET
NO8:
	CPI	9
	JNZ	NO9
	LDA	Numero9
	RET
NO9:
	LDA	LetraE
	RET

BCDTO7SEG:
	PUSH H
	LXI H,Numero0
	ANI	0Fh
	ADD L
	MOV L,A
	MOV A,M
	POP H
	RET
	
	HLT
	