;******************************************************************************
;T�cnicas Digitales II
;Ejemplo de ingreso de texto y desplazamiento en el display
;Ing. Maggiolo Gustavo
;******************************************************************************

;******************************************************************************
;	Definici�n de Etiquetas
;******************************************************************************

.define
	BootAddr		0000h
	AddrIntRST1		0008h
	AddrIntRST2		0010h
	AddrIntRST3		0018h
	AddrIntRST4		0020h
	AddrIntTRAP		0024h
	AddrIntRST5		0028h
	AddrIntRST55	002Ch
	AddrIntRST6		0030h
	AddrIntRST65	0034h
	AddrIntRST7		0038h
	AddrIntRST75	003Ch

	STACK_ADDR		FFFFh

	IniDataROM		0540h
	IniDataRAM		2000h

	MaskSetEn		8
	M75			4
	M65			2
	M55			1
	

	Keyboard		03h
	KEYPRESS		01h
	
	

	AddrDISP		00h

	sega			0200h
	segb			0400h
	segc			4000h
	segd			2000h
	sege			1000h
	segf			0100h
	segg			0800h
	segh			0002h
	segi			0004h
	segj			0010h
	segk			0080h
	segl			0040h
	segm			0020h
	segn			0008h
	sego			0001h
	ptop			8000h

	DIG0			0
	DIG1			2
	DIG2			4
	DIG3			6
	DIG4			8
	DIG5			10
	DIG6			12
	DIG7			14
	


	
;******************************************************************************
;	Definici�n de Datos en RAM (Variables)
;******************************************************************************
.data	IniDataRAM

Data_KBD:		dB		0
Flags:		dB		0

DisplayRAM:		dW		0000h,0000h,0000h,0000h,0,0,0,0	;Datos de los display en RAM
NewDisplayRAM:	dW		0


;******************************************************************************
;	Definici�n de Datos en ROM (Constantes)
;******************************************************************************
.data	IniDataROM	
CteWord:		dW	03E8h
CteByte:		dB	64h
		
;Digitos para los display

Dig8SegSPto:	DB 77h, 44h, 3Eh, 6Eh, 4Dh, 6Bh, 7Bh, 46h, 7Fh, 4Fh		;digitos sin punto
Dig8SegCPto:	DB F7h, C4h, BEh, EEh, CDh, EBh, FBh, C6h, FFh, CFh		;digitos con punto

Numero0:		dW sega|segb|segc|segd|sege|segf
Numero1:		dW segb|segc
Numero2:		dW sega|segb|segg|sege|segd
Numero3:		dW sega|segb|segc|segd|segg
Numero4:		dW segf|segg|segb|segc
Numero5:		dW sega|segf|segg|segc|segd
Numero6:		dW segf|sege|segd|segc|segg
Numero7:		dW sega|segb|segc
Numero8:		dW sega|segb|segc|segd|sege|segf|segg
Numero9:		dW sega|segb|segc|segg|segf

PtoDec:		dW ptop

Letra_A:		dW sege|segf|sega|segb|segc|segg
Letra_b:		dW segc|segd|sege|segf|segg
Letra_c:		dW segd|sege|segg
Letra_C:		dW segd|sege|segf|sega
Letra_d:		dW segb|segc|segd|sege|segg
Letra_e:		dW sega|segb|segd|sege|segf|segg
Letra_E:		dW sega|segd|sege|segf|segn
Letra_F:		dW sega|sege|segf|segn
Letra_G:		dW sega|segc|segd|segf|sege|segj
Letra_H:		dW segb|segc|sege|segf|segg
Letra_h:		dW segc|sege|segf|segg
Letra_I:		dW sega|segd|segh|segl
Letra_J:		dW segb|segc|segd|sege
Letra_K:		dW sege|segf|segi|segn|segk
Letra_L:		dW sege|segf|segd
Letra_M:		dW segb|segc|sege|segf|sego|segi
Letra_N:		dW segb|segc|sege|segf|sego|segk
Letra_o:		dW segc|segd|sege|segg
Letra_O:		dW sega|segb|segc|segd|sege|segf
Letra_Q:		dW sega|segb|segc|segd|sege|segf|segk
Letra_P:		dW sega|segb|sege|segf|segg
Letra_R:		dW sega|segb|sege|segf|segg|segk
Letra_r:		dW sege|segg
Letra_s:		dW sega|segf|segg|segd|segc
Letra_t:		dW sega|segh|segl
Letra_u:		dW segc|segd|sege
Letra_U:		dW segb|segc|segd|sege|segf
Letra_W:		dW segb|segc|segk|segm|sege|segf
Letra_X:		dW segi|segm|sego|segk
Letra_Y:		dW sego|segi|segl
Letra_Z:		dW sega|segd|segi|segm

Simbol_*:		dW sego|segh|segi|segk|segl|segm|segg
Simbol_/:		dW segm|segi
Simbol_\:		dW sego|segk
Simbol_(:		dW segi|segk
Simbol_):		dW sego|segm
Simbol_^:		dW segm|segk
Simbol_mas:		dW segh|segl|segn|segj
Simbol_men:		dW segg
Simbol_equ:		dW segg|segd
Simbol_may:		dW sege|segf|sego|segm
Simbol_min:		dW segb|segc|segk|segi
Simbol_spa:		dW 0
Simbol_13:		dW segd
Simbol_14:		dW segf|segb
Simbol_15:		dW segh
Simbol_16:		dW sego
Simbol_17:		dW ptop


;		a		a: 0200h	
;	-------------	b: 0400h
;	|\	|    /|	c: 4000h	 
;	| \	|h  /	|	d: 2000h
;    f|  \	|  /	|b	e: 1000h
;	|  o\	| /i	|	f: 0100h
;	| n  \|/  j	|	g: 0800h
;	------g------	h: 0002h
;	|    /|\    |	i: 0004h
;	|  m/	| \k 	|	j: 0010h
;    e|  /	|  \	|c	k: 0080h
;	| / 	|l  \	|	l: 0040h
;	|/    |    \| p	m: 0020h
;	------------- .	n: 0008h
;		d		o: 0001h	
;				p: 8000h

	

;******************************************************************************
;	Sector de Arranque del 8085
;******************************************************************************
	.org	BootAddr
		JMP	Boot

;******************************************************************************
;	Sector del Vector de Interrupciones
;******************************************************************************
	.org	AddrIntRST1
		JMP	IntRST1
	.org	AddrIntRST2
		JMP	IntRST2
	.org	AddrIntRST3
		JMP	IntRST3
	.org	AddrIntRST4
		JMP	IntRST4
	.org	AddrIntTRAP
		JMP	IntTRAP
	.org	AddrIntRST5
		JMP	IntRST5
	.org	AddrIntRST55
		JMP	IntRST55
	.org	AddrIntRST6
		JMP	IntRST6
	.org	AddrIntRST65
		JMP	IntRST65
	.org	AddrIntRST7
		JMP	IntRST7
	.org	AddrIntRST75
		JMP	IntRST75

;******************************************************************************
;	Sector de las Interrupciones
;******************************************************************************

IntRST1:
		;Ac� va el c�digo de la Interrupci�n RST1
		EI
		RET
IntRST2:
		;Ac� va el c�digo de la Interrupci�n RST2
		EI
		RET
IntRST3:
		;Ac� va el c�digo de la Interrupci�n RST3
		EI
		RET
IntRST4:
		;Ac� va el c�digo de la Interrupci�n RST4
		EI
		RET
IntTRAP:
		;Ac� va el c�digo de la Interrupci�n TRAP
		EI
		RET
IntRST5:
		;Ac� va el c�digo de la Interrupci�n RST5
		EI
		RET
IntRST55:
		;Ac� va el c�digo de la Interrupci�n RST5.5
		EI
		RET
IntRST6:
		;Ac� va el c�digo de la Interrupci�n RST6
		EI
		RET
IntRST65:
		;Ac� va el c�digo de la Interrupci�n RST6.5
		PUSH	PSW
		
		LDA	Flags
		ANI	KEYPRESS
		JNZ	NOProcessKEY

		IN	Keyboard
		STA	Data_KBD
		
		MVI	A,KEYPRESS
		STA	Flags

NOProcessKEY:

		POP	PSW
		EI
		RET
IntRST7:
		;Ac� va el c�digo de la Interrupci�n RST7
		EI
		RET
IntRST75:
		;Ac� va el c�digo de la Interrupci�n RST7.5
		EI
		RET



;******************************************************************************
;	Sector del Programa Principal
;******************************************************************************
Boot:
		LXI	SP,STACK_ADDR	;Inicializo el Puntero de Pila
	
		MVI	A,MaskSetEn | M75 | M55		;0Dh para las interrupciones	
		SIM						;Inicializo las interrupciones
		EI
Main:
	
		LDA	Flags
		ANI	01h
		CNZ	ProcessKEY
		
		CALL	PrintDig
		
		JMP	Main
	
		HLT

;******************************************************************************
;	Funci�n: ProcessKEY
;	Funci�n: Esta es llamada despues de detectar que se presiono una tecla. 
;		   Luego de leer que tecla se presiono, carga en HL una constante 
;		   de 16 bits que representa en el display de 15 seg la tecla
;		   presionada. 
;******************************************************************************
ProcessKEY:
		PUSH	H
		PUSH	PSW

		LDA	Data_KBD
		
		CPI	'0'
		JNZ	NotIsNumber0
		LHLD	Numero0
		JMP	salir
NotIsNumber0:		
		CPI	'1'
		JNZ	NotIsNumber1
		LHLD	Numero1
		JMP	salir
NotIsNumber1:		
		CPI	'2'
		JNZ	NotIsNumber2
		LHLD	Numero2
		JMP	salir
NotIsNumber2:		
		CPI	'3'
		JNZ	NotIsNumber3
		LHLD	Numero3
		JMP	salir
NotIsNumber3:		
		CPI	'4'
		JNZ	NotIsNumber4
		LHLD	Numero4
		JMP	salir
NotIsNumber4:		
		CPI	'5'
		JNZ	NotIsNumber5
		LHLD	Numero5
		JMP	salir
NotIsNumber5:		
		CPI	'6'
		JNZ	NotIsNumber6
		LHLD	Numero6
		JMP	salir
NotIsNumber6:		
		CPI	'7'
		JNZ	NotIsNumber7
		LHLD	Numero7
		JMP	salir
NotIsNumber7:		
		CPI	'8'
		JNZ	NotIsNumber8
		LHLD	Numero8
		JMP	salir
NotIsNumber8:		
		CPI	'9'
		JNZ	NotIsNumber9
		LHLD	Numero9
		JMP	salir
NotIsNumber9:	
		CPI	'a'
		JNZ	NotIsLetraa
		LHLD	Letra_A
		JMP	salir
NotIsLetraa:		
		CPI	'b'
		JNZ	NotIsLetrab
		LHLD	Letra_b
		JMP	salir
NotIsLetrab:		
		CPI	'c'
		JNZ	NotIsLetrac
		LHLD	Letra_C
		JMP	salir
NotIsLetrac:		
		CPI	'd'
		JNZ	NotIsLetrad
		LHLD	Letra_d
		JMP	salir
NotIsLetrad:		
		CPI	'e'
		JNZ	NotIsLetrae
		LHLD	Letra_E
		JMP	salir
NotIsLetrae:		
		CPI	'f'
		JNZ	NotIsLetraf
		LHLD	Letra_F
		JMP	salir
NotIsLetraf:		
		CPI	'g'
		JNZ	NotIsLetrag
		LHLD	Letra_G
		JMP	salir
NotIsLetrag:		
		CPI	'h'
		JNZ	NotIsLetrah
		LHLD	Letra_H
		JMP	salir
NotIsLetrah:		
		CPI	'i'
		JNZ	NotIsLetrai
		LHLD	Letra_I
		JMP	salir
NotIsLetrai:		
		CPI	'j'
		JNZ	NotIsLetraj
		LHLD	Letra_J
		JMP	salir
NotIsLetraj:		
		CPI	'k'
		JNZ	NotIsLetrak
		LHLD	Letra_K
		JMP	salir
NotIsLetrak:		
		CPI	'l'
		JNZ	NotIsLetral
		LHLD	Letra_L
		JMP	salir
NotIsLetral:		
		CPI	'm'
		JNZ	NotIsLetram
		LHLD	Letra_M
		JMP	salir
NotIsLetram:		
		CPI	'n'
		JNZ	NotIsLetran
		LHLD	Letra_N
		JMP	salir
NotIsLetran:		
		CPI	'o'
		JNZ	NotIsLetrao
		LHLD	Letra_o
		JMP	salir
NotIsLetrao:		
		CPI	'p'
		JNZ	NotIsLetrap
		LHLD	Letra_P
		JMP	salir
NotIsLetrap:		
		CPI	'q'
		JNZ	NotIsLetraq
		LHLD	Letra_Q
		JMP	salir
NotIsLetraq:		
		CPI	'R'
		JNZ	NotIsLetraR
		LHLD	Letra_r
		JMP	salir
NotIsLetraR:		
		CPI	'r'
		JNZ	NotIsLetrar
		LHLD	Letra_r
		JMP	salir
NotIsLetrar:		
		CPI	's'
		JNZ	NotIsLetras
		LHLD	Letra_s
		JMP	salir
NotIsLetras:		
		CPI	't'
		JNZ	NotIsLetrat
		LHLD	Letra_t
		JMP	salir
NotIsLetrat:		
		CPI	'U'
		JNZ	NotIsLetraU
		LHLD	Letra_U
		JMP	salir
NotIsLetraU:
		CPI	'u'
		JNZ	NotIsLetrau
		LHLD	Letra_u
		JMP	salir
NotIsLetrau:		
		CPI	'w'
		JNZ	NotIsLetraw
		LHLD	Letra_W
		JMP	salir
NotIsLetraw:		
		CPI	'x'
		JNZ	NotIsLetrax
		LHLD	Letra_X
		JMP	salir
NotIsLetrax:		
		CPI	'y'
		JNZ	NotIsLetray
		LHLD	Letra_Y
		JMP	salir
NotIsLetray:		
		CPI	'z'
		JNZ	NotIsLetraz
		LHLD	Letra_Z
		JMP	salir
NotIsLetraz:		
		
		CPI	'*'
		JNZ	NotIsSimbol1
		LHLD	Simbol_*
		JMP	salir
NotIsSimbol1:
		CPI	'/'
		JNZ	NotIsSimbol2
		LHLD	Simbol_/
		JMP	salir
NotIsSimbol2:
		CPI	'\'
		JNZ	NotIsSimbol3
		LHLD	Simbol_\
		JMP	salir
NotIsSimbol3:
		CPI	'('
		JNZ	NotIsSimbol4
		LHLD	Simbol_(
		JMP	salir
NotIsSimbol4:
		CPI	')'
		JNZ	NotIsSimbol5
		LHLD	Simbol_)
		JMP	salir
NotIsSimbol5:
		CPI	'^'
		JNZ	NotIsSimbol6
		LHLD	Simbol_^
		JMP	salir
NotIsSimbol6:
		CPI	'+'
		JNZ	NotIsSimbol7
		LHLD	Simbol_mas
		JMP	salir
NotIsSimbol7:
		CPI	'-'
		JNZ	NotIsSimbol8
		LHLD	Simbol_men
		JMP	salir
NotIsSimbol8:
		CPI	'='
		JNZ	NotIsSimbol9
		LHLD	Simbol_equ
		JMP	salir
NotIsSimbol9:
		CPI	'>'
		JNZ	NotIsSimbol10
		LHLD	Simbol_may
		JMP	salir
NotIsSimbol10:
		CPI	'<'
		JNZ	NotIsSimbol11
		LHLD	Simbol_min
		JMP	salir
NotIsSimbol11:
		CPI	' '
		JNZ	NotIsSimbol12
		LHLD	Simbol_spa
		JMP	salir
NotIsSimbol12:
		CPI	'_'
		JNZ	NotIsSimbol13
		LHLD	Simbol_13
		JMP	salir
NotIsSimbol13:
		CPI	'"'
		JNZ	NotIsSimbol14
		LHLD	Simbol_14
		JMP	salir
NotIsSimbol14:
		CPI	'''
		JNZ	NotIsSimbol15
		LHLD	Simbol_15
		JMP	salir
NotIsSimbol15:
		CPI	'`'
		JNZ	NotIsSimbol16
		LHLD	Simbol_16
		JMP	salir
NotIsSimbol16:
		CPI	2Eh
		JNZ	NotIsSimbol17
		LHLD	Simbol_17
		JMP	salir
NotIsSimbol17:
		
		
		LXI	H,0000h
		JMP	NoDato
		
salir:
		SHLD	NewDisplayRAM
		
		CALL	Desplazar
		
NoDato:	
		
		LDA	Flags
		ANI	FEh			;Borro el flags de tecla presionada
		STA	Flags
		
		POP	PSW
		POP	H
		RET

;******************************************************************************
;	Funci�n: Desplazar
;	Funci�n: Desplaza una posici�n a la izquierda el contenido del 
;		   vector DisplayRAM. Por el lado derecho entra el dato que esta
;		   en NewDisplayRAM
;	 _Dig_ 		 _RAM__		 _I/O_
;	|     |		| 2002 |		|  0	|
;	|  0  |		| 2003 |		|  1	|
;	|_____|		|______|		|_____|
;	|     |		| 2004 |		|  2	|
;	|  1  |		| 2005 |		|  3	|
;	|_____|		|______|		|_____|
;	|     |		| 2006 |		|  4	|
;	|  2  |		| 2007 |		|  5	|	
;	|_____|		|______| 		|_____|	
;	|     |		| 2008 |		|  6	|
;	|  3  |		| 2009 |		|  7	|
;	|_____|		|______|		|_____|
;	|     |		| 200A |		|  8	|
;	|  4  |		| 200B |		|  9	|
;	|_____|		|______|		|_____|
;	|     |		| 200C |		|  A	|
;	|  5  |		| 200D |		|  B	|
;	|_____|		|______|		|_____|
;	|     |		| 200E |		|  C	|
;	|  6  |		| 200F |		|  D	|
;	|_____|		|______|		|_____|
;	|     |		| 2010 |		|  E	|
;	|  7  |		| 2011 |		|  F	|
;	|_____|		|______|		|_____|
;******************************************************************************
Desplazar:
		PUSH	H

		LHLD	DisplayRAM+DIG1
		SHLD	DisplayRAM+DIG0
		LHLD	DisplayRAM+DIG2
		SHLD	DisplayRAM+DIG1
		LHLD	DisplayRAM+DIG3
		SHLD	DisplayRAM+DIG2
		LHLD	DisplayRAM+DIG4
		SHLD	DisplayRAM+DIG3
		LHLD	DisplayRAM+DIG5
		SHLD	DisplayRAM+DIG4
		LHLD	DisplayRAM+DIG6
		SHLD	DisplayRAM+DIG5
		LHLD	DisplayRAM+DIG7
		SHLD	DisplayRAM+DIG6
		LHLD	NewDisplayRAM
		SHLD	DisplayRAM+DIG7
		
		POP	H		
		RET	
;******************************************************************************
;	Funci�n: PrintDig
;	Funci�n: Imprime los N�meros en los display de 15 Segmentos
;		   Los datos son tomados de un buffer denominado DisplayRAM
;	 _____   _____   _____   _____   _____   _____   _____   _____	
;	|     | |     | |     | |     | |     | |     | |     | |     |
;	|     | |     | |     | |     | |     | |     | |     | |     |
;	|  0  | |  1  | |  2  | |  3  | |  4  | |  5  | |  6  | |  7  |
;	|     | |     | |     | |     | |     | |     | |     | |     |
;	|_____| |_____| |_____| |_____| |_____| |_____| |_____| |_____|
;******************************************************************************
PrintDig:
		PUSH	H
		
		CALL	PrintDig0
		CALL	PrintDig1
		CALL	PrintDig2
		CALL	PrintDig3
		CALL	PrintDig4
		CALL	PrintDig5
		CALL	PrintDig6
		CALL	PrintDig7
		
		POP	H
		RET	
;******************************************************************************
;	Funci�n: PrintDig0
;	Funci�n: Imprime el Digito 0 en los display de 15 Segmentos
;		   Los datos son tomados de un buffer denominado DisplayRAM
;	
;******************************************************************************
PrintDig0:	
		LHLD	DisplayRAM+DIG0
		MOV	A,L
		OUT	AddrDISP+DIG0
		MOV	A,H
		OUT	AddrDISP+DIG0+1
		RET

PrintDig1:	
		LHLD	DisplayRAM+DIG1
		MOV	A,L
		OUT	AddrDISP+DIG1
		MOV	A,H
		OUT	AddrDISP+DIG1+1
		RET


PrintDig2:	
		LHLD	DisplayRAM+DIG2
		MOV	A,L
		OUT	AddrDISP+DIG2
		MOV	A,H
		OUT	AddrDISP+DIG2+1
		RET

PrintDig3:	
		LHLD	DisplayRAM+DIG3
		MOV	A,L
		OUT	AddrDISP+DIG3
		MOV	A,H
		OUT	AddrDISP+DIG3+1
		RET

PrintDig4:	
		LHLD	DisplayRAM+DIG4
		MOV	A,L
		OUT	AddrDISP+DIG4
		MOV	A,H
		OUT	AddrDISP+DIG4+1
		RET

PrintDig5:	
		LHLD	DisplayRAM+DIG5
		MOV	A,L
		OUT	AddrDISP+DIG5
		MOV	A,H
		OUT	AddrDISP+DIG5+1
		RET

PrintDig6:	
		LHLD	DisplayRAM+DIG6
		MOV	A,L
		OUT	AddrDISP+DIG6
		MOV	A,H
		OUT	AddrDISP+DIG6+1
		RET

PrintDig7:	
		LHLD	DisplayRAM+DIG7
		MOV	A,L
		OUT	AddrDISP+DIG7
		MOV	A,H
		OUT	AddrDISP+DIG7+1
		RET
