;********************************************************************
;T�cnicas Digitales II
;Ejercitaci�n: Ordenar un bloque de datos, que comienzan en la 
;		   direcci�n 8000h (son 200 datos), y almacenarlos a 
;		   partir de la posici�n 8100h seg�n el criterio mostrado
;		   m�s abajo.
;Ing. Maggiolo Gustavo
;********************************************************************

;********************************************************************
;	Definici�n de Etiquetas
;********************************************************************

.define
	BootAddr		0000h
	AddrIntRST1		0008h
	AddrIntRST2		0010h
	AddrIntRST3		0018h
	AddrIntRST4		0020h
	AddrIntTRAP		0024h
	AddrIntRST5		0028h
	AddrIntRST55	002Ch
	AddrIntRST6		0030h
	AddrIntRST65	0034h
	AddrIntRST7		0038h
	AddrIntRST75	003Ch

	STACK_ADDR		FFFFh

	IniDataROM		0540h		;Comienzo de Constantes en ROM
	IniDataRAM		8500h		;Comienzo de Variables en RAM

	

;********************************************************************
;	Definici�n de Datos en RAM (Variables)
;********************************************************************

.data	IniDataRAM
Contador:		db		0
PtroDatos:		dw		0		
PtroBloque1:	dw		0
PtroBloque2:	dw		0
PtroBloque3:	dw		0
PtroBloque4:	dw		0
	
;********************************************************************
;	Definici�n de Datos en ROM (Constantes)
;********************************************************************

.data	IniDataROM
	
CteWord:		dW	03E8h
CteByte:		dB	64h

;********************************************************************
;	Sector de Arranque del 8085
;********************************************************************

	.org	BootAddr
		JMP	Boot
;********************************************************************
;	Sector del Vector de Interrupciones
;********************************************************************
	.org	AddrIntRST1
		JMP	IntRST1
	.org	AddrIntRST2
		JMP	IntRST2
	.org	AddrIntRST3
		JMP	IntRST3
	.org	AddrIntRST4
		JMP	IntRST4
	.org	AddrIntTRAP
		JMP	IntTRAP
	.org	AddrIntRST5
		JMP	IntRST5
	.org	AddrIntRST55
		JMP	IntRST55
	.org	AddrIntRST6
		JMP	IntRST6
	.org	AddrIntRST65
		JMP	IntRST65
	.org	AddrIntRST7
		JMP	IntRST7
	.org	AddrIntRST75
		JMP	IntRST75
;********************************************************************
;	Sector de las Interrupciones
;********************************************************************
IntRST1:
		;Ac� va el c�digo de la Interrupci�n RST1
		EI
		RET
IntRST2:
		;Ac� va el c�digo de la Interrupci�n RST2
		EI
		RET
IntRST3:
		;Ac� va el c�digo de la Interrupci�n RST3
		EI
		RET
IntRST4:
		;Ac� va el c�digo de la Interrupci�n RST4
		EI
		RET
IntTRAP:
		;Ac� va el c�digo de la Interrupci�n TRAP
		EI
		RET
IntRST5:
		;Ac� va el c�digo de la Interrupci�n RST5
		EI
		RET
IntRST55:
		;Ac� va el c�digo de la Interrupci�n RST5.5
		EI
		RET
IntRST6:
		;Ac� va el c�digo de la Interrupci�n RST6
		EI
		RET
IntRST65:
		;Ac� va el c�digo de la Interrupci�n RST6.5
		EI
		RET
IntRST7:
		;Ac� va el c�digo de la Interrupci�n RST7
		EI
		RET
IntRST75:
		;Ac� va el c�digo de la Interrupci�n RST7.5
		EI
		RET
;********************************************************************
;	Sector del Programa Principal
;********************************************************************
Boot:
	LXI	SP,STACK_ADDR	;Inicializo el Puntero de Pila
	
Main:	
	LXI	H,8000h
	SHLD	PtroDatos		;Inicializo puntero de datos
	LXI	H,8100h
	SHLD	PtroBloque1		;Inicializo puntero de Bloque 1
	LXI	H,8200h
	SHLD	PtroBloque2		;Inicializo puntero de Bloque 2
	LXI	H,8300h
	SHLD	PtroBloque3		;Inicializo puntero de Bloque 3
	LXI	H,8400h
	SHLD	PtroBloque4		;Inicializo puntero de Bloque 4
	MVI	A,0
	STA	Contador

MainLoop:
	CALL	PROCESS
	
	HLT

;******************************************************************************
; Funci�n: PROCESS
; Descripci�n: 
;			Funci�n que procesa un bloque de datos en RAM, y los ordena de 
;		   	acuerdo al siguiente criterio:
; ---------- 8000h	 ----------	 8100h	 ----------	 8200h
;|		|   		| 		|		|Dato >= 64	|
;|		|   		| Dato < 64	|		|		|	
;|		|   		| 		|		|Dato < 128	|
;|   200	|   		 -----------		 ----------
;|   Datos	|   		
;|		|		 ----------	 8300h	 ----------	 8400h
;|		|   		|Dato >= 128|		|		|
;|		|   		| 		|		|Dato >= 192|
;|		|		|Dato < 192	|		|		|
; ---------	   		 -----------		 ----------
;******************************************************************************
PROCESS:
	PUSH H
	PUSH PSW
Loop:
	LDA	Contador
	CPI	200
	JZ	Fin
	INR	A
	STA	Contador

	LHLD	PtroDatos		;Cargo puntero de dato
	MOV	A,M			;Cargo dato ha comparar
	INX	H			;Incremento puntero de datos
	SHLD	PtroDatos		;Guardo puntero de dato

	CPI	192			;Dato - 192 
	JZ	Mayor192		;Si Z => = 192
	JNC	Mayor192		;Si NC => > 192
					;Si continua, => < 192
	CPI	128			;Dato - 128
	JZ	EntreSup		;Si Z => = 128
	JNC	EntreSup		;Si NC => > 128
					;Si continua, => < 128
	CPI	64			;Dato - 64
	JZ	EntreInf		;Si Z => = 64
	JNC	EntreInf		;Si NC => > 64
					;Si continua, => < 64
Menor64:
	LHLD	PtroBloque1		;Cargo puntero de Bloque 1
	MOV	M,A
	INX	H
	SHLD	PtroBloque1		;Guardo puntero de Bloque 1
	JMP	Loop

EntreInf:
	LHLD	PtroBloque2		;Cargo puntero de Bloque 2
	MOV	M,A
	INX	H
	SHLD	PtroBloque2		;Guardo puntero de Bloque 2
	JMP	Loop

EntreSup:
	LHLD	PtroBloque3		;Cargo puntero de Bloque 3
	MOV	M,A
	INX	H
	SHLD	PtroBloque3		;Guardo puntero de Bloque 3
	JMP	Loop

Mayor192:
	LHLD	PtroBloque4		;Cargo puntero de Bloque 4
	MOV	M,A
	INX	H
	SHLD	PtroBloque4		;Guardo puntero de Bloque 4
	JMP	Loop

Fin:
	POP PSW
	POP H
	RET





