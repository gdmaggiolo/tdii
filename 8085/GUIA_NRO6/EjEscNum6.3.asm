;******************************************************************************
;T�cnicas Digitales II
;Ejercitaci�n Nro 6.3
;Autor: Ing. Maggiolo Gustavo
;******************************************************************************

; Ejercitaci�n Nro 6.3
; Se dispone de un dispositivo perif�rico que avisa al microcontrolador
; que tiene un dato listo para enviarle, a trav�s de una interrupci�n,
; la RST 7.5. Una vez producida la interrupci�n el micro tendr� que ir
; almacenando los datos que le env�a el perif�rico a partir de la 
; direcci�n 2500h. Si el dato es "*", se deber� guardar este �ltimo
; dato, y se deshabilitara la interrupci�n (fin de datos). La direcci�n
; del perif�rico es 23h.

;********************************************************************
;	Definici�n de Etiquetas
;********************************************************************

.define
	BootAddr			0000h
	AddrIntRST1		0008h
	AddrIntRST2		0010h
	AddrIntRST3		0018h
	AddrIntRST4		0020h
	AddrIntTRAP		0024h
	AddrIntRST5		0028h
	AddrIntRST55		002Ch
	AddrIntRST6		0030h
	AddrIntRST65		0034h
	AddrIntRST7		0038h
	AddrIntRST75		003Ch

	STACK_ADDR		FFFFh

	IniDataROM		0540h		;Comienzo de Constantes en ROM
	IniDataRAM		24FEh		;Comienzo de Variables en RAM

	MaskSetEn		8
	M75			4
	M65			2
	M55			1


;********************************************************************
;	Definici�n de Datos en RAM (Variables)
;********************************************************************

.data	IniDataRAM
pTrBuffer:	dw		0
Buffer:	dB		0
	
;********************************************************************
;	Definici�n de Datos en ROM (Constantes)
;********************************************************************

.data	IniDataROM

CteWord:		dW	03E8h
CteByte:		dB	64h
	
Texto:		dB	'C','a','d','e','n','a',0

;********************************************************************
;	Sector de Arranque del 8085
;********************************************************************

	.org	BootAddr
		JMP	Boot

;********************************************************************
;	Sector del Vector de Interrupciones
;********************************************************************
	.org	AddrIntRST1
		JMP	IntRST1
	.org	AddrIntRST2
		JMP	IntRST2
	.org	AddrIntRST3
		JMP	IntRST3
	.org	AddrIntRST4
		JMP	IntRST4
	.org	AddrIntTRAP
		JMP	IntTRAP
	.org	AddrIntRST5
		JMP	IntRST5
	.org	AddrIntRST55
		JMP	IntRST55
	.org	AddrIntRST6
		JMP	IntRST6
	.org	AddrIntRST65
		JMP	IntRST65
	.org	AddrIntRST7
		JMP	IntRST7
	.org	AddrIntRST75
		JMP	IntRST75

;********************************************************************
;	Sector de las Interrupciones
;********************************************************************
IntRST1:
		;Ac� va el c�digo de la Interrupci�n RST1
		EI
		RET
IntRST2:
		;Ac� va el c�digo de la Interrupci�n RST2
		EI
		RET
IntRST3:
		;Ac� va el c�digo de la Interrupci�n RST3
		EI
		RET
IntRST4:
		;Ac� va el c�digo de la Interrupci�n RST4
		EI
		RET
IntTRAP:
		;Ac� va el c�digo de la Interrupci�n TRAP
		EI
		RET
IntRST5:
		;Ac� va el c�digo de la Interrupci�n RST5
		EI
		RET
IntRST55:
		;Ac� va el c�digo de la Interrupci�n RST5.5
		EI
		RET
IntRST6:
		;Ac� va el c�digo de la Interrupci�n RST6
		EI
		RET
IntRST65:
		;Ac� va el c�digo de la Interrupci�n RST6.5
		EI
		RET
IntRST7:
		;Ac� va el c�digo de la Interrupci�n RST7
		EI
		RET
IntRST75:
		;Ac� va el c�digo de la Interrupci�n RST7.5
		PUSH	PSW
		PUSH	H

		IN	23h		; Leo perif�rico
		CPI	'*'
		CZ	DisableINT
		
		LHLD pTrBuffer
		MOV	M,A
		INX	H
		SHLD pTrBuffer

		POP	H		
		POP	PSW
		EI
		RET

DisableINT:
		PUSH	PSW
		MVI	A,MaskSetEn | M75 | M65 | M55	;0Fh para las interrupciones	
		SIM						;Inicializo las interrupciones
		POP	PSW
		RET
;********************************************************************
;	Sector del Programa Principal
;********************************************************************
Boot:
	LXI	SP,STACK_ADDR	;Inicializo el Puntero de Pila
	
	XRA	A
	STA	pTrBuffer		;Inicializo Puntero en RAM
	MVI	A, 25h
	STA	pTrBuffer+1
	
		
	MVI	A,MaskSetEn | M65 | M55		;0Bh para las interrupciones	
	SIM						;Inicializo las interrupciones
	EI
	
Main:
	NOP
	JMP	Main
	
	HLT
	