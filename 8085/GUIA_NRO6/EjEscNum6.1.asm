;******************************************************************************
;T�cnicas Digitales II
;Ejercitaci�n Nro 6.1
;Autor: Ing. Maggiolo Gustavo
;******************************************************************************

; Ejercitaci�n Nro 6.1
; Hacer un programa, en el que, cada vez que se produzca una
; interrupci�n TRAP se incremente el valor de la posici�n de 
; memoria 2000h.

;********************************************************************
;	Definici�n de Etiquetas
;********************************************************************

.define
	BootAddr			0000h
	AddrIntRST1		0008h
	AddrIntRST2		0010h
	AddrIntRST3		0018h
	AddrIntRST4		0020h
	AddrIntTRAP		0024h
	AddrIntRST5		0028h
	AddrIntRST55		002Ch
	AddrIntRST6		0030h
	AddrIntRST65		0034h
	AddrIntRST7		0038h
	AddrIntRST75		003Ch

	STACK_ADDR		FFFFh

	IniDataROM		0540h		;Comienzo de Constantes en ROM
	IniDataRAM		2000h		;Comienzo de Variables en RAM

	MaskSetEn		8
	M75			4
	M65			2
	M55			1


;********************************************************************
;	Definici�n de Datos en RAM (Variables)
;********************************************************************

.data	IniDataRAM
Var1:			dB		0		;Inicializo la variable en 16
Var2:		dB		0
Var3:		dB		0
Var4:		dB		0

	
;********************************************************************
;	Definici�n de Datos en ROM (Constantes)
;********************************************************************

.data	IniDataROM
	
CteWord:		dW	03E8h
CteByte:		dB	64h
		
		
Texto:		dB	'C','a','d','e','n','a',0

	

;********************************************************************
;	Sector de Arranque del 8085
;********************************************************************

	.org	BootAddr
		JMP	Boot

;********************************************************************
;	Sector del Vector de Interrupciones
;********************************************************************
	.org	AddrIntRST1
		JMP	IntRST1
	.org	AddrIntRST2
		JMP	IntRST2
	.org	AddrIntRST3
		JMP	IntRST3
	.org	AddrIntRST4
		JMP	IntRST4
	.org	AddrIntTRAP
		JMP IntTRAP
	.org	AddrIntRST5
		JMP	IntRST5
	.org	AddrIntRST55
		JMP	IntRST55
	.org	AddrIntRST6
		JMP	IntRST6
	.org	AddrIntRST65
		JMP	IntRST65
	.org	AddrIntRST7
		JMP	IntRST7
	.org	AddrIntRST75
		JMP	IntRST75

;********************************************************************
;	Sector de las Interrupciones
;********************************************************************
IntRST1:
		;Ac� va el c�digo de la Interrupci�n RST1
		EI
		RET
IntRST2:
		;Ac� va el c�digo de la Interrupci�n RST2
		EI
		RET
IntRST3:
		;Ac� va el c�digo de la Interrupci�n RST3
		EI
		RET
IntRST4:
		;Ac� va el c�digo de la Interrupci�n RST4
		EI
		RET
IntTRAP:
		;Ac� va el c�digo de la Interrupci�n TRAP
		PUSH	PSW
		
		LDA	2000h
		INR	A
		STA	2000h
		
		POP	PSW
		
		EI
		RET
IntRST5:
		;Ac� va el c�digo de la Interrupci�n RST5
		EI
		RET
IntRST55:
		;Ac� va el c�digo de la Interrupci�n RST5.5
		EI
		RET
IntRST6:
		;Ac� va el c�digo de la Interrupci�n RST6
		EI
		RET
IntRST65:
		;Ac� va el c�digo de la Interrupci�n RST6.5
		EI
		RET
IntRST7:
		;Ac� va el c�digo de la Interrupci�n RST7
		EI
		RET
IntRST75:
		;Ac� va el c�digo de la Interrupci�n RST7.5
		EI
		RET


;********************************************************************
;	Sector del Programa Principal
;********************************************************************
Boot:
	LXI	SP,STACK_ADDR	;Inicializo el Puntero de Pila
	
	MVI	B,0

	MVI	A,MaskSetEn | M75 | M65 | M55		;0Fh para las interrupciones	
	SIM							;Inicializo las interrupciones
	EI
	
Main:
	NOP
	JMP	Main
	
	HLT
	