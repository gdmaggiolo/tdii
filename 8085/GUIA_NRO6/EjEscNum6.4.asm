;******************************************************************************
;T�cnicas Digitales II
;Ejercitaci�n Nro 6.4
;Autor: Ing. Maggiolo Gustavo
;******************************************************************************

; Ejercitaci�n Nro 6.4
; Realizar el circuito y el programa para implementar un reloj digital.
; El formato del sistema es HH.MM.SS. Los display de 7 segmentos est�n a
; partir de la direcci�n 10h, consecutivamente. La entrada de la base de
; tiempo es por medio de la interrupci�n RST5.5 cada 100mseg. En el circuito
; incorporar el reloj externo que genere la base de tiempo.
;******************************************************************************

;******************************************************************************
;	Definici�n de Etiquetas
;******************************************************************************

.define
	BootAddr			0000h
	AddrIntRST1		0008h
	AddrIntRST2		0010h
	AddrIntRST3		0018h
	AddrIntRST4		0020h
	AddrIntTRAP		0024h
	AddrIntRST5		0028h
	AddrIntRST55		002Ch
	AddrIntRST6		0030h
	AddrIntRST65		0034h
	AddrIntRST7		0038h
	AddrIntRST75		003Ch

	STACK_ADDR		FFFFh

	IniDataROM		0540h
	IniDataRAM		2000h

	MaskSetEn		8
	M75			4
	M65			2
	M55			1

	Keyboard		03h
	KEYPRESS		01h
	
	AddrDISP15S		10h
	AddrDISP7S		10h

	sega			0200h
	segb			0400h
	segc			4000h
	segd			2000h
	sege			1000h
	segf			0100h
	segg			0800h
	segh			0002h
	segi			0004h
	segj			0010h
	segk			0080h
	segl			0040h
	segm			0020h
	segn			0008h
	sego			0001h
	ptop			8000h

	DIG0			0
	DIG1				2
	DIG2			4
	DIG3			6
	DIG4			8
	DIG5			10
	DIG6			12
	DIG7			14
	
;******************************************************************************
;	Definici�n de Datos en RAM (Variables)
;******************************************************************************
.data	IniDataRAM

TimeBase:		dB		0
Seg:			db		0
Min:			db		0
Hor:			db		0
Data_KBD:	dB		0
Flags:		dB		0
Reloj:		db		0,0,0,0,0,0
DisplayRAM:		dW		0000h,0000h,0000h,0000h,0,0,0,0	;Datos de los display en RAM
NewDisplayRAM:	dW		0


;******************************************************************************
;	Definici�n de Datos en ROM (Constantes)
;******************************************************************************
.data	IniDataROM	
CteWord:		dW	03E8h
CteByte:		dB	64h
		
;Digitos para los display

Dig8SegSPto:	DB 77h, 44h, 3Eh, 6Eh, 4Dh, 6Bh, 7Bh, 46h, 7Fh, 4Fh		;digitos sin punto
Dig8SegCPto:	DB F7h, C4h, BEh, EEh, CDh, EBh, FBh, C6h, FFh, CFh		;digitos con punto

Numero0:		dW sega|segb|segc|segd|sege|segf
Numero1:		dW segb|segc
Numero2:		dW sega|segb|segg|sege|segd
Numero3:		dW sega|segb|segc|segd|segg
Numero4:		dW segf|segg|segb|segc
Numero5:		dW sega|segf|segg|segc|segd
Numero6:		dW segf|sege|segd|segc|segg
Numero7:		dW sega|segb|segc
Numero8:		dW sega|segb|segc|segd|sege|segf|segg
Numero9:		dW sega|segb|segc|segg|segf

PtoDec:		dW ptop

Letra_A:		dW sege|segf|sega|segb|segc|segg
Letra_b:		dW segc|segd|sege|segf|segg
Letra_c:		dW segd|sege|segg
Letra_C:		dW segd|sege|segf|sega
Letra_d:		dW segb|segc|segd|sege|segg
Letra_e:		dW sega|segb|segd|sege|segf|segg
Letra_E:		dW sega|segd|sege|segf|segn
Letra_F:		dW sega|sege|segf|segn
Letra_G:		dW sega|segc|segd|segf|sege|segj
Letra_H:		dW segb|segc|sege|segf|segg
Letra_h:		dW segc|sege|segf|segg
Letra_I:		dW sega|segd|segh|segl
Letra_J:		dW segb|segc|segd|sege
Letra_K:		dW sege|segf|segi|segn|segk
Letra_L:		dW sege|segf|segd
Letra_M:		dW segb|segc|sege|segf|sego|segi
Letra_N:		dW segb|segc|sege|segf|sego|segk
Letra_o:		dW segc|segd|sege|segg
Letra_O:		dW sega|segb|segc|segd|sege|segf
Letra_Q:		dW sega|segb|segc|segd|sege|segf|segk
Letra_P:		dW sega|segb|sege|segf|segg
Letra_R:		dW sega|segb|sege|segf|segg|segk
Letra_r:		dW sege|segg
Letra_s:		dW sega|segf|segg|segd|segc
Letra_t:		dW sega|segh|segl
Letra_u:		dW segc|segd|sege
Letra_U:		dW segb|segc|segd|sege|segf
Letra_W:		dW segb|segc|segk|segm|sege|segf
Letra_X:		dW segi|segm|sego|segk
Letra_Y:		dW sego|segi|segl
Letra_Z:		dW sega|segd|segi|segm

Simbol_*:		dW sego|segh|segi|segk|segl|segm|segg
Simbol_/:		dW segm|segi
Simbol_\:		dW sego|segk
Simbol_(:		dW segi|segk
Simbol_):		dW sego|segm
Simbol_^:		dW segm|segk
Simbol_mas:		dW segh|segl|segn|segj
Simbol_men:		dW segg
Simbol_equ:		dW segg|segd
Simbol_may:		dW sege|segf|sego|segm
Simbol_min:		dW segb|segc|segk|segi
Simbol_spa:		dW 0
Simbol_13:		dW segd
Simbol_14:		dW segf|segb
Simbol_15:		dW segh
Simbol_16:		dW sego
Simbol_17:		dW ptop


;		a		a: 0200h	
;	-------------	b: 0400h
;	|\	|    /|	c: 4000h	 
;	| \	|h  /	|	d: 2000h
;    f|  \	|  /	|b	e: 1000h
;	|  o\	| /i	|	f: 0100h
;	| n  \|/  j	|	g: 0800h
;	------g------	h: 0002h
;	|    /|\    |	i: 0004h
;	|  m/	| \k 	|	j: 0010h
;    e|  /	|  \	|c	k: 0080h
;	| / 	|l  \	|	l: 0040h
;	|/    |    \| p	m: 0020h
;	------------- .	n: 0008h
;		d		o: 0001h	
;				p: 8000h

	

;******************************************************************************
;	Sector de Arranque del 8085
;******************************************************************************
	.org	BootAddr
		JMP	Boot

;******************************************************************************
;	Sector del Vector de Interrupciones
;******************************************************************************
	.org	AddrIntRST1
		JMP	IntRST1
	.org	AddrIntRST2
		JMP	IntRST2
	.org	AddrIntRST3
		JMP	IntRST3
	.org	AddrIntRST4
		JMP	IntRST4
	.org	AddrIntTRAP
		JMP	IntTRAP
	.org	AddrIntRST5
		JMP	IntRST5
	.org	AddrIntRST55
		JMP	IntRST55
	.org	AddrIntRST6
		JMP	IntRST6
	.org	AddrIntRST65
		JMP	IntRST65
	.org	AddrIntRST7
		JMP	IntRST7
	.org	AddrIntRST75
		JMP	IntRST75

;******************************************************************************
;	Sector de las Interrupciones
;******************************************************************************

IntRST1:
		;Ac� va el c�digo de la Interrupci�n RST1
		EI
		RET
IntRST2:
		;Ac� va el c�digo de la Interrupci�n RST2
		EI
		RET
IntRST3:
		;Ac� va el c�digo de la Interrupci�n RST3
		EI
		RET
IntRST4:
		;Ac� va el c�digo de la Interrupci�n RST4
		EI
		RET
IntTRAP:
		;Ac� va el c�digo de la Interrupci�n TRAP
		EI
		RET
IntRST5:
		;Ac� va el c�digo de la Interrupci�n RST5
		EI
		RET
IntRST55:
		;Ac� va el c�digo de la Interrupci�n RST5.5
		PUSH	PSW

		LDA	TimeBase
		INR	A
		CPI	0Ah
		JNZ	seguir
		LDA	Flags
		ORI	1h
		STA	Flags
		MVI	A,0
seguir:
		STA	TimeBase

		POP	PSW
		EI
		RET
IntRST6:
		;Ac� va el c�digo de la Interrupci�n RST6
		
		RET
IntRST65:
		;Ac� va el c�digo de la Interrupci�n RST6.5
		EI
		RET
IntRST7:
		;Ac� va el c�digo de la Interrupci�n RST7
		
		RET
IntRST75:
		;Ac� va el c�digo de la Interrupci�n RST7.5
		
		RET
;******************************************************************************
;	Sector del Programa Principal
;******************************************************************************
Boot:
		LXI	SP,STACK_ADDR	;Inicializo el Puntero de Pila
	
		MVI	A,MaskSetEn | M75 | M65		;0Eh para las interrupciones	
		SIM						;Inicializo las interrupciones
		EI
		CALL	MOSTRAR
Main:
		LDA	Flags
		ANI	01h
		CNZ	IncReloj
		JMP	Main
		
		HLT
;******************************************************************************
; Funci�n: IncReloj
; Descripci�n: Incrementa el reloj y muestra la nueva hora.
; Entrada: ninguna
; Salida: ninguna	 
;******************************************************************************
IncReloj:
		CALL	IncSeg
		CZ		IncMin
		CZ		IncHor
		
		CALL	MOSTRAR
		
		LDA	Flags
		ANI	FEh
		STA	Flags

		RET
;******************************************************************************
; Funci�n: IncSeg
; Descripci�n: Incrementa los segundos
; Entrada: ninguna
; Salida: Z = 1 si hay que incrementar Min	 
;******************************************************************************
IncSeg:
		LDA	Seg
		ADI	1
		DAA
		CPI	60h
		JNZ	finincseg
		MVI	A,0
finincseg:
		STA	Seg
		RET

;******************************************************************************
; Funci�n: IncMin
; Descripci�n: Incrementa los Minutos
; Entrada: ninguna
; Salida: Z = 1 si hay que incrementar Hor	 
;******************************************************************************
IncMin:	
		LDA	Min
		ADI	1
		DAA
		CPI	60h
		JNZ	finincmin
		MVI	A,0
finincmin:
		STA	Min
		RET

;******************************************************************************
; Funci�n: IncHor
; Descripci�n: Incrementa las Horas
; Entrada: ninguna
; Salida: Z = 1 si llega a 24h	 
;******************************************************************************
IncHor:	
		LDA	Hor
		ADI	1
		DAA
		CPI	24h
		JNZ	fininchor
		MVI	A,0
fininchor:
		STA	Hor
		RET

;******************************************************************************
; Funci�n: MOSTRAR
; Descripci�n: Divide los valores BCD empaquetados los convierte a
; 7 seg y los muestra
; Entrada: ninguna
; Salida: ninguna	 
;******************************************************************************
MOSTRAR:
		CALL	SPLIT
		CALL	CONV7SEG
		CALL	PrintDig7S
		RET

;******************************************************************************
; Funci�n: SPLIT
; Descripci�n: Divide los valores BCD Empaquetados
; Entrada: ninguna
; Salida: ninguna	 
;******************************************************************************
SPLIT:
		LDA	Seg
		ANI	0Fh
		STA	Reloj+5
		LDA	Seg
		ANI	F0h
		RRC
		RRC
		RRC
		RRC
		STA	Reloj+4
		
		LDA	Min
		ANI	0Fh
		STA	Reloj+3
		LDA	Min
		ANI	F0h
		RRC
		RRC
		RRC
		RRC
		STA	Reloj+2

		LDA	Hor
		ANI	0Fh
		STA	Reloj+1
		LDA	Hor
		ANI	F0h
		RRC
		RRC
		RRC
		RRC
		STA	Reloj+0
		RET

;******************************************************************************
; Funci�n: CONV7SEG
; Descripci�n: Convierte los valores de Reloj a 7 segmentos
; Entrada: ninguna
; Salida: ninguna	 
;******************************************************************************
CONV7SEG:
		LXI	H,Reloj+0	; Convierto Horas (MSB)
		MOV	A,M
		CALL SieteSEG
		SHLD DisplayRAM+DIG2
		
		LXI	H,Reloj+1	; Convierto Horas (LSB)
		MOV	A,M
		CALL SieteSEG
		SHLD DisplayRAM+DIG3

		LXI	H,Reloj+2	; Convierto Minutos (MSB)
		MOV	A,M
		CALL SieteSEG
		SHLD DisplayRAM+DIG4
		LXI	H,Reloj+3	; Convierto Minutos (LSB)
		MOV	A,M
		CALL SieteSEG
		SHLD DisplayRAM+DIG5

		LXI	H, Reloj+4	; Convierto Segundos (MSB)
		MOV	A,M
		CALL SieteSEG
		SHLD DisplayRAM+DIG6
		LXI	H, Reloj+5	; Convierto Segundos (MSB)
		MOV	A,M
		CALL SieteSEG
		SHLD DisplayRAM+DIG7
		RET

;******************************************************************************
; Funci�n: SieteSEG
; Descripci�n: Convierte a 7 segmentos
; Entrada: A (Valor BCD)
; Salida: H-L (C�digo 15 segmentos) 
;******************************************************************************
SieteSEG:
		CPI	0
		JNZ	NotIsNumber0
		LHLD	Numero0
		JMP	salir
NotIsNumber0:		
		CPI	1
		JNZ	NotIsNumber1
		LHLD	Numero1
		JMP	salir
NotIsNumber1:		
		CPI	2
		JNZ	NotIsNumber2
		LHLD	Numero2
		JMP	salir
NotIsNumber2:		
		CPI	3
		JNZ	NotIsNumber3
		LHLD	Numero3
		JMP	salir
NotIsNumber3:		
		CPI	4
		JNZ	NotIsNumber4
		LHLD	Numero4
		JMP	salir
NotIsNumber4:		
		CPI	5
		JNZ	NotIsNumber5
		LHLD	Numero5
		JMP	salir
NotIsNumber5:		
		CPI	6
		JNZ	NotIsNumber6
		LHLD	Numero6
		JMP	salir
NotIsNumber6:		
		CPI	7
		JNZ	NotIsNumber7
		LHLD	Numero7
		JMP	salir
NotIsNumber7:		
		CPI	8
		JNZ	NotIsNumber8
		LHLD	Numero8
		JMP	salir
NotIsNumber8:		
		CPI	9
		JNZ	NotIsNumber9
		LHLD	Numero9
NotIsNumber9:
salir:
		RET

;******************************************************************************
;	Funci�n: PrintDig7S
;	Descripci�n: Imprime los N�meros en los display de 7 Segmentos
;		   Los datos son tomados de un buffer denominado DisplayRAM
;	 _____   _____   _____   _____   _____   _____   _____   _____	
;	|     | |     | |     | |     | |     | |     | |     | |     |
;	|     | |     | |     | |     | |     | |     | |     | |     |
;	|  0  | |  1  | |  2  | |  3  | |  4  | |  5  | |  6  | |  7  |
;	|     | |     | |     | |     | |     | |     | |     | |     |
;	|_____| |_____| |_____| |_____| |_____| |_____| |_____| |_____|
;******************************************************************************
PrintDig7S:
		PUSH	H
		
		;CALL	Print7SDig0
		;CALL	Print7SDig1
		CALL	Print7SDig2
		CALL	Print7SDig3
		CALL	Print7SDig4
		CALL	Print7SDig5
		CALL	Print7SDig6
		CALL	Print7SDig7
		
		POP	H
		RET	
;******************************************************************************
;	Funci�n: Print7SDig0
;	Descripci�n: Imprime el Digito 0 en los display de 15 Segmentos
;		   Los datos son tomados de un buffer denominado DisplayRAM
;	
;******************************************************************************
Print7SDig0:	
		LHLD	DisplayRAM+DIG0
		;MOV	A,L
		;OUT	AddrDISP7S+0
		MOV	A,H
		OUT	AddrDISP7S+0
		RET

Print7SDig1:	
		LHLD	DisplayRAM+DIG1
		;MOV	A,L
		;OUT	AddrDISP7S+1
		MOV	A,H
		OUT	AddrDISP7S+1
		RET


Print7SDig2:	
		LHLD	DisplayRAM+DIG2
		;MOV	A,L
		;OUT	AddrDISP7S+2
		MOV	A,H
		OUT	AddrDISP7S+2
		RET

Print7SDig3:	
		LHLD	DisplayRAM+DIG3
		;MOV	A,L
		;OUT	AddrDISP7S+3
		MOV	A,H
		OUT	AddrDISP7S+3
		RET

Print7SDig4:	
		LHLD	DisplayRAM+DIG4
		;MOV	A,L
		;OUT	AddrDISP7S+4
		MOV	A,H
		OUT	AddrDISP7S+4
		RET

Print7SDig5:	
		LHLD	DisplayRAM+DIG5
		;MOV	A,L
		;OUT	AddrDISP7S+5
		MOV	A,H
		OUT	AddrDISP7S+5
		RET

Print7SDig6:	
		LHLD	DisplayRAM+DIG6
		;MOV	A,L
		;OUT	AddrDISP7S+6
		MOV	A,H
		OUT	AddrDISP7S+6
		RET

Print7SDig7:	
		LHLD	DisplayRAM+DIG7
		;MOV	A,L
		;OUT	AddrDISP7S+7
		MOV	A,H
		OUT	AddrDISP7S+7
		RET


